<?php  $page='home';
/* Template Name: Home 
 * @package escolha-livre
 */
?>
        <?php get_header(); ?>

        <main id="page-home">
          <!-- Linha do topo -->
          <div class="conteiner-topo container">
            <div class="linha-titulo row d-flex align-items-center justify-content-center">
                    
              <!-- Coluna Esquerda -->
              <div class="titulo col-12 col-lg-6">

                <h1 class="titulo-home">
                  <?php
                  // Permitindo html seguro ao filtrar/ascapar tags inseguras
                  $title = get_the_title();
                  $allowed_tags = array(
                    'br' => array(),
                    'a' => array(
                      'href' => array(),
                      'title' => array()
                    ),
                    'em' => array(),
                    'strong' => array(),
                    'p' => array(),
                    'span' => array(),
                    // Adicione outras tags permitidas aqui, se necessário
                  );
                  echo wp_kses($title, $allowed_tags);?>
                </h1>
              
                <div class="paragrafo pe-5 ps-4 pb-lg-0">
                  <?php
                  // Conteúdo abaixo do título
                  the_content();?>
                </div>
              </div>

              <!-- Coluna Direita -->
              <div class="deslocar col-12 col-lg-6 pt-5">
                <div class="subtitulo">
                  <?php
                  $link = esc_url(get_field('linque_da_novidade'));

                  if ($link) {
                    echo '<a href="' . $link . '" class="text-decoration-none text-muted">';
                  }?>
                
                  <h2 class="text-captalize p-2 px-4">
                    <?php //echo get_field('titulo_da_novidade'); ?>
                    <?php // Verificar o idioma atual
                    $current_language = pll_current_language();
                    // Obter o conteúdo do campo personalizado com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('titulo_da_novidade');
                    } elseif($current_language == 'es') {
                      echo get_field('titulo_da_novidade_es');
                    } elseif($current_language == 'en') {
                      echo get_field('titulo_da_novidade_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                      echo get_field('titulo_da_novidade');
                    }?>
                  </h2>

                  <div class="mr-5 div-4 p-4">
                    <?php //echo get_field('resumo_da_novidade'); ?>
                    <?php // Verificar o idioma atual
                    $current_language = pll_current_language();
                    // Obter o conteúdo do campo personalizado com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('resumo_da_novidade');
                    } elseif($current_language == 'es') {
                      echo get_field('resumo_da_novidade_es');
                    } elseif($current_language == 'en') {
                      echo get_field('resumo_da_novidade_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                      echo get_field('resumo_da_novidade');
                    }?>
                  </div>

                  <img class="img-fluid px-4 pt-0 pb-4" height="250" src="<?php echo esc_url(get_field('imagem_da_novidade')); ?>" />
                  <?php
                  if ($link) {
                    echo '</a>';
                  }?>
                </div>
              </div>
            </div>
          </div>

          <!-- Linha do Meio -->
          <!-- Desktop -->
          <div class="desktop linha-meio conteiner-meio">
            <ul class="row my-5 d-flex align-items-center justify-content-center">

              <li class="col-5"></li>
              <li class="col-1"></li>

              <li class="col-6 aprenda separador">
                <h2 class="text-uppercase me-5">
                  <a class="text-decoration-none text-muted me-5" href="<?php echo get_site_url(); ?>/aprenda">
                    <?php echo esc_html( pll__( 'Aprenda', 'theme-escolha-livre' )); ?>
                  </a>
                </h2>
              </li>

              <li class="col-5 recursos text-end separador">
                <h2 class="text-uppercase">
                  <a class="text-decoration-none text-muted" href="<?php echo get_site_url(); ?>/recursos">
                    <?php echo esc_html( pll__( 'Recursos', 'theme-escolha-livre' )); ?>
                  </a>
                </h2>
              </li>
                        
              <li class="col-1"></li>
              <li class="col-6"></li>
              <li class="col-5"></li>
              <li class="col-1"></li>

              <li class="col-6 tutoriais text-uppercase separador">
                <h2 class="text-uppercase">
                <a class="text-decoration-none text-muted" href="<?php echo get_site_url(); ?>/tutoriais">
                  <?php echo esc_html( pll__( 'Tutoriais', 'theme-escolha-livre' )); ?>
                </a>
                </h2>
              </li>
            </ul>
          </div>

          <!-- Mobile -->
          <div class="mobile linha-meio conteiner-meio">
            <ul class="row my-5 d-flex align-items-center justify-content-center">

              <li class="col-6"></li>

              <li class="col-6 aprenda separador">
                <h2 class="text-uppercase">
                  <a class="text-decoration-none text-muted" href="<?php echo get_site_url(); ?>/aprenda">
                    <?php echo esc_html( pll__( 'Aprenda', 'theme-escolha-livre' )); ?>
                  </a>
                </h2>
              </li>

              <li class="col-6 recursos text-end separador">
                <h2 class="text-uppercase">
                  <a class="text-decoration-none text-muted" href="<?php echo get_site_url(); ?>/recursos">
                    <?php echo esc_html( pll__( 'Recursos', 'theme-escolha-livre' )); ?>
                  </a>
                </h2>
              </li>

              <li class="col-6"></li>
              <li class="col-6"></li>

              <li class="col-6 tutoriais text-uppercase separador">
                <h2 class="text-uppercase">
                  <a class="text-decoration-none text-muted" href="<?php echo get_site_url(); ?>/tutoriais">
                    <?php echo esc_html( pll__( 'Tutoriais', 'theme-escolha-livre' )); ?>
                  </a>
                </h2>
              </li>
            </ul>
          </div>
          <!-- Fim da Linha do Meio -->

          <!-- Linha da Base -->
          <div class="container px-5">
            <div class="row linha-base">

              <div class="col-12 descricao d-flex align-items-center justify-content-center p-0">
                <?php // Verificar o idioma atual
                $current_language = pll_current_language();
                
                // Obter o conteúdo do campo personalizado com base no idioma
                if($current_language == 'pt_BR') {
                  echo get_field('descricao');
                } elseif($current_language == 'es') {
                  echo get_field('descricao_es');
                } elseif($current_language == 'en') {
                  echo get_field('descricao_en');
                } else {
                // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                  echo get_field('descricao');
                }?>
              </div>
            </div>
          </div>
          <!-- Fim da Linha da Base -->
        </main>

      <?php get_footer(); ?>

<?php $page='liberdade';
/* Template Name: Liberdade
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-liberdade" class="container pb-5">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="pb-3">
                        Com o software livre, você pode baixar e executar um programa, ter acesso ao código do programa para estudar como ele funciona, modificá-lo para seus propósitos e distribuir cópias do software para quem quiser (veja mais sobre a filosofia do software livre). Ou seja, você pode baixar o software e rodá-lo em seu computador, além de modificá-lo para seus propósitos. Boa parte do software livre também é gratuito – você não precisa de permissões, licenças ou autorizações para baixar, instalar e utilizar como bem entender. Recursos Educacionais Abertos (REA) são gratuitos e dão uma série de permissões de uso e reuso que não estão disponíveis com recursos comuns.
                    </p>
                    <p class="pb-3">
                        Mas existe software livre que não é gratuito, pelo qual você paga para ter acesso ou para obter suporte, apoio e atualizações. São mecanismos de remuneração para a produção de software e sistemas de alto nível. Mesmo sendo pagos, uma vez que se tenha o acesso ao software, você tem total liberdade de visualizar o seu código e o funcionamento, de alterar, copiar e compartilhar o software (existem diversas perspectivas sobre o software livre).
                    </p>
                    <p class="pb-3">
                        Com essa lógica, é fácil ver que o software livre é mais do que um produto ou serviço que você simplesmente compra. Ele é baseado no princípio da transparência, da melhoria contínua, do trabalho colaborativo e cooperativo.
                    </p class="pb-3">
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row">
                <div class="col-md-12 px-5">

                    <!-- Linha interna -->
                    <div class="row borda-esq-base d-flex align-items-center justify-content-center p-0 mb-5">

                        <p class="videos col-md-12 align-srart m-0 p-3">
                            Código aberto e livre
                        </p>

                        <div class="col-md-12 d-flex align-items-center justify-content-center align-self-end py-3">
                            <iframe src="https://player.vimeo.com/video/461392848?h=c607a02ac6&color=ff0179" width="660" height="390" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>
                    <!-- /Fim da Linha interna -->
                    
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

            <!-- Linha 4 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="px-5">
                        <div class="col-md-12 borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                            <!-- Linha interna -->
                            <div class="row">

                                <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                                    "Sem romantismo, mas com investimento econômico e cultural, é possível fomentar e manter um ecossistema sustentável e inovador de produção tecnológica com software livre…"
                                </p>

                                <img class="col-md-2 img-fluid py-3" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/karina.jpg" width="400" height="533" alt="Foto de professor Nelson Pretto da UFBA" />
                                
                                <div class="col-md-4 align-self-end py-3">
                                    <p class="d-inline align-text-bottom">
                                        Profa. Karina Menezes
                                        <br><br>
                                        Pedagoga. Professora da Faculdade de Educação da Universidade Federal da Bahia. Doutora em Educação. Vencedora do Prêmio Capes de Tese 2019 na área Educação. Integrante do Raul Hacker Club de Salvador Bahia, Idealizadora do Crianças Hackers.
                                    </p>
                                </div>

                                <!-- Audio player -->
                                <div class="holder col-md-6 d-flex align-self-end p-0">
                                    <div class="audio green-audio-player">
                                        <!-- Adicione a palavra "Escute" aqui -->
                                        <span class="listen-text text-light me-3">Escute</span>
                                        <div class="loading">
                                            <div class="spinner"></div>
                                        </div>
                                        <div class="play-pause-btn">  
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                            </svg>
                                        </div>

                                        <div class="controls">
                                            <span class="current-time">0:00</span>
                                            <div class="slider" data-direction="horizontal">
                                                <div class="progress">
                                                    <div class="pin" id="progress-pin" data-method="rewind"></div>
                                                </div>
                                            </div>
                                            <span class="total-time">0:00</span>
                                        </div>

                                        <div class="volume">
                                            <div class="volume-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                                </svg>
                                            </div>
                                            <div class="volume-controls hidden">
                                                <div class="slider" data-direction="vertical">
                                                    <div class="progress">
                                                        <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <audio crossorigin>
                                            <source src="https://archive.org/download/karina-menezes-vigilancia/karina.mp3" type="audio/mp3">
                                        </audio>
                                    </div>
                                    <!-- Script do audio player acima -->
                                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                                </div>
                                <!-- /Fim do audio player -->
                            </div>

                        </div>
                        <!-- /Fim da Linha interna -->
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 4 -->

        </main>

<?php get_footer(); ?>
<?php
/* Template Name: Single Sobre
 * @package escolha-livre
 */
get_header();
?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>

		<main id="single-aprenda" class="container">
			<section>

        <!-- Espaço -->
        <div class="espaco-80"></div>
        <!-- Linha 1 -->
        <!-- Título 1 da Página -->
        <div class="titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
            <?php
            // Permitindo html seguro ao filtrar/ascapar tags inseguras
            $title = get_the_title();
            $allowed_tags = array(
                'br' => array(),
                'a' => array(
                    'href' => array(),
                    'title' => array()
                ),
                'em' => array(),
                'strong' => array(),
                'p' => array(),
                // Adicione outras tags permitidas aqui, se necessário
            );
            echo wp_kses($title, $allowed_tags);
            ?>
            </h1>
          </div>
        </div>
        <!-- /Fim da Linha 1 -->

        <!-- Espaço -->
        <div class="espaco-60"></div>

        <!-- Linha 2 -->
        <!-- Conteúdo textual -->
        <div class="row mb-3">
          <div class="col-md-12">
            <?php echo the_content() ?>
          </div>
          
          <?php if (!empty(get_field('imagem_conteudo_1'))) { ?>
          <!-- Imagem Conteúdo 1 -->
          <div class="col-12 d-flex justify-content-center">
            <div class="col col-lg-8 mb-5">
              <figure class="borda-esq-base row d-flex flex-row p-md-3">
                <img class="img-fluid col-md-6 py-3" src="<?php echo get_field('imagem_conteudo_1'); ?>">
                <figcaption class="col-md-6 align-items-end justify-content-center">
                    <?php //echo get_field('texto_legenda_1'); ?>
                    <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                      
                      // Obter o conteúdo do campo personalizado com base no idioma
                      if($current_language == 'pt_BR') {
                        echo get_field('texto_legenda_1');
                      } elseif($current_language == 'es') {
                        echo get_field('texto_legenda_1_es');
                      } elseif($current_language == 'en') {
                        echo get_field('texto_legenda_1_en');
                      } else {
                        // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                        echo get_field('texto_legenda_1');
                      }
                    ?>
                </figcaption>
              </figure>
            </div>
          </div>
          <?php } ?>

          <?php if (!empty(get_field('imagem_conteudo_1'))) { ?>
          <!-- Texto Conteúdo 1 -->
          <div class="texto-div col-12 mt-3 mb-5">
            <?php //echo get_field('texto_conteudo_1'); ?>
            <?php 
              // Verificar o idioma atual
              $current_language = pll_current_language();
                      
              // Obter o conteúdo do campo personalizado com base no idioma
              if($current_language == 'pt_BR') {
                echo get_field('texto_conteudo_1');
              } elseif($current_language == 'es') {
                echo get_field('texto_conteudo_1_es');
              } elseif($current_language == 'en') {
                echo get_field('texto_conteudo_1_en');
              } else {
                // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                echo get_field('texto_conteudo_1');
              }
            ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 1 -->
          <?php } ?>
          
          <?php if (!empty(get_field('imagem_conteudo_2'))) { ?>
          <!-- Imagem Conteúdo 2 -->
          <div class="col-12 mb-5 d-flex justify-content-center">
            <div class="col col-lg-8">
              <figure class="borda-esq-base row py-3">
                <figcaption class="col-md-8 align-items-end justify-content-center">
                  <?php //echo get_field('texto_legenda_2'); ?>
                  <?php 
                    // Verificar o idioma atual
                    $current_language = pll_current_language();
                            
                    // Obter o conteúdo do campo personalizado com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('texto_legenda_2');
                    } elseif($current_language == 'es') {
                      echo get_field('texto_legenda_2_es');
                    } elseif($current_language == 'en') {
                      echo get_field('texto_legenda_2_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                      echo get_field('texto_legenda_2');
                    }
                  ?>
                </figcaption>
                <img class="img-fluid col-md-4 py-md-3 py-3" src="<?php echo get_field('imagem_conteudo_2'); ?>">
              </figure>
            </div>
          </div>
          <?php } ?>
          
          <?php if (!empty(get_field('texto_conteudo_2'))) { ?>
          <!-- Texto Conteúdo 2 -->
          <div class="texto-div col-12 mt-3 mb-5">
            <?php //echo get_field('texto_conteudo_2'); ?>
            <?php 
              // Verificar o idioma atual
              $current_language = pll_current_language();
                            
              // Obter o conteúdo do campo personalizado com base no idioma
              if($current_language == 'pt_BR') {
                echo get_field('texto_conteudo_2');
              } elseif($current_language == 'es') {
                echo get_field('texto_conteudo_2_es');
              } elseif($current_language == 'en') {
                echo get_field('texto_conteudo_2_en');
              } else {
                // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                echo get_field('texto_conteudo_2');
              }
            ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 2 -->
          <?php } ?>

          <?php if (!empty(get_field('imagem_banner_3'))) { ?>
          <!-- Imagem Conteúdo 3 -->
          <div class="col-12 d-flex justify-content-center mb-5">
              <figure class="borda-esq-base row d-flex flex-row p-md-1">
                <img class="img-fluid" src="<?php echo get_field('imagem_banner_3'); ?>">
              
                <figcaption class="align-items-end justify-content-center">
                  <?php //echo get_field('texto_imagem_3'); // Foi editado para o termo abaixo ?>
                  <?php //echo get_field('legenda_banner'); ?>
                  <?php 
                    // Verificar o idioma atual
                    $current_language = pll_current_language();
                                  
                    // Obter o conteúdo do campo personalizado com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('field_655eb0b782032');
                    } elseif($current_language == 'es') {
                      echo get_field('field_6603fe376cbac');
                    } elseif($current_language == 'en') {
                      echo get_field('field_6603fe676cbad');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                      echo get_field('field_655eb0b782032');
                    }
                  ?>
                </figcaption>
              
              </figure>
          </div>
          <?php } ?>

          <?php if (!empty(get_field('texto_conteudo_3'))) { ?>
          <div class="texto-div col-12 mt-3 mb-5">
              <?php //echo get_field('texto_conteudo_3'); ?>
              <?php 
                // Verificar o idioma atual
                $current_language = pll_current_language();
                                  
                // Obter o conteúdo do campo personalizado com base no idioma
                if($current_language == 'pt_BR') {
                  echo get_field('texto_conteudo_3');
                } elseif($current_language == 'es') {
                  echo get_field('texto_conteudo_3_es');
                } elseif($current_language == 'en') {
                  echo get_field('texto_conteudo_3_en');
                } else {
                // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                  echo get_field('texto_conteudo_3');
                }
              ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 3 -->
          <?php } ?>

        </div>
        <!-- /Fim da Linha 2 -->

        <!-- Linha 3 Vídeo Grande -->
        <div class="row enviar-publicar mb-3">
          <?php if (!empty(get_field('video_grande'))) { ?>
          <!-- Caixa de vídeo Grande -->
          <div class="col-md-12">
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php //echo get_field('video_grande_titulo'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o título do vídeo grande com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('video_grande_titulo');
                  } elseif($current_language == 'es') {
                    echo get_field('video_grande_titulo_es');
                  } elseif($current_language == 'en') {
                    echo get_field('video_grande_titulo_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o título padrão
                    echo get_field('video_grande_titulo');
                  }
                ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_grande'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php //echo get_field('video_grande_legenda'); ?>
                  <?php 
                    // Obter a legenda do vídeo grande com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('video_grande_legenda');
                    } elseif($current_language == 'es') {
                      echo get_field('video_grande_legenda_es');
                    } elseif($current_language == 'en') {
                      echo get_field('video_grande_legenda_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir a legenda padrão
                      echo get_field('video_grande_legenda');
                    }
                  ?>
                </figcaption>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <!-- /Fim da Linha 3 -->
                
        <!-- Linha 4 -->
        <!-- Componente audio -->
        <div class="row">
          <div class="col-md-12">
            <?php get_template_part('template/componentes/audio-part'); ?>
          </div>
        </div>
        <!-- /Fim da Linha 4 -->
      </section>

      <!-- Espaço -->
      <div class="espaco-80"></div>

      <!-- Linha 5 -->
      <section>

        <?php if (!empty(get_field('titulo_2'))) { ?>          
        <!-- Título 2 Produção de vídeo -->
        <div id="enviar-publicar" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php //echo get_field('titulo_2') ?>
              <?php 
                // Verificar o idioma atual
                $current_language = pll_current_language();
                
                // Obter o título do vídeo grande com base no idioma
                if($current_language == 'pt_BR') {
                  echo get_field('titulo_2');
                } elseif($current_language == 'es') {
                  echo get_field('titulo_2_es');
                } elseif($current_language == 'en') {
                  echo get_field('titulo_2_en');
                } else {
                  // Caso o idioma não seja encontrado, exibir o título padrão
                  echo get_field('titulo_2');
                }
              ?>
            </h1>
          </div>
        </div>
        <?php } ?>
        <!-- /Fim da Linha 5 -->

        <!-- Espaço -->
        <div class="espaco-60"></div>

        <!-- Linha 6 -->
        <div class="row enviar-publicar">

          <!-- Coluna 1 Texto -->
          <div class="col-md-6 mb-3">

            <?php if (!empty(get_field('subtitulo_1'))) { ?>          
            <!-- Caixa de texto 1 -->
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php //echo get_field('subtitulo_1'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('subtitulo_1');
                  } elseif($current_language == 'es') {
                    echo get_field('subtitulo_1_es');
                  } elseif($current_language == 'en') {
                    echo get_field('subtitulo_1_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('subtitulo_1');
                  }
                ?>
              </h3>
              <div class="p-3">
                <?php //echo get_field('texto_da_caixa_1'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('texto_da_caixa_1');
                  } elseif($current_language == 'es') {
                    echo get_field('texto_da_caixa_1_es');
                  } elseif($current_language == 'en') {
                    echo get_field('texto_da_caixa_1_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('texto_da_caixa_1');
                  }
                ?>
              </div>

              <?php if (!empty(get_field('audio_linque_1'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner">
                      </div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                      <div class="slider" data-direction="horizontal">
                        <div class="progress">
                          <div class="pin" id="progress-pin" data-method="rewind">
                          </div>
                        </div>
                      </div>
                        <span class="total-time">
                          0:00
                        </span>
                      </div>
                      <div class="volume">
                        <div class="volume-btn">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                          </svg>
                        </div>
                        <div class="volume-controls hidden">
                          <div class="slider" data-direction="vertical">
                            <div class="progress">
                              <div class="pin" id="volume-pin" data-method="changeVolume">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <audio crossorigin>
                        <source src="<?php echo get_field('audio_linque_1'); ?>" type="audio/mpeg">
                      </audio>
                    </div>
                  </div>
                  <!-- Script do audio player acima -->
                  <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                </div>
                <?php } ?>
              </div>
              <?php } ?>
            </div>

          <!-- Coluna 2 Texto -->
          <div class="col-md-6 mb-3">

            <?php if (!empty(get_field('subtitulo_2'))) { ?>
            <!-- Caixa de texto 2 -->
            <div class="borda-dir-base">
              <h3 class="fundo-preto p-3">
                <?php //echo get_field('subtitulo_2'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('subtitulo_2');
                  } elseif($current_language == 'es') {
                    echo get_field('subtitulo_2_es');
                  } elseif($current_language == 'en') {
                    echo get_field('subtitulo_2_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('subtitulo_2');
                  }
                ?>
              </h3>
              <div class="p-3">
                <?php //echo get_field('texto_da_caixa_2'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('texto_da_caixa_2');
                  } elseif($current_language == 'es') {
                    echo get_field('texto_da_caixa_2_es');
                  } elseif($current_language == 'en') {
                    echo get_field('texto_da_caixa_2_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('texto_da_caixa_2');
                  }
                ?>
              </div>

              <?php if (!empty(get_field('audio_linque_2'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner">
                      </div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                    <div class="slider" data-direction="horizontal">
                      <div class="progress">
                        <div class="pin" id="progress-pin" data-method="rewind">
                        </div>
                      </div>
                    </div>
                    <span class="total-time">
                      0:00
                    </span>
                  </div>
                  <div class="volume">
                    <div class="volume-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                      </svg>
                    </div>
                    <div class="volume-controls hidden">
                      <div class="slider" data-direction="vertical">
                        <div class="progress">
                          <div class="pin" id="volume-pin" data-method="changeVolume">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <audio crossorigin>
                    <source src="<?php echo get_field('audio_linque_2'); ?>" type="audio/mpeg">
                  </audio>
                </div>
                <!-- Script do audio player acima -->
                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
              </div>
            </div>
            <?php } ?>        
          </div>
          <?php } ?>
        </div>

        <div class="row">
          <!-- Coluna 3 -->
          <div class="col-md-12 mb-3">
            <?php if (!empty(get_field('subtitulo_3'))) { ?>
            <!-- Caixa de texto 3 -->
            <div class="borda-dir-base">

              <h3 class="fundo-preto p-3">
                <?php //echo get_field('subtitulo_3'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('subtitulo_3');
                  } elseif($current_language == 'es') {
                    echo get_field('subtitulo_3_es');
                  } elseif($current_language == 'en') {
                    echo get_field('subtitulo_3_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('subtitulo_3');
                  }
                ?>
              </h3>

              <div class="p-3">
                <?php //echo get_field('texto_da_caixa_3'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                  
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('texto_da_caixa_3');
                  } elseif($current_language == 'es') {
                    echo get_field('texto_da_caixa_3_es');
                  } elseif($current_language == 'en') {
                    echo get_field('texto_da_caixa_3_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('texto_da_caixa_3');
                  }
                ?>
              </div>

              <?php if (!empty(get_field('audio_linque_3'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner"></div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                      <div class="slider" data-direction="horizontal">
                        <div class="progress">
                          <div class="pin" id="progress-pin" data-method="rewind"></div>
                        </div>
                      </div>
                      <span class="total-time">
                        0:00
                      </span>
                    </div>
                    <div class="volume">
                      <div class="volume-btn">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                          <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                        </svg>
                      </div>
                      <div class="volume-controls hidden">
                        <div class="slider" data-direction="vertical">
                          <div class="progress">
                            <div class="pin" id="volume-pin" data-method="changeVolume">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <audio crossorigin>
                      <source src="<?php echo get_field('audio_linque_3'); ?>" type="audio/mpeg">
                    </audio>
                  </div>
                  <!-- Script do audio player acima -->
                  <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <!-- /Fim da Coluna 3 -->
          <?php } ?>
        </div>
        </div>
      </section>

      <!-- Espaço -->
      <div class="espaco-80"></div>

      <!-- Linha 7 -->
      <section>
        <?php if (!empty(get_field('titulo_3'))) { ?>
                          
        <!-- Título 2 -->
        <div id="enviar-publicar" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php //echo get_field('titulo_3') ?>
              <?php 
                // Verificar o idioma atual
                $current_language = pll_current_language();
                  
                // Obter o subtítulo com base no idioma
                if($current_language == 'pt_BR') {
                  echo get_field('titulo_3');
                } elseif($current_language == 'es') {
                  echo get_field('titulo_3_es');
                } elseif($current_language == 'en') {
                  echo get_field('titulo_3_en');
                } else {
                  // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                  echo get_field('titulo_3');
                }
              ?>
            </h1>
          </div>
        </div>
        <?php } ?>
        <!-- /Fim da Linha 5 -->

        <!-- Espaço -->
        <div class="espaco-60"></div>

        <!-- Linha 6 -->
        <div class="row enviar-publicar">

          <?php if (!empty(get_field('titulo_video_1'))) { ?>         
          <!-- Caixa de vídeo 1 -->
          <div class="col-md-6 mb-3">
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php //echo get_field('titulo_video_1'); ?>
                <?php 
                  // Verificar o idioma atual
                  $current_language = pll_current_language();
                    
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('titulo_video_1');
                  } elseif($current_language == 'es') {
                    echo get_field('titulo_video_1_es');
                  } elseif($current_language == 'en') {
                    echo get_field('titulo_video_1_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('titulo_video_1');
                  }
                ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_1'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php //echo get_field('texto_video_1'); ?>
                  <?php 
                    // Verificar o idioma atual
                    $current_language = pll_current_language();
                      
                    // Obter o subtítulo com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('texto_video_1');
                    } elseif($current_language == 'es') {
                      echo get_field('texto_video_1_es');
                    } elseif($current_language == 'en') {
                      echo get_field('texto_video_1_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('texto_video_1');
                    }
                  ?>
                </figcaption>
              </div>
            </div>
          </div>
          <?php } ?>
          <!-- Caixa de vídeo 2 -->
          <div class="col-md-6">
            <?php if (!empty(get_field('titulo_video_2'))) { ?>
            <!-- Caixa de texto 2 -->
            <div class="borda-dir-base">
              <h3 class="fundo-preto p-3">
                <?php //echo get_field('titulo_video_2'); ?>
                <?php 
                    // Verificar o idioma atual
                    $current_language = pll_current_language();
                      
                    // Obter o subtítulo com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('titulo_video_2');
                    } elseif($current_language == 'es') {
                      echo get_field('titulo_video_2_es');
                    } elseif($current_language == 'en') {
                      echo get_field('titulo_video_2_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('titulo_video_2');
                    }
                  ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_2'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php //echo get_field('texto_video_2'); ?>
                  <?php 
                    // Verificar o idioma atual
                    $current_language = pll_current_language();
                      
                    // Obter o subtítulo com base no idioma
                    if($current_language == 'pt_BR') {
                      echo get_field('texto_video_2');
                    } elseif($current_language == 'es') {
                      echo get_field('texto_video_2_es');
                    } elseif($current_language == 'en') {
                      echo get_field('texto_video_2_en');
                    } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('texto_video_2');
                    }
                  ?>
                </figcaption>
              </div>
            </div>
            <?php } ?>

          </div>
        </div>
      </section>
      <!-- /Fim da Linha 7 -->

      <!-- Espaço -->
      <div class="espaco-80"></div>

      <!-- Linha 8 -->
      <section id="escrita" class="row titulo-h1 d-flex align-items-center justify-content-center">
        <div class="col-12 separador">
          
          <?php if (!empty(get_field('titulo_4'))) { ?>
          <!-- Título 4 Recursos  -->
          <h1 class="text-uppercase me-5 text-decoration-none text-muted">
            <?php //echo get_field('titulo_4'); ?>
            <?php 
              // Verificar o idioma atual
              $current_language = pll_current_language();
                      
              // Obter o subtítulo com base no idioma
              if($current_language == 'pt_BR') {
              echo get_field('titulo_4');
              } elseif($current_language == 'es') {
              echo get_field('titulo_4_es');
              } elseif($current_language == 'en') {
              echo get_field('titulo_4_en');
              } else {
              // Caso o idioma não seja encontrado, exibir o subtítulo padrão
              echo get_field('titulo_4');
              }
            ?>
          </h1>
          <?php } ?>
        </div>
        <!-- /Fim da Linha 8 -->

        <!-- Espaço -->
        <div class="espaco-60"></div>

        <!-- Linha 9 -->
        <div class="row">
          <div class="col-md-6 my-2">

            <?php if (!empty(get_field('imagem_1_titulo_4'))) { ?>
            <!-- Imagem Escrita 1 -->
            <a class="link-offset-2 link-underline link-underline-opacity-0" href="<?php echo get_field('abrir_a_imagem'); ?>">
              <div class="borda-esq-topo d-flex align-items-center justify-content-center">
                <figure class="figure p-3">
                  <img src="<?php echo get_field('imagem_1_titulo_4'); ?>" class="img-fluid" width="600" alt="...">
                  <figcaption class="figure-caption text-center py-3">
                    <?php //echo get_field('legenda_imagem'); ?>
                    <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                              
                      // Obter o subtítulo com base no idioma
                      if($current_language == 'pt_BR') {
                      echo get_field('legenda_imagem');
                      } elseif($current_language == 'es') {
                      echo get_field('legenda_imagem_es');
                      } elseif($current_language == 'en') {
                      echo get_field('legenda_imagem_en');
                      } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('legenda_imagem');
                      }
                    ?>
                  </figcaption>
                </figure>
              </div>
            </a>
            <?php } ?>

          </div>
          <div class="col-md-6 my-2">

            <?php if (!empty(get_field('imagem_2_titulo_4'))) { ?>
            <!-- Imagem Escrita 2 -->
            <a class="link-offset-2 link-underline link-underline-opacity-0" href="<?php echo get_field('abrir_a_imagem_2'); ?>">
              <div class="borda-esq-topo d-flex align-items-center justify-content-center">
                <figure class="figure p-3">
                  <img src="<?php echo get_field('imagem_2_titulo_4'); ?>" class="img-fluid" width="600" alt="...">
                  <figcaption class="figure-caption text-center py-3">
                    <?php //echo get_field('legenda_imagem_2'); ?>
                    <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                              
                      // Obter o subtítulo com base no idioma
                      if($current_language == 'pt_BR') {
                      echo get_field('legenda_imagem_2');
                      } elseif($current_language == 'es') {
                      echo get_field('legenda_imagem_2_es');
                      } elseif($current_language == 'en') {
                      echo get_field('legenda_imagem_2_en');
                      } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('legenda_imagem_2');
                      }
                    ?>
                  </figcaption>
                </figure>
              </div>
            </a>
            <?php } ?>
            
          </div>
        </div>
      </section>
      <!-- Linha 10 -->

      <!-- Espaço -->
      <div class="espaco-80"></div>

      <!-- Linha 11 -->
      <section>
        <div id="recursos" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <!-- Recursos Título 3 -->
          <div class="col-12 separador">

            <?php if (!empty(get_field('titulo_5_recursos'))) { ?>
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php //echo get_field('titulo_5_recursos'); ?>
              <?php 
                // Verificar o idioma atual
                $current_language = pll_current_language();
                
                // Obter o subtítulo com base no idioma
                if($current_language == 'pt_BR') {
                echo get_field('titulo_5_recursos');
                } elseif($current_language == 'es') {
                echo get_field('titulo_5_recursos_es');
                } elseif($current_language == 'en') {
                echo get_field('titulo_5_recursos_en');
                } else {
                // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                echo get_field('titulo_5_recursos');
                }
              ?>
            </h1>
            <?php } ?>
          </div>
        </div>
        <!-- /Fim da Linha 9 -->

        <!-- Espaço -->
        <div class="espaco-60"></div>

        <!-- Linha 10 -->
        <div id="relacionados" class="row">
          <div class="col-md-4 my-2">
            <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
            <!-- Recurso 1 -->
            <a class="link-offset-2 link-underline link-underline-opacity-0 d-block h-100" href="<?php echo get_field('recurso_1_linque'); ?>" target="_blank">
              <div class="borda-esq-topo d-flex align-items-center justify-content-center h-100">
                <figure class="figure p-3">
                  <figcaption class="figure-caption text-center py-3">
                    <h5>
                    <?php //echo get_field('recurso_1_titulo'); ?>
                    <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                      
                      // Obter o subtítulo com base no idioma
                      if($current_language == 'pt_BR') {
                      echo get_field('recurso_1_titulo');
                      } elseif($current_language == 'es') {
                      echo get_field('recurso_1_titulo_es');
                      } elseif($current_language == 'en') {
                      echo get_field('recurso_1_titulo_en');
                      } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('recurso_1_titulo');
                      }
                    ?>
                    </h5>
                  </figcaption>

                  <img src="<?php echo get_field('recurso_1_imagem'); ?>" class="img-fluid" width="180" height="140" alt="...">
                </figure>
              </div>
            </a>
            <?php } ?>
            </div>
            <div class="col-md-4 my-2">
              <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
              <!-- Recurso 2 -->
              <a class="link-offset-2 link-underline link-underline-opacity-0 d-block h-100" href="<?php echo get_field('recurso_2_linque'); ?>" target="_blank">
                <div class="borda-esq-topo d-flex align-items-center justify-content-center h-100">
                  <figure class="figure p-3">
                    <figcaption class="figure-caption text-center py-3">
                      <h5>
                      <?php //echo get_field('recurso_2_titulo'); ?>
                      <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                      
                      // Obter o subtítulo com base no idioma
                      if($current_language == 'pt_BR') {
                      echo get_field('recurso_2_titulo');
                      } elseif($current_language == 'es') {
                      echo get_field('recurso_2_titulo_es');
                      } elseif($current_language == 'en') {
                      echo get_field('recurso_2_titulo_en');
                      } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('recurso_2_titulo');
                      }
                      ?>
                      </h5>
                    </figcaption>
                    <img src="<?php echo get_field('recurso_2_imagem'); ?>" class="img-fluid pt-3 pb-4" width="180" height="140" alt="...">
                  </figure>
                </div>
              </a>
              <?php } ?>
            </div>
            <div class="col-md-4 my-2">
              <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
              <!-- Recurso 3 -->
              <a class="link-offset-2 link-underline link-underline-opacity-0 d-block h-100" href="<?php echo get_field('recurso_3_linque'); ?>" target="_blank">
                <div class="borda-dir-topo d-flex align-items-center justify-content-center h-100">
                  <figure class="figure p-3">
                    <figcaption class="figure-caption text-center py-3">
                      <h5>
                      <?php //echo get_field('recurso_3_titulo'); ?>
                      <?php 
                      // Verificar o idioma atual
                      $current_language = pll_current_language();
                      
                      // Obter o subtítulo com base no idioma
                      if($current_language == 'pt_BR') {
                      echo get_field('recurso_3_titulo');
                      } elseif($current_language == 'es') {
                      echo get_field('recurso_3_titulo_es');
                      } elseif($current_language == 'en') {
                      echo get_field('recurso_3_titulo_en');
                      } else {
                      // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                      echo get_field('recurso_3_titulo');
                      }
                      ?>
                      </h5>
                    </figcaption>
                    <img src="<?php echo get_field('recurso_3_imagem'); ?>" class="img-fluid py-3" width="180" height="140" alt="...">
                  </figure>
                </div>
              </a>
              <?php } ?>
            </div>
          </div>
        </div>
        <!-- /Fim da Linha 10 -->
      </section>
    </main>
    <!-- Espaço -->
    <div class="espaco-80"></div>
    <?php endwhile; ?>
    <?php endif; ?>

<?php get_footer(); ?>
<?php $page = 'aprendizagem';
/* Template Name: Aprendizagem
 * @package escolha-livre
 */
?>

        <?php get_header(); ?>

        <main id="page-aprendizagem" class="container pb-5">

            <!-- Espaço -->
            <div class="espaco-80"></div>

            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                    <div class="col-12 separador">
                        <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                            <?php esc_html_e(single_post_title('', false)); ?>
                        </h1>
                    </div>
                </div>
            <!-- /Fim da Linha 1 -->

            <!-- Espaço -->
            <div class="espaco-60"></div>

            <!-- Linha 2 -->
            <div class="row">
                <!-- Coluna Migalhas de pão -->
                <div class="migalhas col-md-12 pb-5">

                    <!-- Migalhas de pão -->
                    <nav aria-label="breadcrumb">
                        <!-- .linque-verde - Cor do hover -->
                        <ol class="linque-verde breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?php echo home_url(); ?>/">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo home_url(); ?>/sobre">Sobre</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo home_url(); ?>/livre-e-aberto">livre, grátis ou aberto?</a>
                            </li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?php echo home_url(); ?>/aprendizagem">Aprendizagem</a>
                            </li>
                        </ol>
                    </nav>

                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="mb-4">
                    As liberdades permitidas pelo software livre fazem com que ele tenha um enorme potencial educativo. Primeiro, com o software livre, alunos e professores podem ter acesso ao recurso sem dificuldades ou limitações para instalar o software onde e quantas vezes quiserem. Reduzimos a barreira de acesso. Segundo, como o código está sempre disponível, abrem-se possibilidades “mão na massa” – isto é, de estudar o código, de alterar, remixar e reutilizar. Torna-se muito mais fácil e instigante trabalhar a programação, por exemplo, em atividades de desenvolvimento web, de robótica, dentre outras. Essas possibilidades, encorajadas pelo software livre, permitem um olhar crítico, curioso e participativo, colaborativo e autoral com relação às tecnologias.
                    </p>
                    <p class="mb-4">
                    A Competência Geral 5, da Base Nacional Comum Curricular, aponta para a relevância dessa possibilidade aberta pelo software livre, ao indicar que alunos devem:  
                    </p>
                    <p class="mb-4">
                    compreender, utilizar e criar tecnologias digitais de informação e comunicação de forma crítica, significativa, reflexiva e ética nas diversas práticas sociais (incluindo as escolares) para se comunicar, acessar e disseminar informações, produzir conhecimentos, resolver problemas e exercer protagonismo e autoria na vida pessoal e coletiva. 
                    </p>
                    <p class="mb-4">
                    Mais especificamente, a Competência Específica 7 de Linguagens e Tecnologias aponta que alunos devem:
                    </p>
                    <p class="mb-4">
                    Mobilizar práticas de linguagem no universo digital, considerando as dimensões técnicas, críticas, criativas, éticas e estéticas, para expandir as formas de produzir sentidos, de engajar-se em práticas autorais e coletivas, e de aprender a aprender nos campos da ciência, cultura, trabalho, informação e vida pessoal e coletiva.
                    </p>
                    <p class="mb-4">
                    É muito comum encontrar um ótimo um recurso que, às vezes, precisa de ajustes para ser utilizado com mais eficácia; um software ou recurso que precisa ser traduzido, que necessita de uma pequena alteração no funcionamento, em uma imagem ou outra funcionalidade. Com o software livre, o código aberto e os REA, isso se torna possível!
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

        </main>

        <?php get_footer(); ?>
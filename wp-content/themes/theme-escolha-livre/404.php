<?php $page = '404';
/* Template Name: 404
 * @package escolha-livre
 */
?>
<?php get_header(); ?>
<main id="page-404" class="text-wrap">
  <section>
	<div class="container">
		<div class="row">
		  <div class="col-md-12 mt-5">
				<h1 class="fs-1 my-5 pb-5 text-wrap">Oops a página não foi encontrada.</h1>
				<span class="col-sm-6 col-lg-3 pb-sm-5 text-lg-start text-sm-center linque linque-rodape">
                    <a class="d-inline-block btn mb-5 py-2 px-3 text-light bg-danger" href="<?php echo esc_url(); ?>/">
                        Voltar para a página inicial
                    </a>
                </span>			  
		  </div>
		</div>
    </div>
  </section>
  
 </main>
<?php get_footer(); ?>

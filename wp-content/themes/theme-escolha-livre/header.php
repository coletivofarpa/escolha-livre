<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <?php // force Internet Explorer to use the latest rendering engine available ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php bloginfo('name'); ?></title>
    <?php facebook_open_graph(); ?>
    <?php // mobile meta (hooray!) ?>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- REGISTRANDO E ENFILEIRANDO Bibliotecas, Scripts e Estilos corretamente a partir do functions.php -->
    <?php wp_head(); ?>

    <!-- Icone favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

    <!-- Fontes tipográficas auto-hospedadas -->
    <!-- Fontes MontSerrat -->
    <link href="<?php echo get_template_directory_uri(); ?>/library/fonts/Montserrat/Montserrat-VariableFont_wght.ttf" as="font" type="font/ttf" crossorigin>
    <link href="<?php echo get_template_directory_uri(); ?>/library/fonts/Montserrat/Montserrat-Italic-VariableFont_wght.ttf" as="font" type="font/ttf" crossorigin>
    <!-- Fontes Neris -->
    <link href="<?php echo get_template_directory_uri(); ?>/library/fonts/NerisFreeWeigths/Neris-Thin.otf" as="font" type="font/otf" crossorigin>
    <!-- Fontes Open Sans -->
    <link href="<?php echo get_template_directory_uri(); ?>/library/fonts/Open_Sans/OpenSans-VariableFont_wdth,wght.ttf" as="font" type="font/ttf" crossorigin>

    <!-- CDN|Fonts Fonts CDN -->
    <!-- Neris -->
    <link href="https://fonts.cdnfonts.com/css/neris" rel="stylesheet">
    <!-- Google Fonts CDN -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!-- Fonts Montserrat -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,900;1,300;1,400;1,500;1,600;1,900&display=swap" rel="stylesheet">
    <!-- Fonts Open Sans -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
  </head>

  <body <?php body_class(); ?>>

    <div id="header">
      <header>
        <div class="border-bottom">
          <div class="container">
            <!-- Barra de navegação desktop -->
            <nav class="navbar navbar-expand-lg justify-content-md-between">

              <!-- Logotipo Brand -->
              <div class="py-1 px-1">
                  <a class="navbar-brand d-inline-block align-text-top" href="<?php echo home_url(); ?>/">
                      <?php
                      $custom_logo_id = get_theme_mod('custom_logo');
                      $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                      if ($logo) {
                          echo '<img id="img-logo" src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '" class="img-fluid custom-logo logo-grande" />';
                      } else {
                          echo '<h1>' . get_bloginfo('name') . '</h1>';
                      }
                      ?>
                  </a>
              </div>

              <!-- Barra de navegação -->
              <div id="navbarNav" class="escrivania collapse navbar-collapse justify-content-lg-end">
                <ul class="navbar-nav align-items-center">
                  <li class="recursos nav-item">
                    <a class="recursos-link nav-link text-decoration-none" href="<?php echo esc_url(home_url('/recursos/')); ?>">
                      <?php echo esc_html( pll__( 'Recursos', 'theme-escolha-livre' )); ?>
                    </a>
                  </li>
                  <li class="aprenda nav-item">
                    <a class="aprenda-link nav-link text-decoration-none" href="<?php echo esc_url(home_url('/aprenda/')); ?>">
                      <?php echo esc_html( pll__( 'Aprenda', 'theme-escolha-livre' )); ?>
                    </a>
                  </li>
                  <li class="tutoriais nav-item">
                    <a class="tutoriais-link nav-link text-decoration-none" href="<?php echo esc_url(home_url('/tutoriais/')); ?>">
                      
                      <?php echo esc_html( pll__( 'Tutoriais', 'theme-escolha-livre' )); ?>
                    </a>
                  </li>
                  <li class="sobre nav-item">
                    <a class="sobre-link nav-link text-decoration-none" href="<?php echo esc_url(home_url('/sobre/')); ?>">
                      <?php echo esc_html( pll__( 'Sobre', 'theme-escolha-livre' )); ?>
                    </a>
                  </li>
                </ul>
              </div>

              <!-- Botões de pesquisa, contato e seleção de idioma -->
              <div class="navbar-nav d-flex flex-row align-items-center justify-content-end">
                <a class="nav-link lupa align-items-center justify-content-center text-align-center text-center align-bottom text-decoration-none" href="<?php echo esc_url(home_url('/pesquisar/')); ?>"></a>

                <a class="nav-link correios align-items-center justify-content-center text-align-center text-center align-bottom text-decoration-none" href="<?php echo esc_url(home_url('/contato/')); ?>"></a>

                <!-- Botão sanduiche (Toggle button) para dispositivos pequenos -->
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon botao-sanduiche"></span>
                </button>

                <!-- Seleção de idioma -->
                <?php if ( function_exists('pll_the_languages') ) : ?>
                  <div class="language-switcher p-0">
                    <?php $languages = pll_the_languages( array( 'raw' => 1 ) ); ?>
                    <?php if ( $languages ) : ?>
                      <ul class="list-unstyled mb-0">
                        <?php foreach ( $languages as $language ) : ?>
                          <!-- Define uma classe baseada no slug do idioma -->
                          <?php $class = 'language-item language-' . esc_attr( $language['slug'] ); ?>

                          <!-- Verifica se o idioma atual é o idioma atual do WordPress -->
                          <?php if ( $language['current_lang'] ) : ?>
                            <?php $class .= ' current-language'; ?><!-- Adiciona uma classe para estilização adicional -->
                          <?php endif; ?>
                          <li class="<?php echo esc_attr( $class ) ; ?> idiomas align-items-center justify-content-center text-align-center text-center align-bottom">
                            <a class="text-decoration-none mt-5" href="<?php echo esc_url( $language['url'] ); ?>">
                              <?php echo esc_html( strtoupper( $language['slug'] ) ); ?>
                            </a>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              </div>
            </nav>
            <!-- Fim da barra de navegação desktop -->
          </div>
        </div>
        <!-- Barra de navegação móvel -->
        <nav class="portatil navbar navbar-expand-lg justify-content-md-between m-1">
          <div id="navbarNav" class="collapse col-12 navbar-collapse justify-content-lg-center borda-dir-topo">
            <ul class="navbar-nav align-items-center">
              <li class="recursos nav-item w-100 m-0 border">
                <a class="recursos-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/recursos/')); ?>">
                  <?php echo esc_html( pll__( 'Recursos', 'theme-escolha-livre' )); ?>
                </a>
              </li>
              <li class="aprenda nav-item w-100 m-0 border">
                <a class="aprenda-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/aprenda/')); ?>">
                  <?php echo esc_html( pll__( 'Aprenda', 'theme-escolha-livre' )); ?>
                </a>
              </li>
              <li class="tutoriais nav-item w-100 m-0 border">
                <a class="tutoriais-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/tutoriais/')); ?>">
                  <?php echo esc_html( pll__( 'Tutoriais', 'theme-escolha-livre' )); ?>
                </a>
              </li>
              <li class="sobre nav-item w-100 m-0 border">
                <a class="sobre-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/sobre/')); ?>">
                  <?php echo esc_html( pll__( 'Sobre', 'theme-escolha-livre' )); ?>
                </a>
              </li>
              <li class="sobre nav-item w-100 m-0 border">
                <a class="sobre-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/pesquisar/')); ?>">
                  <?php echo esc_html( pll__( 'Pesquisar', 'theme-escolha-livre' )); ?>
                </a>
              </li>
              <li class="sobre nav-item w-100 m-0 border">
                <a class="sobre-link nav-link text-decoration-none d-flex justify-content-center align-items-center h-100 p-0  text-muted" href="<?php echo esc_url(home_url('/contato/')); ?>">
                  <?php echo esc_html( pll__( 'Contato', 'theme-escolha-livre' )); ?>
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <!-- Fim da barra de navegação mobile -->
      </header>
    </div>

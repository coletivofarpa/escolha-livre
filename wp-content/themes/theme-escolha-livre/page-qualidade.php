<?php $page = 'qualidade';
/* Template Name: Qualidade
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-qualidade" class="container pb-5">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <!-- Coluna Migalhas de pão -->
                <div class="migalhas col-md-12 pb-5">

                    <!-- Migalhas de pão -->
                    <nav aria-label="breadcrumb">
                        <!-- .linque-verde - Cor do hover -->
                        <ol class="linque-verde breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/sobre">Sobre</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/livre-eaberto">livre,  grátis ou aberto?</a>
                            </li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="<?php echo get_site_url(); ?>/qualidade">Qualidade</a>
                            </li>
                        </ol>
                    </nav>

                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="mb-2">
                        Hoje, é muito difícil encontrar uma empresa de software que não crie ou incorpore software livre em suas produções. Isso vale para gigantes internacionais como Google, Facebook e Microsoft, bem como para empresas menores, organizações da sociedade civil, prestadores de serviço independentes e instituições públicas. Para gestores da área de tecnologia, o uso de software livre é imprescindível. Um bom exemplo de quão robusto pode ser um software livre é muito recente: o Falcon 9, foguete utilizado pela SpaceX em sua recente missão espacial, roda Linux – um software livre!
                    </p>
                    <p class="mb-2">
                        Da mesma forma, inúmeros repositórios e sites de qualidade são abertos. O portal de recursos educacionais do Ministério da Educação (MEC-RED) e o portal de recursos educacionais da CAPES (EduCAPES), por exemplo, são feitos em software livre (DSpace), tendo sido criados com a lógica dos REA. Parte dos recursos criados por editoras para o Plano Nacional do Livro Didático (PNLD), um dos maiores programas de distribuição de livros didáticos do mundo, também são REA. Como já apontado pela UNESCO, REA são parte de uma perspectiva de desenvolvimento sustentável. Não por menos, REA fazem parte integral da Estratégia Brasileira para a Transformação Digital publicada pelo Ministério de Ciência, Tecnologia, Inovações e Comunicações em 2018.
                    </p>
                    <p class="mb-2">
                        Para usuários, como educadores e alunos, ainda há um estigma de que qualquer coisa livre venha a ser um produto de menor qualidade – algo que se usa quando não há alternativa comercial. Essa percepção está equivocada (leia mais). Em muitos mercados e áreas de atuação, o software livre é o padrão e o melhor software disponível. E em quase todos os outros casos, existem alternativas livres de alta e comparável qualidade para quase qualquer software que você está acostumado a utilizar regularmente.
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

        </main>

<?php get_footer(); ?>
<?php get_header(); ?>

<main id="archive">
  <section>
	<div class="container">
		<div class="row">
		  <div class="col-md-12">
			<h1><?php 
				$category = str_replace("Category: ", "", get_the_archive_title()); 
				$category = str_replace("Categoria: ", "", $category); 
				echo $category;
				?>
			</h1>
			<?php
			the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>		  
		  </div>
		</div>
		<div class="row mt-5">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="col-md-4">
				<a href="<?php the_permalink() ?>">
					<div class="card">
						<?php $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) ); ?>
						<div class="img" style="background-image: url('<?php echo $image; ?>')"></div>
						<div class="card-body">
							<h2><?php the_title(); ?></h2>
							<p class="card-text"><?php get_field('resume')?></p>
						</div>
					</div>
				</a>
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
    </div>
  </section>
 </main>


<?php get_footer(); ?>

<?php $page = 'sobre';
/* Template Name: Sobre 
 * @package escolha-livre
 */
get_header(); ?>

		<main id="page-sobre" class="container main-sobre" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="row">

				<!-- Espaço -->
				<div class="espaco-80"></div>

				<header class="cabecalho">
					<div class="titulo-h1 d-flex align-items-center justify-content-center">
						<div class="col-12 separador">
							<h1 class="text-uppercase me-5 text-decoration-none text-muted">
								<?php //esc_html_e(single_post_title('', false)); ?>
								<?php
									// Permitindo html seguro ao filtrar/ascapar tags inseguras 
									$title = get_the_title();
									$allowed_tags = array(
										'br' => array(),
										'a' => array(
										'href' => array(),
										'title' => array()
										),
										'em' => array(),
										'strong' => array(),
										'p' => array(),
										'span' => array(),
										// Adicione outras tags permitidas aqui, se necessário
									);
									echo wp_kses($title, $allowed_tags);
								?>
							</h1>
						</div>
					</div>
				</header> <?php // end article header ?>

				<!-- Espaço -->
				<div class="espaco-60"></div>

				<div class="texto-div col-12">

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<section class="entry-content cf" itemprop="articleBody">
							<?php
								// the content (pretty self explanatory huh)
								the_content();

								/*
								* Link Pages is used in case you have posts that are set to break into
								* multiple pages. You can remove this if you don't plan on doing that.
								*
								* Also, breaking content up into multiple pages is a horrible experience,
								* so don't do it. While there are SOME edge cases where this is useful, it's
								* mostly used for people to get more ad views. It's up to you but if you want
								* to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
								*
								* http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
								*
								*/
								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'theme-escolha-livre' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
								) );
							?>
						</section> <?php // end article section ?>

						<footer class="article-footer cf">

						</footer>

						<?php //comments_template(); ?>

					</article>

					<!-- Espaço -->
					<div class="espaco-40"></div>

				</div>
				<?php endwhile; endif; ?>
				
				<div class="row">
					<?php
							$args = array(
								'post_type'   => 'sobre',
								'post_status' => 'publish',
								'orderby'     => array(
									'date' =>'DESC'
								)
							);

							$data = new WP_Query( $args );
							if( $data->have_posts() ) :
							?>

							<?php
							while( $data->have_posts() ) :
								$data->the_post();
								$post_content = get_the_content();
								$resumo = get_field('resumo');
								$resumo_es = get_field('resumo_es');
								$resumo_en = get_field('resumo_en');

								?>
						<div class="col-md-6 mb-3">
							<a class="card mt-2 text-decoration-none text-muted me-5" id="contexto-sobre" href="<?php echo get_permalink()?>">
								<div class="card-header sobre"> <?php echo get_the_title()?> </div>
								<div class="card-body texto-cards align-items-center justify-content-center">
								<?php //$resumo?>
								<?php 
									// Verificar o idioma atual
									$current_language = pll_current_language();
									
									// Obter o subtítulo com base no idioma
									if($current_language == 'pt_BR') {
										echo $resumo;
									} elseif($current_language == 'es') {
										echo $resumo_es;
									} elseif($current_language == 'en') {
										echo $resumo_en;
									} else {
										// Caso o idioma não seja encontrado, exibir o subtítulo padrão
										echo $resumo;
									}
								?>
								</div>
							</a>
						</div>
						<?php
							endwhile;
							wp_reset_postdata();
							endif;
							?>
					</div>
				</div>

			<!-- Espaço -->
			<div class="espaco-80"></div>

		</main>

		<?php get_footer(); ?>
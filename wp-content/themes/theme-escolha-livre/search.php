<?php get_header(); ?>

		<div id="content">

      <div class="espaco-80"></div>

			<div id="inner-content" class="wrap cf container">

				<main id="main" class="m-all t-2of3 d-5of7 cf" role="main">
  
          <div id="resultado">
            
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
              <div class="col-12 separador">
                <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                  Pesquisar
                </h1>
              </div>
            </div>

            <div class="espaco-80"></div>

            <section id="buscar">
              <!-- Linha 2 barra de pesquisa -->
              <form role="search" method="get" class="search-form" action="<?php echo esc_url(pll_home_url('/')); ?>">
                <div class="row">
                  <div class="col-12">
                    <div class="input-group mb-3">
                      <input type="search" class="form-control" id="campo-busca" id="s" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e( pll__( 'digite aqui o que quer pesquisar', 'theme-escolha-livre' )); ?>" />
                      <span class="input input-group-btn">
                        <button type="submit" id="botao-busca" class="btn align-items-center justify-content-center">
                                      
                          <svg xmlns:xlink="http://www.w3.org/1999/xlink" fill="none" width="40" xmlns="http://www.w3.org/2000/svg" style="-webkit-print-color-adjust:exact" id="screenshot-634cb711-062a-8080-8002-60898dbd88f3" version="1.1" viewBox="5965.5 867.5 40 32" height="32">
                              <g id="shape-634cb711-062a-8080-8002-60898dbd88f3" rx="0" ry="0"><g id="shape-634cb711-062a-8080-8002-60898dbdc92e"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92e"><ellipse rx="11.839810094838867" ry="11.939804420205178" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92e" class="strokes"><g class="stroke-shape"><ellipse rx="11.839810094838867" ry="11.939804420205178" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc92f"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92f"><ellipse rx="8.75116398314185" ry="8.825072832325418" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92f" class="strokes"><g class="stroke-shape"><ellipse rx="8.75116398314185" ry="8.825072832325418" style="fill:none;fill-opacity:none;stroke-width:0.5;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc930"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc930"><path rx="0" ry="0" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc930" class="strokes"><g class="stroke-shape"><path rx="0" ry="0" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g></g></g></g>
                          </svg>

                          <?php esc_html_e( pll__( 'Pesquisar', 'theme-escolha-livre' )); ?>
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </form>
              <!-- /Fim da Linha 2 -->

              <!-- Linha 3 Filtro por idioma -->
              <div class="row py-5 idiomas">
                <div class="col-12">
                  <h2 class="pb-3">
                    <?php echo esc_html( pll__( 'Escolha o idioma', 'theme-escolha-livre' )); ?>
                  </h2>
                </div>

                <!-- Botões do Polylang -->
                <?php if ( function_exists('pll_the_languages') ) : ?>
                <ul class="polylang-buttons row">
                  <?php $languages = pll_the_languages( array( 'raw' => 1 ) ); ?>
                  <?php if ( $languages ) : ?>
                  <?php foreach ( $languages as $language ) : ?>
                  <?php 
                    // Verifica a existência das chaves 'slug', 'locale', 'url', 'name' e 'current_lang'
                    $slug = isset($language['slug']) ? $language['slug'] : '';
                    $locale = isset($language['locale']) ? $language['locale'] : '';
                    $url = isset($language['url']) ? $language['url'] : '#';
                    $name = isset($language['name']) ? $language['name'] : '';
                    $current_lang = isset($language['current_lang']) && $language['current_lang'] ? 'current-lang' : '';
                  ?>
                  <div class="col-md-4">
                    <a class="linques <?php echo esc_attr( $slug ); ?>" hreflang="<?php echo esc_attr( $locale ); ?>" href="<?php echo esc_url( $url ); ?>" lang="<?php echo esc_attr( $locale ); ?>">
                      <li class="my-2 borda-dir-base lang-item <?php echo esc_attr( $slug ); ?> <?php echo esc_attr( $current_lang ); ?>">
                        <h3 class="py-4">
                          <?php echo esc_html( $name ); ?>
                        </h3>
                      </li>
                    </a>
                  </div>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </ul>
                <?php endif; ?>

              </div>
            </section>

            <div class="archive-title titulo-h1 d-flex align-items-center justify-content-center">
              <div class="col-12 separador">

                <h2 class="text-decoration-none text-muted">

                  <?php _e( 'Resultados da busca por:', 'theme-escolha-livre' ); ?>

                  <strong><?php echo esc_attr(get_search_query()); ?></strong>

                </h2>

              </div>
            </div>

            <div class="posts-wrapper">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <a class="post-linque" href="<?php the_permalink(); ?>">
                  <article id="post-<?php the_ID(); ?>" <?php post_class('cf resultado'); ?> role="article">

                    <header class="entry-header article-header">
                      <h3 class="search-title entry-title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      </h3>
                    </header>

                    <section class="entry-content">
                      <?php 
                      // Verifica se o campo personalizado "resumo" do ACF está presente e o exibe
                      if(get_field('resumo')) {
                        echo '<p>' . get_field('resumo') . '</p>';
                      } else {
                        // Se não houver resumo do ACF, exibe o excerto padrão do WordPress
                        the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;', 'theme-escolha-livre' ) . '</span>' );
                      }
                      ?>
                    </section>

                    <footer class="article-footer">
                      <?php if(get_the_category_list(', ') != ''): ?>
                        <?php printf( __( 'Filed under: %1$s', 'theme-escolha-livre' ), get_the_category_list(', ') ); ?>
                      <?php endif; ?>
                      <?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'theme-escolha-livre' ) . '</span> ', ', ', '</p>' ); ?>
                    </footer> <!-- end article footer -->

                  </article>
                </a>

              <?php endwhile; ?>

                <?php //bones_page_navi(); ?>

              <?php else : ?>
                <article id="post-not-found" class="hentry cf">
                  <header class="article-header">
                    <h1><?php esc_html_e( 'Sorry, No Results.', 'theme-escolha-livre' ); ?></h1>
                  </header>
                  <section class="entry-content">
                    <p><?php esc_html_e( 'Try your search again.', 'theme-escolha-livre' ); ?></p>
                  </section>
                  <footer class="article-footer">
                    <p><?php esc_html_e( 'This is the error message in the search.php template.', 'theme-escolha-livre' ); ?></p>
                  </footer>
                </article>
              <?php endif; ?>

            </div> <!-- Fechando a div 'posts-wrapper' -->
            
        </main>

      </div>

      <div class="espaco-80"></div>

    </div>

<?php get_footer(); ?>
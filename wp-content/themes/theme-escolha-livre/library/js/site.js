// Modo de compatibilidade jQuery
/*
$.noConflict();
jQuery(document).ready(function(){
  jQuery("button").click(function(){
jQuery("p").text("jQuery is still working!");
  });
});
*/
// Menu cabeçalho
document.addEventListener("DOMContentLoaded", function() {
    var currentUrl = window.location.href;
    var links = document.querySelectorAll('.navbar-nav .nav-link');
    links.forEach(function(link) {
        if (link.getAttribute('href') === currentUrl) {
            link.classList.add('active');
        }
        link.addEventListener('click', function() {
            links.forEach(function(el) {
                el.classList.remove('active');
            });
            this.classList.add('active');
        });
    });
});

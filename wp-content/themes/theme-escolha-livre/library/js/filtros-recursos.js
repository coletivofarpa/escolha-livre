// Filtros por categorias e temas da página Recursos
var selectedThemes = [];
var selectedCategories = [];

filterPage = function(id, type) {
    var $filter = jQuery('.lnk-' + id);
    var isAlreadyClicked = $filter.hasClass('filter-click');

    // Hide all posts
    jQuery('.list-all').hide();

    if (isAlreadyClicked) {
        // If already clicked, remove selected classes for the clicked filter
        $filter.removeClass('filter-click selected');

        // Remove the clicked item from the selected array
        if (type === 'categoria') {
            selectedCategories = selectedCategories.filter(function(category) {
                return category !== id;
            });
        } else if (type === 'tema') {
            selectedThemes = selectedThemes.filter(function(theme) {
                return theme !== id;
            });
        }
    } else {
        // If not clicked, remove selected classes of the opposite type
        if (type === 'categoria') {
            jQuery('.filter-click.categoria').removeClass('filter-click selected');
            selectedCategories = [id];
        } else if (type === 'tema') {
            jQuery('.filter-click.tema').removeClass('filter-click selected');
            selectedThemes = [id];
        }

        // Add selected classes for the clicked filter
        $filter.addClass('filter-click selected');
    }

    // Show posts with the selected themes and categories
    if (selectedThemes.length > 0 || selectedCategories.length > 0) {
        var selector = selectedThemes.concat(selectedCategories).join('.');
        jQuery('.' + selector).show();
    } else {
        // If no themes or categories are selected, show all posts
        jQuery('.list-all').show();
    }
}
// Fim dos filtros por categorias e temas da página Recursos
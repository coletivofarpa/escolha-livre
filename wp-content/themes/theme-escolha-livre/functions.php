<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );
require_once( 'includes/escolha-livre.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  load_theme_textdomain( 'theme-escolha-livre', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/custom-post-type.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px', 'theme-escolha-livre'),
        'bones-thumb-300' => __('300px by 100px', 'theme-escolha-livre'),
    ) );
}

/*
pt_BR
A função acima adiciona a capacidade de usar o menu suspenso para selecionar os novos tamanhos de imagens que você acabou de criar no gerenciador de mídia ao adicionar mídia aos blocos de conteúdo. Se você adicionar mais tamanhos de imagem, duplique uma das linhas da matriz e nomeie-a de acordo com o novo tamanho de imagem.

EN
The function above adds the ability to use the dropdown menu to select the new images sizes you have just created from within the media manager when you add media to your content blocks. If you add more image sizes, duplicate one of the lines in the array and name it according to your new image size.
*/

/************* THEME CUSTOMIZE *********************/

/* 
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722
  
  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162
  
  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors', 'theme-escolha-livre' );
  // $wp_customize->get_section('background_image')->title = __( 'Images', 'theme-escolha-livre' );
}

add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'theme-escolha-livre' ),
		'description' => __( 'The first (primary) sidebar.', 'theme-escolha-livre' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'theme-escolha-livre' ),
		'description' => __( 'The second (secondary) sidebar.', 'theme-escolha-livre' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo esc_url(get_template_directory_uri()); ?>/library/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'theme-escolha-livre' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'theme-escolha-livre' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'theme-escolha-livre' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'theme-escolha-livre' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!


/*
This is a modification of a function found in the
twentythirteen theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
function bones_fonts() {
  wp_enqueue_style('nerisFont', 'https://fonts.cdnfonts.com/css/neris');
  wp_enqueue_style('openSansFont', 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap');
  wp_enqueue_style('montserratFont', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
}

add_action('wp_enqueue_scripts', 'bones_fonts');


function get_custom_url(){
  return "";
}

function get_activate_link($lan){
  return "";
}

function get_language(){
  return "";
}

add_action( 'init', 'lc_register_movie_post_type' );

// CUSTOM POSTS
// A custom function that calls register_post_type
function lc_register_movie_post_type() {

  // RECURSOS
  // Define várias partes do texto, $labels é usado dentro da matriz $args
  // Set various pieces of text, $labels is used inside the $args array
  $labels = array(
     'name' => _x('Recursos', 'Nome geral do tipo de post | post type general name', 'theme-escolha-livre'),
     'singular_name' => _x('Recurso', 'Nome singular do tipo de post | post type singular name', 'theme-escolha-livre'),
  );
  // Define várias informações sobre o tipo de postagem
  // Set various pieces of information about the post type
  $args = array(
    'labels' => $labels,
    'description' => 'Meu tipo de post personalizado | My custom post type',
    'public' => true,
    'has_archive' => false, // Desativa a arquivamento (archive)
    'publicly_queryable' => false, // Impede a consulta pública individual
//    'rewrite' => array('slug' => 'recursos'), // Adiciona a reescrita de URLs (amigáveis)
    'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'),
  );
  // Registra o tipo de postagem de 'recurso' com todas as informações contidas na matriz $arguments
  // Register the 'Recurso' post type with all the information contained in the $arguments array
  register_post_type('recurso', $args);
  // END RECURSOS

  // TUTORIAIS 
  $labels = array(
    'name' => _x( 'Tutoriais', 'post type general name', 'theme-escolha-livre' ),
    'singular_name' => _x( 'Tutorial', 'post type singular name', 'theme-escolha-livre' ),
  );
  // Set various pieces of information about the post type
  $args = array(
   'labels' => $labels,
   'description' => 'My custom post type',
   'public' => true,
  );
  // Register the movie post type with all the information contained in the $arguments array
  register_post_type( 'tutorial', $args );
  // END TUTORIAIS

  // APRENDA 
  $labels = array(
    'name' => _x( 'Aprenda', 'post type general name', 'theme-escolha-livre' ),
    'singular_name' => _x( 'Aprenda', 'post type singular name', 'theme-escolha-livre' ),
  );
  // Set various pieces of information about the post type
  $args = array(
   'labels' => $labels,
   'description' => 'My custom post type',
   'public' => true,
  );
  // Register the movie post type with all the information contained in the $arguments array
  register_post_type( 'aprenda', $args );
  // END APRENDA

  // SOBRE 
  $labels = array(
    'name' => _x( 'Sobre', 'post type general name', 'theme-escolha-livre' ),
    'singular_name' => _x( 'Sobre', 'post type singular name', 'theme-escolha-livre' ),
  );
  // Set various pieces of information about the post type
  $args = array(
  'labels' => $labels,
  'description' => 'My custom post type',
  'public' => true,
  );
  // Register the movie post type with all the information contained in the $arguments array
  register_post_type( 'sobre', $args );
  // END SOBRE
}

//Add Open Graph Meta Info from the actual article data, or customize as necessary
function facebook_open_graph() {
  global $post;

  if ( !is_singular()) //if it is not a post or a page
      return;

  $excerpt = get_field('resume', $post->ID);

  if(!isset($excerpt)) {
    $excerpt = get_bloginfo('description');
  }

  //You'll need to find you Facebook profile Id and add it as the admin
  // echo '<meta property="fb:admins" content="XXXXXXXXX-fb-admin-id"/>';
  echo '<meta property="og:title" content="' . get_the_title() . '"/>' . "\n";
  echo '<meta property="og:description" content="' . $excerpt . '"/>' . "\n";
  echo '<meta property="og:type" content="article"/>' . "\n" . "\n";
  echo '<meta property="og:url" content="' . get_permalink() . '"/>' . "\n";
  //Let's also add some Twitter related meta data
  echo '<meta name="twitter:card" content="summary" />' . "\n";
  //This is the site Twitter @username to be used at the footer of the card
  //echo '<meta name="twitter:site" content="@escolhalivre" />' . "\n";
  //This the Twitter @username which is the creator / author of the article
  //echo '<meta name="twitter:creator" content="@escolhalivre" />' . "\n";
  
  // Customize the below with the name of your site
  echo '<meta property="og:site_name" content="Escolha Livre"/>' . "\n";
  if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    //Create a default image on your server or an image in your media library, and insert it's URL here
    $default_image="TODO"; 
    echo '<meta property="og:image" content="' . $default_image . '"/>' . "\n";
    echo '<meta name=”twitter:image” content="'.$default_image.'">' . "\n";
  }
  else{
      $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
      echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>' . "\n";
      echo '<meta name=”twitter:image” content="'.esc_attr( $thumbnail_src[0] ).'">' . "\n";
  }

}

function calcPost($count){
  // TODO
  // realiza o calculo da div
  switch ($count) {
      case 0:
          $class="class0";
          break;
      case 1:
          $class="class1";
          break;
      default:
          $class="classDefault";
          break;
  }
  return $class;
}

function formatString($str) {
    // Remove espaços em branco e caracteres especiais
    $str = preg_replace('/[^a-zA-Z0-9]+/', '', $str);
    
    // Converte para minúsculas
    $str = strtolower($str);
    
    return $str;
}

/************* TEMPLATES PARTS *********************/
// Função para incluir todos os componentes do diretório "componentes/"
function include_all_components() {
    $component_dir = get_template_directory() . '/template/componentes/';

    // Verifica se o diretório existe
    if (is_dir($component_dir)) {
        // Obtém todos os arquivos no diretório
        $files = glob($component_dir . '*.php');

        // Inclui cada arquivo encontrado
        foreach ($files as $file) {
            include_once $file;
        }
    }
}

// Adiciona a ação para chamar a função em algum ponto do ciclo de vida do WordPress
add_action('init', 'include_all_components');
/* ### Fim da inclusão de todos os componentes presentes no diretório `componentes/` ### */


/**************** REGISTRANDO E ENFILEIRANDO Bibliotecas, Scripts e Estilos corretamente ****************/
function theme_enqueue_scripts() {
  // Bootstrap CSS via CDN
  wp_enqueue_style( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css', array(), '5.3.3' );

  // Font Awesome
  wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0' );

  // SimpleLightbox
  wp_enqueue_style( 'simple-lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.4/simple-lightbox.min.css', array(), '2.10.4' );

  // Estilos personalizados
  wp_enqueue_style( 'custom-styles', get_template_directory_uri() . '/library/css/estilos.css', array(), '1.0.0' );

  // jQuery
  wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.7.1.min.js', array(), '3.7.1', true );

  // Popper.js
  wp_enqueue_script( 'popper', 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js', array(), '2.11.8', true );

  // Bootstrap JS
  wp_enqueue_script( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js', array('jquery', 'popper'), '5.3.3', true );

  // SimpleLightbox JS
  wp_enqueue_script( 'simple-lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.4/simple-lightbox.min.js', array(), '2.10.4', true );

  // Script personalizado
  wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/library/js/site.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

// Função para registrar novos locais de menu
function registrar_locais_de_menu() {
  register_nav_menus(
      array(
          'miolo-escolha-livre' => __( 'Miolo Escolha Livre', 'theme-escolha-livre' ), // Adicione quantos locais de menu desejar
          // Você pode adicionar mais locais de menu aqui
      )
  );
}
// Adiciona a função ao gancho 'after_setup_theme'
add_action( 'after_setup_theme', 'registrar_locais_de_menu' );


/* DON'T DELETE THIS CLOSING TAG */ ?>

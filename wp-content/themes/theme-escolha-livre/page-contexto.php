<?php $page = 'contexto';
/* Template Name: Contexto 
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

<main id="page-contexto" class="container">

    <div class="titulo-h1 d-flex align-items-center justify-content-center">
        <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                <?php esc_html_e(single_post_title('', false)); ?>
            </h1>
        </div>
    </div>
    <div class="texto-div col-12 mb-5">
        <p>
            A pandemia da COVID19 colocou as tecnologias educacionais no centro das atenções de agências como a UNESCO, além de gestores, professores, pais e responsáveis. Mas, o campo das tecnologias educacionais não é recente – trata-se de uma tendência que vem se acelerando nos últimos anos. Estamos, professores e alunos, cada vez mais conectados e utilizando novas mídias na educação.
        </p>
        <br>
        <p>

            Os dados do estudo TIC Educação (CETIC.br), projeto que chega ao seu décimo ano, aponta um crescente uso de computadores e celulares, além da internet, por parte de alunos e professores – inclusive com propósitos educacionais – dentro e fora da escola. Tais dados indicam também que estratégias consideradas importantes no momento da pandemia, como o uso de vídeos e de tutoriais para aprender online, já são prática corrente para docentes.
        </p>
        <br>
        <p>

            Segundo o TIC Educação, a porcentagem de professores que utiliza vídeos e tutoriais para aprender sobre computadores e internet cresceu de 59% para 81% entre 2015 e 2019, período pré-pandemia (CETIC.br, 2016, 2019). De forma relevante, 55% dos alunos, em áreas urbanas, já haviam utilizado o celular para atividades escolares e 57% dos professores, também em áreas urbanas, indicaram já ter feito uso da internet no celular para atividades com alunos. Os dados apontam para a crescente conexão e o uso das novas mídias, bem como a maior utilização de dispositivos móveis por parte de alunos e professores. Mas, existem diferentes práticas, barreiras e limitações – um cenário diverso e desigual.
        </p>

    </div>
    <div class="col-12 d-flex justify-content-center">
        <div class="card centeredContent col-8 mb-5 d-flex flex-row align-items-end justify-content-center">
            <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/image.png.png">
            <p class="col-3">Saiba mais sobre as ações da UNESCO e o monitoramento do impacto da pandemia de COVID-19 em escolas e na educação global.</p>
        </div>
    </div>
    <div class="texto-div col-12 mt-3 mb-5">
        <p>
            No início da pandemia, o “aprender em casa” foi logo associado às estratégias de “educação à distância”. A EaD é uma modalidade centenária no Brasil, tendo sua história marcada, no que se refere aos veículos utilizados, pelo material impresso, o rádio ou a televisão, e, no que tange aos modos de aprendizado, por formas comunitárias presenciais (todos ao redor de um rádio, por exemplo) e independentes. Tanto iniciativas públicas quanto privadas investiram em modelos EaD.
        </p>
        <br>
        <p>
            Apesar do enorme potencial inclusivo da internet, ficou mais claro, nesse momento, o enorme fosso existente entre os que têm acesso ao mundo digital e condições de utilizá-lo com qualidade e os que não os têm: cenários de exclusão digital e social. Não por menos, houve imediato e renovado interesse na televisão e no rádio como formas mais equitativas de acesso ao conteúdo educacional, o que seria feito em conjunto com serviços disponíveis via internet. Esse momento nos ajudou também a perceber que o “presencial” não é necessariamente uma “melhor” modalidade de ensino, mas é, sobretudo, uma forma de garantia de acesso à educação para muitos alunos.
        </p>
        <br>
        <p>
            É importante pontuar a diferença entre os termos. A noção de uma educação a distância como sendo algo oposto à educação presencial perdeu força há algum tempo – particularmente, com a popularização da web. Novos termos surgiram, como “educação híbrida”, buscando mesclar as melhores e mais apropriadas práticas da EaD e do ensino presencial. Apesar da diversidade de usos desse termo, o híbrido remete a um “meio termo” entre dois polos opostos – EaD e presencial. Na prática, quase sempre que alguém usa o termo “educação híbrida” está falando do uso de novas mídias como algo suplementar: o modelo tradicional de educação presencial acrescido de plataformas online [1].
        </p>
        <p>
            Em relação ao momento presente, para pensar a cultura digital na educação, preferimos falar de uma Educação Aberta. A EA não parte da lógica de supremacia de um modelo (por exemplo, presencial) em detrimento de outros. Atualmente, nós definimos a Educação Aberta como:
        </p>
    </div>


    <div class="col-12 d-flex justify-content-center">
        <div class="card centeredContent col-8 mb-5 d-flex flex-row align-items-center justify-content-center">
            <p>Movimento histórico que busca atualizar princípios da educação progressiva na cultura digital. Promove a equidade, a inclusão e a qualidade através de práticas pedagógicas abertas apoiadas na liberdade de criar, usar, combinar, alterar e redistribuir recursos educacionais de forma colaborativa. Incorpora tecnologias e formatos abertos, priorizando o software livre. Nesse contexto, a Educação Aberta prioriza a proteção dos direitos digitais, incluindo o acesso à informação, a liberdade de expressão e o direito à privacidade.</p>
            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/educacaoDeQualidade.png" class="col-3">
        </div>
    </div>

    <div class="texto-div col-12 mt-3 mb-5">
        <p>
            Qualquer que seja o modelo – a distância, híbrido, aberto ou outros –, o planejamento precisa ser sistêmico, isto é, levar em consideração vários fatores interdependentes. Além disso, precisa também ser sistemático – pensado passo a passo – para que possa ser feito com qualidade. Isso é muito diferente do uso pontual ou emergencial de ferramentas para comunicação e informação, que vem a ser o cenário que estamos vivendo. Explorar ferramentas e plataformas hoje, no entanto, pode influenciar uma estratégia de médio e longo prazo para uma Educação Aberta. As decisões que estamos tomando, nesse momento, com relação às tecnologias que utilizamos e oferecemos aos nossos professores e alunos, têm implicações importantes para o futuro da educação. Elas geram compromissos, contratos, fidelização de alunos, controle sobre dados e outras questões importantes! Pensar e adotar o modelo Livre e Aberto, a partir da tecnologia, é um comprometimento ético com a educação.
        </p>
        <br>
        <br>
        <p>
            [1] SCHIEHL, E. P.; GASPARINI, I. Modelos de Ensino Híbrido: Um Mapeamento Sistemático da Literatura.
        </p>
        <br>

    </div>

</main>

<?php
get_footer();
?>
## Tema wordpress para o site da Escolha Livre

Repo https://gitlab.com/coletivofarpa/escolha-livre

Para rodar o conteiner em localhost via Docker-compose:

``` Bash
git clone https://github.com/sprintcube/docker-compose-lamp.git &&\
mv docker-compose-lamp/ lamp/ &&\
cd lamp/ &&\
cp sample.env .env &&\
docker-compose up -d
```
Clone o repositório do projeto no diretório `www/`:
``` Bash
git clone https://gitlab.com/coletivofarpa/escolha-livre.git
```

Abra o navegador web em http://localhost/ e crie um banco novo via PHPMyAdmin. Acesse http://localhost/escolha-livre e insira as credenciais do banco de dados:
``` Bash
database: nome _do_banco_que_voce_criou
username: root
password: tiger
host: database
```
Criei as páginas `Escolha Livre` (home), `Recursos`, `Aprenda`, `Tutoriais` e `Sobre` e popule o banco com postagens via painel admin em http://localhost/escolha-livre/wp-admin/

Instale as dependencia com NPM. No diretório `escolha-livre-theme/` rode o comando abaixo:
``` Bash
npm install
```

## Para compilar os arquivos de folha de estilo CSS via SASS:

Instale o pré-procesador Sass de forma global em sua máquina:
``` Bash
npm install -g sass
```
Abra o terminal no diretório raíz do tema `theme-escolha-livre/` e roda o comando referente ao seu sistema operaticional:
``` Bash
cd wp-content/themes/theme-escolha-livre/
``` 

No GNU OS com ou sem Linux:

``` Bash
sass --watch library/scss/estilos.scss:library/css/estilos.css --style compressed
```

Windows OS com git-bash ou similar:

``` Bash
sass scss/estilos.scss css/estilos.css --watch compressed
```

## Características do tema:

- Compatível com Bootstrap v5.3.3
- Compatível com jQuery
- ACF community
- Utiliza Sass
- Arquitetura modular
- Diretórios:
    - `includes/`
    - `template/`:
        - `partials/`
        - `blocos/`
    - `post-formats/`:
        - 
- Suporte ao editor de blocos Gutenberg
- Trilha de navegação/migalhas de pão (breadcrumbs)
- Trilíngue:
    - pt_BR (padrão Polylang)
    - ES
    - EN
- Composto por shortcodes:
    - `[titulos_h1_shortcode campo="nome_do_campo"]Texto do título[/titulos_h1_shortcode]`
    - `[audioplayer_shortcode campo="nome_do_campo"]`
    - `[imagem_legenda_shortcode campo="nome_do_campo"]`
    - `[legenda_imagem_shortcode campo="nome_do_campo"]`
    - `[banner_legenda_shortcode campo="nome_do_campo"]`
    - `[video_grande_shortcode campo="nome_do_campo"]`
    - `[audio_esquerda_shortcode campo="nome_do_campo"]`
    - `[audio_direita_shortcode campo="nome_do_campo"]`
    - `[audio_linha_shortcode campo="nome_do_campo"]`
    - `[video_esquerda_shortcode campo="nome_do_campo"]`
    - `[video_direita_shortcode campo="nome_do_campo"]`
    - `[imagem_lightbox_esquerda_shortcode campo="nome_do_campo"]`
    - `[imagem_lightbox_direita_shortcode campo="nome_do_campo"]`
    - `[breadcrumbs]`
    - `[recurso_relacionado_coluna1 campo="nome_do_campo"]`
    - `[recurso_relacionado_coluna2 campo="nome_do_campo"]`
    - `[recurso_relacionado_coluna3 campo="nome_do_campo"]`
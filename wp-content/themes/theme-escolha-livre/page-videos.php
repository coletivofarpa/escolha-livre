<?php $page = 'videos';
/* Template Name: Vídeos
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-videos" class="container">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="pb-3">
                        Muitos escolhem um serviço de conferência porque permite dezenas de usuários conectados com câmeras ligadas ao mesmo tempo. Mas, esse cenário é pouco útil e realista. Pode ser muito útil ver duas ou três pessoas ao mesmo tempo, mas por qual razão teríamos 30 câmeras ligadas? A gravação de aulas não é somente algo que deve ser aventado se não for possível fazer uma aula “ao vivo”. 
                    </p>
                    <p class="pb-3">
                        As aulas síncronas e as aulas gravadas são duas práticas com propósitos e cenários distintos.
                    </p>
                    <p class="pb-3">
                        Muitas vezes, as aulas ao vivo são gravadas e disponibilizadas posteriormente. Com os devidos cuidados técnicos para garantir uma boa qualidade de produção e com os devidos cuidados legais (veja esse guia), essa é uma prática que facilita o acesso aos recursos por alunos.  
                    </p>
                    <p class="pb-3">
                        Mas, avente a possibilidade de gravar suas aulas com calma e disponibilizar o material em múltiplos formatos: vídeo, áudio e texto, por exemplo. Os recursos podem ser acessados pelos alunos a qualquer momento, não dependendo de uma agenda comum. A gravação das aulas dá mais tempo para preparar um material de qualidade, bem como permite que você pense em outras atividades apoiadas por esse mesmo material.
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div id="producao-de-video" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Produção de vídeo
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

            <!-- Linha 4 -->
            <div class="row">
                <div class="col-md-12">

                    <p class="m-0 p-3">
                        Os cursos abaixo – que são recursos abertos – podem ajudar a pensar na produção do seu vídeo educacional.
                    </p>
                    <p class="m-0 p-3">
                        Veja também um guia simples (e aberto!) sobre boas práticas em produção de videopalestras
                    </p>
                </div>

                <div class="col-md-6 pb-3">
                    <div class="borda-esq-base pb-3">

                        <h3 class="fundo-preto p-3">Produção audiovisual para EaD</h3>

                        <div class="d-flex align-items-center justify-content-center">
                            <figure class="figure p-3">
                                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/henrique.jpg" class="img-fluid pb-4" width="600" alt="...">
                                <figcaption class="figure-caption text-center">
                                    
                                </figcaption>
                            </figure>
                        </div>

                        <p class="p-3">
                            Henrique Cunha (UTFPR)
                        </p>
                        <p class="p-3">
                            Muitas informações sobre a produção de videoaulas, incluindo design de iluminação, som e áudio.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <div class="borda-dir-base pb-3">

                        <h3 class="fundo-preto p-3">Produção de vídeos educacionais para web</h3>

                        <div class="d-flex align-items-center justify-content-center">
                            <figure class="figure p-3">
                                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/caitucia.jpg" class="img-fluid pb-4" width="600" alt="...">
                                <figcaption class="figure-caption text-center">
                                    
                                </figcaption>
                            </figure>
                        </div>

                        <p class="p-3">
                            Catiucia Klug Schneider (IFSul)
                        </p>
                        <p class="p-3">
                            Curso voltado para a produção de vídeo educacional caseiro, com considerações práticas e teórica.
                        </p>
                    </div>
                </div>

            </div>
            <!-- /Fim da Linha 4 -->

            <!-- Linha 5 -->
            <div id="recursos" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Recursos
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 5 -->

            <!-- Linha 6 -->
            <div class="row">
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">OBS</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo_obs.png" class="img-fluid pt-2 pb-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">OpenShot</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo_openshot.jpeg" class="img-fluid pb-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-dir-topo d-flex align-items-center justify-content-center">
                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">ScreenCan</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo_screenrecorder.png" class="img-fluid pb-4" width="150" alt="...">
                        </figure>
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 6 -->

            <!-- Linha 7 -->
            <div class="row">
                    
                <!-- Coluna Migalhas de pão -->
                <div class="migalhas col-md-12 pb-5">

                    <!-- Migalhas de pão -->
                    <nav aria-label="breadcrumb">
                        <!-- .linque-verde - Cor do hover -->
                        <ol class="linque-verde breadcrumb d-flex justify-content-end">
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/">Conheça mais recursos ></a>
                            </li>
                        </ol>
                    </nav>
                    
                </div>
                <!-- /Fim da Linha 7 -->

        </main>

<?php
get_footer();
?>
<?php $page = 'contato';
/* Template Name: Contato 
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

<main id="page-contato" class="container">

    <!-- Espaço -->
    <div class="espaco-80"></div>

    <!-- Linha 1 -->
    <div class="titulo-h1 d-flex align-items-center justify-content-center">
        <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                <?php //esc_html_e(single_post_title('', false)); ?>
                <?php
                    // Permitindo html seguro ao filtrar/ascapar tags inseguras 
                    $title = get_the_title();
                    $allowed_tags = array(
                        'br' => array(),
                        'a' => array(
                            'href' => array(),
                            'title' => array()
                        ),
                        'em' => array(),
                        'strong' => array(),
                        'p' => array(),
                        'span' => array(),
                        // Adicione outras tags permitidas aqui, se necessário
                    );
                    echo wp_kses($title, $allowed_tags);
                ?>
            </h1>
        </div>
    </div>

    <!-- Espaço -->
    <div class="espaco-80"></div>
    
    <?php
        // the content (pretty self explanatory huh)
        the_content();

            /*
            * Link Pages is used in case you have posts that are set to break into
            * multiple pages. You can remove this if you don't plan on doing that.
            *
            * Also, breaking content up into multiple pages is a horrible experience,
            * so don't do it. While there are SOME edge cases where this is useful, it's
            * mostly used for people to get more ad views. It's up to you but if you want
            * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
            *
            * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
            *
            */
            wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'theme-escolha-livre' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
            'link_after'  => '</span>',
            ) );
            ?>

    

    <!-- Formulário de contato Polylagn postado via editor de página padrão do WordPress-->
    <section class="entry-content cf" itemprop="articleBody">
<!-- 
    <?php //echo do_shortcode( '[contact-form-7 id="dc8bfad" title="Formulario de contato" html_id="form_contato"]' ); ?>

        <form action="/escolha-livre/contato/" method="post" class="wpcf7-form init" id="form_contato" aria-label="Formulários de contato" novalidate="novalidate" data-status="init">
            <div style="display: none;">
                <input type="hidden" name="_wpcf7" value="202">
                <input type="hidden" name="_wpcf7_version" value="5.9.3">
                <input type="hidden" name="_wpcf7_locale" value="pt_BR">
                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f202-o1">
                <input type="hidden" name="_wpcf7_container_post" value="0">
                <input type="hidden" name="_wpcf7_posted_data_hash" value="">
            </div>

            <div class="card contato mt-2">
                <label class="card-header contato" for="name"><?php //pll_e('Nome'); ?>:</label>
                    <span class="wpcf7-form-control-wrap" data-name="text-365"><input size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required card-body contato w-100" id="name" aria-required="true" aria-invalid="false" placeholder="<?php //pll_e('Escreva aqui seu nome'); ?>" value="" type="text" name="text-365"></span>

                    <!--[text* text-365 id:name class:card-body class:contato class:w-100 placeholder ""] --
            </div>

            <div class="card contato mt-2">
                <label class="card-header contato" for="email"><?php //pll_e('Email'); ?>:</label>
                    <span class="wpcf7-form-control-wrap" data-name="email-201"><input size="40" class="wpcf7-form-control wpcf7-email wpcf7-validates-as-required wpcf7-text wpcf7-validates-as-email card-body contato w-100" id="email" aria-required="true" aria-invalid="false" placeholder="<?php //pll_e('Escreva aqui seu email'); ?>" value="" type="email" name="email-201"></span>

                    <!--[email* email-201 id:email class:card-body class:contato class:w-100 placeholder ""]--
            </div>

            <div class="card contato mt-2">
                <label class="card-header contato" for="category"><?php //pll_e('Categoria'); ?></label>
                <span class="wpcf7-form-control-wrap" data-name="menu-368">
                    <select class="wpcf7-form-control wpcf7-select card-header w-100" aria-invalid="false" name="menu-368">
                        <option value="">
                            <?php //pll_e('Selecione uma opção'); ?>
                        </option>
                        <option value="Sugerir novo recurso">
                            <?php //pll_e('Sugerir novo recurso'); ?>
                        </option>
                        <option value="Apontar um erro/sugerir mudança">
                            <?php //pll_e('Apontar um erro/sugerir mudança'); ?>
                        </option>
                        <option value="Comentário/elogio/crítica">
                            <?php //pll_e('Comentário/elogio/crítica'); ?>
                        </option>
                    </select>
                </span>

                <!--[select menu-368 class:card-header class:w-100 my_select first_as_label "" "" "" ""] --

            </div>

            <div class="card contato mt-2">
                <label class="card-header contato" for="message"><?php //pll_e('Digite sua mensagem'); ?></label>
                <span class="wpcf7-form-control-wrap" data-name="textarea-160">
                    <textarea cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea card-body contato w-100" id="message" aria-invalid="false" name="textarea-160"></textarea>
                </span>

                <!--[textarea textarea-160 message id:message class:card-body class:contato class:w-100]--
            </div>

            <div class="card contato mt-2">
                <input class="wpcf7-form-control wpcf7-submit has-spinner w-100 h-100" id="messege" type="submit" value="<?php //pll_e('Enviar mensagem'); ?>">

                <!--[submit id:messege class:w-100 class:h-100 ""]--
            </div>
            <span class="wpcf7-spinner"></span>
            <div class="wpcf7-response-output" aria-hidden="true"></div>
        </form>
-->
    </section>

    <div class="espaco-80"></div>

</main>

<script>
    document.getElementById('category').addEventListener('change', function() {
        var category = this.value;
        var linkSuggestion = document.getElementById('linkSugestion');
        var selectCategory = document.getElementById('selectCategory');
        var featureLink = document.getElementById('featureLink');
        var featureSummary = document.getElementById('featureSummary');

        if (category === 'select') {
            linkSuggestion.style.display = 'none';
            selectCategory.style.display = 'none';
            featureLink.style.display = 'none';
            featureSummary.style.display = 'none';
        } else if (category === 'change') {
            linkSuggestion.style.display = 'block';
            selectCategory.style.display = 'none';
            featureLink.style.display = 'none';
            featureSummary.style.display = 'none';
        } else if (category === 'feature') {
            linkSuggestion.style.display = 'none';
            selectCategory.style.display = 'block';
            featureLink.style.display = 'block';
            featureSummary.style.display = 'block';
        } else {
            linkSuggestion.style.display = 'none';
            selectCategory.style.display = 'none';
            featureLink.style.display = 'none';
            featureSummary.style.display = 'none';
        }
    });
</script>



<?php get_footer(); ?>
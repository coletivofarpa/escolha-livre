        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url(pll_home_url( '/' )); ?>">
          <div>
            <label for="s" class="screen-reader-text"><?php esc_html_e('Search for:', 'theme-escolha-livre'); ?></label>
            <input type="search" id="s" name="s" value="" />

            <button type="submit" id="searchsubmit"><?php esc_html_e('Search', 'theme-escolha-livre'); ?></button>
          </div>
        </form>
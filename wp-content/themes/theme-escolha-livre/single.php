<?php
/* Template Name: Single 
 * @package escolha-livre
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>

		<main id="single" class="container">

			<!-- Linha 1 -->
			<div class="titulo-h1 d-flex align-items-center justify-content-center">
				<div class="col-12 separador">
					<h1 class="text-uppercase me-5 text-decoration-none text-muted">
						<?php esc_html_e(single_post_title('', false)); ?>
					</h1>
				</div>
			</div>
			<!-- /Fim da Linha 1 -->
			<!-- Linha 2 -->
			<div class="row pb-5">
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
			<!-- /Fim da Linha 2 -->

			<!-- <div class="row">
				<div class="col-md-12">
					<div class="img" style="background-image: url('<?php echo $featured_img_url; ?>')"></div>
				</div>
			</div>

			<div class="row ctn-info mb-5">
				<div class="col-md-8">
					<span>
					</span>
				</div>

				<div class="col-md-4 text-right">
					<span class="mr-4">
						<?php echo get_language() == "pt" ? get_the_date('d/m/Y') : get_the_date('m-d-Y'); ?>
					</span>

					<span><?php get_language() == "pt" ? "Compartilhe" : "Compartilhe"?>:</span>
					<a class="space" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>" target="_blank">
						<i class="fa fa-facebook-official"></i>
					</a>
					<a href="https://www.linkedin.com/cws/share?url=<?php echo urlencode(get_permalink()) ?>" target="_blank">
						<i class="fa fa-linkedin-square"></i>
					</a>
					<a href="http://twitter.com/share?text=<?php get_field('resume')?>&url=<?php echo urlencode(get_permalink()) ?>" target="_blank">
						<i class="fa fa-twitter-square"></i>
					</a>
					
				</div>
			</div>

			-->

		</main>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

        <!--Rodapé-->
        <footer id="rodape" class="border border-bottom border-top">

            <!--Caixa (Grid) do conteúdo fluida-->
            <div class="container p-4">

                <!--Seção semântica do conteúdo principal do rodapé-->
                <section class="mb-4">

                    <!--Título do rodapé-->
                    <h3 class="ms-5 pb-4">
                        <?php echo esc_html( pll__( 'Realização:', 'theme-escolha-livre' )); ?>
                    </h3>

                    <!--Linha-->
                    <div class="logo-rodape row text-center">
                        <!--Coluna 1-->
                        <div class="col-lg-3 col-md-6 col-sm-12 mb-4 mb-sm-0">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a class="navbar-brand d-inline-block align-text-top" href="<?php echo esc_url( get_theme_mod( 'footer_logo_link_1' ) ); ?>" target="_blank">
                                        <img class="img-fluid mx-auto d-block" src="<?php echo esc_url( get_theme_mod( 'footer_logo_image_1' ) ); ?>" alt="Escolha Livre" width="236" height="88" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!--Fim da Coluna 1-->

                        <!--Coluna 2-->
                        <div class="separador-rodape col-lg-3 col-md-6 col-sm-12 mb-4 mb-sm-0">

                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a class="navbar-brand d-inline-block align-text-top" href="<?php echo esc_url( get_theme_mod( 'footer_logo_link_2' ) ); ?>" target="_blank">
                                        <img class="img-fluid mx-auto d-block" src="<?php echo esc_url( get_theme_mod( 'footer_logo_image_2' ) ); ?>" alt="Iniciativa Educação Aberta" width="271" height="101" /></a>
                                </li>
                            </ul>
                        </div>
                        <!--Fim da Coluna 2-->

                        <!--Coluna 3-->
                        <div class="separador-rodape col-lg-3 col-md-6 col-sm-12 mb-4 mb-sm-0">

                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a class="navbar-brand d-inline-block align-text-top" href="<?php echo esc_url( get_theme_mod( 'footer_logo_link_3' ) ); ?>" target="_blank">
                                        <img class="img-fluid mx-auto d-block" src="<?php echo esc_url( get_theme_mod( 'footer_logo_image_3' ) ); ?>" alt="Unesco logo azul" width="148" height="117" /></a>
                                </li>
                            </ul>
                        </div>
                        <!--Fim da Coluna 3-->

                        <!--Coluna 4-->
                        <div class="separador-rodape col-lg-3 col-md-6 col-sm-12 mb-4 mb-sm-0">

                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a class="navbar-brand d-inline-block align-text-top" href="<?php echo esc_url( get_theme_mod( 'footer_logo_link_4' ) ); ?>" target="_blank">
                                        <img class="img-fluid mx-auto d-block" src="<?php echo esc_url( get_theme_mod( 'footer_logo_image_4' ) ); ?>" alt="Cátedra Unesco em Educação a Distância" width="94" height="118" /></a>
                                </li>
                            </ul>
                        </div>
                        <!--Fim da Coluna 4-->

                    </div>
                    <!--Fim da Linha principal-->

                </section>
                <!--Fim da seção semântica do conteúdo principal do rodapé-->

            </div>
            <!--Fim da Caixa (Grid) do conteúdo principal-->

            <!--Copyleft creative commons-->
            <div class="copleft p-3 pb-5 ps-5 border-top">

                <div class="container">

                    <div class="row">

                        <span class="col-sm-6 col-lg-2 pb-sm-2 text-lg-center text-sm-center linque linque-rodape">
                            <a class="d-inline-block" href="<?php echo home_url() ; ?>/termos-de-uso">
                                <?php echo esc_html( pll__( 'Termos de uso', 'theme-escolha-livre' )); ?>
                            </a>
                        </span>

                        <span class="col-sm-6 col-lg-3 pb-sm-2 text-lg-center text-sm-center linque linque-rodape">
                            <a class="d-inline-block" href="<?php echo home_url() ; ?>/politica-de-privacidade">
                                <?php echo esc_html( pll__( 'Política de privacidade', 'theme-escolha-livre' )); ?>
                            </a>
                        </span>

                        <span class="col-sm-12 col-lg-7 pt-sm-2 text-lg-center text-sm-center linque linque-rodape">
                            <?php echo esc_html( pll__( 'A não ser que indicado nos termos de uso, todo conteúdo do site está sob licença', 'theme-escolha-livre' )); ?>
                            <a class="d-inline-block" href="https://creativecommons.org/licenses/by-sa/3.0/igo/" target="_blanck">CC-BY-SA 3.0 IGO</a>
                        </span>
                    </div>
                </div>
            </div>
            <!--Fim do Copyleft creative commons-->

            <div class="bg-light text-center">

                <div class="container">

                    <span class="linque linque-rodape">
                        <!-- https://creativecommons.org/2020/03/18/the-unicode-standard-now-includes-cc-license-symbols/ -->
                        <b class="rato">&#x1f16d;</b> <?php echo date('Y'); ?> <?php echo esc_html( pll__( 'Copyleft Escolha Livre | Criado com', 'theme-escolha-livre' )); ?> <b class="rato">&#9829</b> <?php echo esc_html( pll__( 'pelo', 'theme-escolha-livre' )); ?>
                        <a href="https://coletivofarpa.org" target="_blanck" alt='Link para o sitio do Coletivo Farpa .org'>
                            Coletivo Farpa
                        </a> 
                        <?php echo esc_html( pll__( 'utilizando tecnologias de Software Livre GPLv3+', 'theme-escolha-livre' )); ?>
                    </span>
                </div>
            </div>

        </footer>
        <!--Fim do Rodapé-->

        <!-- REGISTRANDO E ENFILEIRANDO Bibliotecas, Scripts e Estilos corretamente a partir do functions.php -->
        <?php wp_footer(); ?>
        
    </body>
</html> <!-- end of site. what a ride! -->
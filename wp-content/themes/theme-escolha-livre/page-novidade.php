<?php $page='novidade';
/* Template Name: Novidade 
 * @package escolha-livre
 */
?>

        <?php get_header(); ?>

        <main id="page-novidade" class="container">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <div class="col-md-12 border">
                    Tester
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

        </main>

        <?php get_footer(); ?>
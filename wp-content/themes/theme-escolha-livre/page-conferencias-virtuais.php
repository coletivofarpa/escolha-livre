<?php $page = 'conferencias-virtuais';
/* Template Name: Conferências Virtuais 
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-conferencias-virtuais" class="container">
            
            <!-- Linha 1 -->
            <div class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Conferências <br>Virtuais
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row pb-5">
                <div class="col-md-12">
                    <p class="pb-3">
                        A conferência de forma síncrona (em tempo real) é uma das modalidades preferidas para a condução de atividades a distância. O uso do vídeo ao vivo é pensado como o melhor modelo de interação online, já que é o canal que carrega mais informação e, portanto, seria o mais próximo de uma interação presencial.
                    </p>
                    <p class="pb-3">
                        Pensamos assim: já que não podemos nos encontrar, fazemos uma videoconferência! A proliferação de lives e conferências nos ajudou a perceber que a videoconferência deve ser usada com parcimônia.
                    </p>
                    <p class="pb-3">
                        A possibilidade de ver os outros participantes não garante uma sensação de proximidade. O uso do vídeo demanda boa qualidade de conexão. A qualidade pode ser impactada por limitações de franquia e, claro, por problemas técnicos. Vale a pena pensar em formatos alternativos, incluindo a transmissão ao vivo, a organização de grupos pequenos, a gravação da aula e o uso somente de áudio.
                    </p>
                    <p class="pb-3">
                        Muitos escolhem um serviço de conferência porque permite dezenas de usuários conectados com câmeras ligadas ao mesmo tempo. Mas, esse cenário é pouco útil e realista. Pode ser muito útil ver duas ou três pessoas ao mesmo tempo, mas 30 câmeras ligadas? Para selecionar um sistema, é importante primeiro pensar nos cenários de uso e escolher a ferramenta e o modelo mais apropriados.
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row">
                <div class="col-md-12">

                        <p class="col-md-12 m-0 p-3">
                            É possível incorporar ferramentas livres em escolas públicas do ensino básico, mesmo nesse momento de grandes dificuldades. Conheça o exemplo do CEEJA (Marília, SP) que utiliza Jitsi, WordPress e Telegram para pensar em uma estratégia apropriada para os alunos no contexto pandêmico.
                        </p>

                        <!-- Linha interna -->
                        <div class="row borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                            
                            <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                                "Foi uma estratégia pensada com calma, para gente poder ter praticidade mas também sem deixar de ter privacidade."
                            </p>

                            <img class="col-md-2 img-fluid py-3" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/carla_loureiro.jpg" width="400" height="533" alt="Foto de professor Nelson Pretto da UFBA" />
                                
                            <div class="col-md-4 align-self-end py-3">
                                <p class="d-inline align-text-bottom">
                                    Prof. João Ras
                                    <br><br>
                                    Professor coordenador pedagógico do Centro Estadual de Educação de Jovens e Adultos de Marília-SP (desde 2011) e Presidente do Conselho Municipal de Educação de Marília (desde 2019).
                                </p>
                            </div>

                            <!-- Audio player -->
                            <div class="holder col-md-6 d-flex align-self-end p-0">
                                    <div class="audio green-audio-player">

                                    <!-- Adicione a palavra "Escute" aqui -->
                                    <span class="listen-text text-light me-3">Escute</span>
                                    <div class="loading">
                                        <div class="spinner"></div>
                                    </div>
                                    <div class="play-pause-btn">  
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                            <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                        </svg>
                                    </div>

                                    <div class="controls">
                                        <span class="current-time">0:00</span>
                                        <div class="slider" data-direction="horizontal">
                                            <div class="progress">
                                                <div class="pin" id="progress-pin" data-method="rewind"></div>
                                            </div>
                                        </div>
                                        <span class="total-time">0:00</span>
                                    </div>

                                    <div class="volume">
                                        <div class="volume-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                            </svg>
                                        </div>
                                        <div class="volume-controls hidden">
                                            <div class="slider" data-direction="vertical">
                                                <div class="progress">
                                                    <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <audio crossorigin>
                                        <source src="https://archive.org/download/gislaine-software-livre/gislaine.mp3" type="audio/mp3">
                                    </audio>
                                </div>
                                <!-- Script do audio player acima -->
                                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                            </div>
                            <!-- /Fim do audio player -->
                        </div>
                        <!-- /Fim da Linha interna -->

                    </div>
                        
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

            <!-- Linha 4 -->
            <div id="ao-vivo" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Ao vivo
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 4 -->

            <!-- Linha 5 -->
            <div class="row enviar-publicar">

                <div class="col-md-6 pb-3">
                    <div class="borda-esq-base">

                        <h3 class="fundo-preto p-3">Ao vivo</h3>

                        <p class="p-3">
                            Muitos alunos prezam o momento síncrono por permitir algum tipo de interação em tempo real. Nessa modalidade, o professor transmite uma aula ao vivo, utilizando vídeo e/ou áudio, que pode ser combinado com o uso de uma apresentação visual ou o compartilhamento de uma tela do seu computador.
                        </p>
                        <p class="p-3">
                            Pode se assemelhar a uma aula/palestra presencial, mas, de fato, não é a mesma coisa. Por exemplo, alunos podem se manifestar no bate-papo (o que muitos não fariam presencialmente) ou podem se engajar em conversas paralelas na plataforma ou em outros aplicativos. Muitas aulas presenciais têm longos momentos expositivos – o que pode se tornar ainda mais cansativo em um ambiente online. Neste sentido, é sempre importante atentar para uma proposta de encontro ao vivo que estimule a participação efetiva dos alunos.
                        </p>
                        <p class="p-3">
                            Considere que a transmissão ao vivo gera um compromisso e alunos podem reservar esse horário na agenda para focar na atividade. Não ignore a possibilidade de que muitos alunos não participarão por diversas razões (técnicas, tempo, dentre outras) e pense em como poderá oferecer formas complementares de engajamento. A gravação das aulas é uma possibilidade e, ao fazê-lo, considere questões éticas, legais e institucionais.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <div class="borda-dir-base">

                        <h3 class="fundo-preto p-3">Pequenos grupos</h3>

                        <p class="p-3">
                            O momento síncrono pode contribuir para trabalhos em grupo, como quando trabalhamos na perspectiva de projetos.
                        </p>
                        <p class="p-3">
                            Serviços de videoconferência normalmente não restringem o número de salas que podem ser criadas, mas podem restringir o número de participantes por sala – seja por questões técnicas ou por limitações de um plano contratado. Você pode, então, pensar em criar uma sala principal para o encontro de todos e várias salas para discussão em grupos menores.
                        </p>
                        <p class="p-3">
                            Algumas ferramentas permitem que você organize uma sala principal e, conectada a ela, outras salas para pequenos grupos de alunos. Esse alunos podem ir a uma sala menor, discutir e, depois, retornar à sala maior para compartilhar o conteúdo que elaboraram. 
                        </p>
                        <p class="p-3">
                            O uso somente de áudio pode ser uma boa ideia, considerando o propósito (como uma ligação telefônica) e levando em conta a baixa qualidade das conexões nossas à internet. Alternativamente, o vídeo pode ser rotacionado entre os participantes, aumentando a participação de todos. 
                        </p>
                    </div>
                </div>

                <div class="col-md-12 my-3">
                    <div class="borda-dir-base">

                        <h3 class="fundo-preto p-3">Foco nos alunos</h3>

                        <p class="p-3">
                            Tente pensar além da transposição da aula expositiva para o espaço online. Você pode organizar um espaço de videoconferência que não tem como foco uma palestra ou exposição de conteúdos.
                        </p>
                        <p class="p-3">
                            Pense em como o momento ao vivo pode engajar seus alunos, sendo usado para apresentar trabalhos e atividades deles, ou, ainda, como um espaço de avaliação formativa com pares.
                        </p>
                        <p class="p-3">
                            Para melhor qualidade de interação, pode-se rotacionar o controle da apresentação com grupos de alunos, que viram apresentadores. Mesmo em uma sala com diversos alunos, somente uma ou um pequeno grupo de pessoas controla a apresentação e deixa sua câmera ligada a cada momento. 
                        </p>
                    </div>
                </div>
                
            </div>
            <!-- /Fim da Linha 5 -->

            <!-- Linha 6 -->
            <div id="recursos" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Recursos
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 6 -->

            <!-- Linha 7 -->
            <div class="row">
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">Jitsi</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo-jitsi.png" class="img-fluid pt-2 pb-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">Mumble</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/mumble_logo.png" class="img-fluid pb-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-dir-topo d-flex align-items-center justify-content-center">
                    <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">Mconf</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/bigbluebutton.png" class="img-fluid pb-4" width="150" alt="...">
                        </figure>
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 7-->

            <!-- Linha 8 -->
            <div class="row">
                    
                <!-- Coluna Migalhas de pão -->
                <div class="migalhas col-md-12 pb-5">

                    <!-- Migalhas de pão -->
                    <nav aria-label="breadcrumb">
                        <!-- .linque-verde - Cor do hover -->
                        <ol class="linque-verde breadcrumb d-flex justify-content-end">
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/">Conheça mais recursos ></a>
                            </li>
                        </ol>
                    </nav>
                    
                </div>
                <!-- /Fim da Linha 8 -->

        </main>

<?php get_footer(); ?>
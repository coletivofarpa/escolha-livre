<?php $page = 'tutoriais';
/* Template Name: Tutoriais
 * @package escolha-livre
 */
?>
    <?php get_header(); ?>

    <main id="page-tutoriais" class="main-tutoriais container">
      <div class="row">

        <!-- Espaço -->
        <div class="espaco-80"></div>

        <div class="titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php //esc_html_e(single_post_title('', false), 'theme-escolha-livre'); ?>
              <?php
                // Permitindo html seguro ao filtrar/ascapar tags inseguras 
                $title = get_the_title();
                $allowed_tags = array(
                  'br' => array(),
                  'a' => array(
                  'href' => array(),
                  'title' => array()
                ),
                'em' => array(),
                'strong' => array(),
                'p' => array(),
                'span' => array(),
                // Adicione outras tags permitidas aqui, se necessário
              );
              echo wp_kses($title, $allowed_tags);
              ?>
              </h1>
            </div>
          </div>

          <!-- Espaço -->
          <div class="espaco-60"></div>

          <?php
          $posts = array();
          $categories = array();

          $args = array('post_type' => 'tutorial', 'post_status' => 'publish', 'orderby' => array('date' =>'DESC'));

          $data = new WP_Query($args);
          if ($data->have_posts()) {
            while ($data->have_posts()) {
              $data->the_post();
              $post_content = get_the_content();

              // get acf
              $resumo = get_field('resumo');
              $resumo_es = get_field('resumo_es');
              $resumo_en = get_field('resumo_en');
              $imagem = get_field('imagem');
              $categoria = get_field('categoria');

              // limita a string do resumo
              $resumo_limit = $resumo ? substr($resumo, 0, 1000) : '';
              $resumo_es_limit = $resumo_es ? substr($resumo_es, 0, 1000) : '';
              $resumo_en_limit = $resumo_en ? substr($resumo_en, 0, 1000) : '';

              array_push($posts, array(
                'id' => get_the_ID(), // Obter o ID da postagem
                'title' => get_the_title(),
                'imagem' => $imagem,
                'resumo_limit' => $resumo_limit,
                'resumo_es_limit' => $resumo_es_limit,
                'resumo_en_limit' => $resumo_en_limit,
                'categorias' => $categoria
              ));
            }
          }

          // Loop através da lista de post
          foreach ($posts as $post) {
            if (isset($post['categorias']) && is_array($post['categorias'])) {
              // Adicione as categorias à lista de categorias únicas
              $categories = array_merge($categories, $post['categorias']);
            }
          }

          // Remova duplicatas usando array_unique
          $categories = array_unique($categories);

          // Agora $categoriasUnicas contém as categorias únicas
          // print_r($categories);

          // Obtém o idioma atual usando Polylang
          $current_language = function_exists('pll_current_language') ? pll_current_language() : 'pt_BR';
          ?>

          <div class="col-12" id="titulo-filtro">
            <h2 class="mb-2">
            <?php echo esc_html( pll__( 'Temas', 'theme-escolha-livre' )); ?>
            </h2>
          </div>

          <div class="col-12" id="filtro-div">
            <div class="d-flex flex-wrap border-0 w-100">
              <?php foreach ($categories as $key => $item) { ?>
                <div class="borda-esq-base filtro armazenamento lnk-<?php echo formatString($item)?>" onClick="filterPage('<?php echo formatString($item)?>')" id="card-filtro-tutoriais">
                  <?php //echo $item ?>
                  <?php echo esc_html( pll__( $item, 'theme-escolha-livre' )); ?>
                </div>
              <?php } ?>
          </div>
        </div>
      </div>

      <!-- Espaço -->
      <div class="espaco-40"></div>

      <div class="row">
      <!--
          <pre>
          <?php //print_r($posts); ?>
          </pre>
      -->
          <?php foreach ($posts as $key => $post) { ?>
          <div class="col-md-6 mb-3 list-all 
          <?php foreach ($post['categorias'] as $k => $category) {  echo formatString($category) . ' '; }?>
          ">
              <a class="text-decoration-none text-muted me-5" href="<?php echo get_permalink($post['id']) ?>">
                  <div class="card tutoriais mt-2">
                      <div class="card-header tutoriais">
                          <?php echo $post['title']?>
                      </div>
                      <div class="card-body d-flex flex-column texto-cards">
                          <?php //$post['resumo_limit']?>
                          <?php 
                              // Obter o resumo do post com base no idioma
                              if($current_language == 'pt_BR' && !empty($post['resumo_limit'])) {
                              echo $post['resumo_limit'];
                              } elseif($current_language == 'es' && !empty($post['resumo_es_limit'])) {
                              echo $post['resumo_es_limit'];
                              } elseif($current_language == 'en' && !empty($post['resumo_en_limit'])) {
                              echo $post['resumo_en_limit'];
                              } else {
                              // Caso o resumo no idioma específico não esteja disponível, exibir o resumo padrão
                              echo $post['resumo_limit'];
                              }
                          ?>
                          <div class="img" style="background-image: url(<?php echo $post['imagem']?>);"></div>
                      </div>
                  </div>
              </a>
          </div>
          <?php } ?>
      </div>

    </main>

    <div class="espaco-80"></div>

    <script>
        filterPage = function(id){

            var alreadyClicked = jQuery('.lnk-'+id).hasClass('filter-click');

            jQuery('.list-all').hide();

            if (alreadyClicked) 
            {
                jQuery('.filter-click').removeClass('filter-click');
                jQuery('.list-all').show();
            } 
            else 
            {
                jQuery('.'+id).show();
                jQuery('.filter-click').removeClass('filter-click');
                jQuery('.lnk-'+id).addClass('filter-click');
            }
        }
    </script>

<?php get_footer(); ?>


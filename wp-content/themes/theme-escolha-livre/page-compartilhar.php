<?php $page='compartilhar';
/* Template Name: Compartilhar
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-compartilhar" class="container pb-5">

            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row enviar-publicar">

                <div class="col-md-12 mb-3">
                    <div class="borda-dir-base">

                        <h3 class="fundo-preto p-3">
                            WIKIMEDIA COMMONS
                        </h3>

                        <div class="row">

                            <figure class="col-md-4 figure p-3">
                                <figcaption class="figure-caption text-center pb-3">
                                    Unsplash
                                </figcaption>
                                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/Unsplash.png" class="img-fluid" width="430" alt="...">
                            </figure>

                            <div class="col-md-8">
                                <p class="p-3">
                                    É onde todos os recursos da Wikipedia e de outros projetos da Fundação Wikimedia (como a Wikiversidade) ficam hospedados. Qualquer um pode contribuir com recursos educacionais de qualidade nesse espaço, que hospeda vídeos, áudios, documentos, fotos e imagens com licenças livres ou que estejam no domínio público. No espírito da Wikipedia, você deve subir seus recursos aqui como uma forma de contribuir para o conhecimento livre. 
                                </p>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="borda-esq-base">

                        <h3 class="fundo-preto p-3">Licenças livres</h3>

                        <p class="p-3">
                            Recursos Educacionais Abertos dependem, primeiro de tudo, de uma licença livre. Isso porque, de acordo com nossa legislação de direito autoral (saiba mais), quem cria a obra tem todos os direitos sob ela. Os REA adotam licenças livres, que permitem que quem criou a obra defina as condições de seu uso por terceiros.
                        </p>
                        <p class="p-3">
                            Educadores normalmente querem muito compartilhar o que criam com colegas, mas também querem garantir alguns direitos, como a atribuição da autoria. Outros não querem que sua obra seja utilizada por terceiros para gerar lucro.
                        </p>
                        <p class="px-3 pt-3">
                            Você pode definir esses termos com uma licença livre! Saiba mais no Guia de Bolso da Educação Aberta. 
                        </p>

                        <!-- Audio player -->
                        <div class="holder col-md-12 d-flex align-self-end p-md-5">
                            Prof. Juarez Bento da Silva<br>
                            <div class="audio green-audio-player">

                                <!-- Adicione a palavra "Tutorial" aqui -->
                                <span class="listen-text text-light me-3">Tutorial</span>
                                <div class="loading">
                                    <div class="spinner"></div>
                                </div>
                                <div class="play-pause-btn">  
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                    </svg>
                                </div>

                                <div class="controls">
                                    <span class="current-time">0:00</span>
                                    <div class="slider" data-direction="horizontal">
                                        <div class="progress">
                                            <div class="pin" id="progress-pin" data-method="rewind"></div>
                                        </div>
                                    </div>
                                    <span class="total-time">0:00</span>
                                </div>

                                <div class="volume">
                                    <div class="volume-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                            <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                        </svg>
                                    </div>
                                    <div class="volume-controls hidden">
                                        <div class="slider" data-direction="vertical">
                                            <div class="progress">
                                                <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <audio crossorigin>
                                    <source src="https://archive.org/download/juarez-rexlab/juarez.mp3" type="audio/mp3">
                                </audio>
                            </div>
                            <!-- Script do audio player acima -->
                            <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                        </div>
                        <!-- /Fim do audio player -->

                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="borda-dir-base">

                        <h3 class="fundo-preto p-3">Formatos abertos</h3>

                        <p class="p-3">
                            Quando criamos recursos, salvamos os arquivos em algum formato. É importante compartilhar em formatos que possam ser ao menos abertos por outras pessoas (ODT, ao invés de DOC, por exemplo). Existem formatos que só funcionam em alguns sistemas operacionais, ou, que só podem ser abertos com programas que não são livres.
                        </p>
                        <p class="px-3 pt-3">
                            Por isso, o uso de formatos abertos e a criação de recursos utilizando software livre é tão importante. Só assim, conseguimos garantir que o maior número de pessoas poderá abrir, usar e (se assim quisermos) alterar o recurso para criar obras derivadas, com base nos nossos recursos. 
                        </p>

                        <!-- Audio player -->
                        <div class="holder col-md-12 d-flex align-self-end p-md-5">
                            Profa. Tânia Midian de Souza
                            <div class="audio green-audio-player">

                                <!-- Adicionando a palavra "Tutorial" aqui -->
                                <span class="listen-text text-light me-3">Tutorial</span>
                                <div class="loading">
                                    <div class="spinner"></div>
                                </div>
                                <div class="play-pause-btn">  
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                    </svg>
                                </div>

                                <div class="controls">
                                    <span class="current-time">0:00</span>
                                    <div class="slider" data-direction="horizontal">
                                        <div class="progress">
                                            <div class="pin" id="progress-pin" data-method="rewind"></div>
                                        </div>
                                    </div>
                                    <span class="total-time">0:00</span>
                                </div>

                                <div class="volume">
                                    <div class="volume-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                            <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                        </svg>
                                    </div>
                                    <div class="volume-controls hidden">
                                        <div class="slider" data-direction="vertical">
                                            <div class="progress">
                                                <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <audio crossorigin>
                                    <source src="https://archive.org/download/tania-rea/tania.mp3" type="audio/mp3">
                                </audio>
                            </div>
                            <!-- Script do audio player acima -->
                            <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                        </div>
                        <!-- /Fim do audio player -->

                    </div>
                </div>

                

                
            </div>
            <!-- /Fim da Linha 2 -->

        </main>

<?php get_footer(); ?>
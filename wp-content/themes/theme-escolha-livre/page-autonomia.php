<?php $page='autonomia';
/* Template Name: Autonomia
 * @package escolha-livre
 */
?>

        <?php get_header(); ?>

        <main id="page-autonomia" class="container pb-5">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="pb-3">
                    Os sistemas e portais que apresentamos no Escolha Livre estão disponíveis na internet para uso através de aplicativos ou do seu navegador. O uso gratuito desses sistemas pode resolver as demandas de professores, alunos e de instituições de maneira rápida e fácil. No entanto, caso você, gestor, queira maior controle, customização, garantia de funcionamento e qualidade de serviço, você pode optar por dois outros modelos:
                    </p>
                    <p class="pb-3">
                    Você pode baixar, customizar e instalar o sistema para uso de sua instituição. O software livre dá essa permissão! Você pode, por exemplo, criar um sistema de videoconferência em sua instituição ou para sua organização. Alguns são instalados com relativa facilidade. Outros são mais complexos e podem demandar a atuação de uma equipe de tecnologia da informação em sua rede ou instituição. Você pode pagar somente pela configuração e instalação do serviço na sua instituição ou, adicionalmente, contratar suporte técnico externo. Há ampla documentação e fóruns de discussão na internet para lhe apoiar nesse processo.
                    </p>
                    <p class="pb-3">
                    Os REA criados por professores, ou aqueles baixados da internet, podem ser disponibilizados no site da sua instituição, por exemplo, fazendo uma curadoria própria de conteúdos para escola. No depoimento abaixo da Profa. Gislaine Munhoz, você pode saber mais sobre a perspectiva da gestão na implementação de um projeto de robótica educativa com software livre.
                    </p class="pb-3">
                    <p class="pb-3">
                    Você pode contratar o serviço. Muitos prestadores de serviço oferecem o uso de um plataforma construída com software livre (como, por exemplo, repositório, videoconferência ou ambiente virtual de aprendizagem) através de um serviço pago, como uma assinatura mensal (calculada por número de usuários, salas, ou outros critérios). Muitas vezes são os próprios desenvolvedores do software que oferecem o serviço. Há aqui uma garantia de qualidade, disponibilidade de suporte técnico e possibilidade de uso imediato. Existem também provedores de recursos educacionais abertos que permitem o uso livre e gratuito dos recursos, mas que oferecem curadoria, mentoria, apoio, plataformas de avaliação, dentre outros serviços que podem ser muito úteis.
                    </p class="pb-3">
                    <p class="pb-3">
                    Esses serviços podem ser contratados de diversas formas. Muitas vezes o próprio desenvolvedor, em seu site, oferece o serviço. Em outros casos, parceiros ou provedores são listados. Para tanto, se beneficie do Mapa de Serviços Abertos, que ajudará você a encontrar empresas, cooperativas e prestadores que atendem em sua região.
                    </p class="pb-3">
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row"  id="player-autonomia">
                <div class="col-md-12">
                    <div class="px-5">
                        <div class="col-md-12 borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                            <!-- Linha interna -->
                            <div class="row">

                                <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                                Um dos motivos pelos quais as redes não davam continuidade a projetos de robótica… era porque não havia verba para renovação de licenças proprietárias…
                                </p>
                                <img class="col-md-2 img-fluid py-3" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/gislaine.jpg" width="400" height="533" alt="Foto da professora Gislaine Batista Munhoz" />
                                
                                <div class="col-md-4 align-self-end py-3">
                                    <p class="d-inline align-text-bottom">
                                        Profa. Gislaine Batista Munhoz
                                        <br><br>
                                        Coordenadora Pedagógica da Rede Municipal de Ensino de São Paulo, Mestre em Educação (FEUSP). Fellow de Aprendizagem Criativa.                                    </p>
                                </div>

                                <!-- Audio player -->
                                <div class="holder col-md-6 d-flex align-self-end p-0">
                                    <div class="audio green-audio-player">
                                        <!-- Adicione a palavra "Escute" aqui -->
                                        <span class="listen-text text-light me-3">Escute</span>
                                        <div class="loading">
                                            <div class="spinner"></div>
                                        </div>
                                        <div class="play-pause-btn">  
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                            </svg>
                                        </div>

                                        <div class="controls">
                                            <span class="current-time">0:00</span>
                                            <div class="slider" data-direction="horizontal">
                                                <div class="progress">
                                                    <div class="pin" id="progress-pin" data-method="rewind"></div>
                                                </div>
                                            </div>
                                            <span class="total-time">0:00</span>
                                        </div>

                                        <div class="volume">
                                            <div class="volume-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                                </svg>
                                            </div>
                                            <div class="volume-controls hidden">
                                                <div class="slider" data-direction="vertical">
                                                    <div class="progress">
                                                        <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <audio crossorigin>
                                            <source src="https://archive.org/download/gislaine-software-livre/gislaine.mp3" type="audio/mp3">
                                        </audio>
                                    </div>
                                    <!-- Script do audio player acima -->
                                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                                </div>
                                <!-- /Fim do audio player -->
                            </div>

                        </div>
                        <!-- /Fim da Linha interna -->
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

        </main>

        <?php get_footer(); ?>
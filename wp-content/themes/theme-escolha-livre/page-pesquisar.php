<?php 
/* Template Name: Pesquisar 
 * @package escolha-livre
 */
?>
        <?php get_header(); ?>

        <main id="page-pesquisar" class="container" role="main">
                
            <!-- Espaço -->
            <div class="espaco-80"></div>
            
            <!-- Linha 1 título h1 -->
            <div class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php //esc_html_e(single_post_title('', false)); ?>
                        <?php
                            // Permitindo html seguro ao filtrar/ascapar tags inseguras 
                            $title = get_the_title();
                            $allowed_tags = array(
                                'br' => array(),
                                'a' => array(
                                'href' => array(),
                                'title' => array()
                                ),
                                'em' => array(),
                                'strong' => array(),
                                'p' => array(),
                                'span' => array(),
                                // Adicione outras tags permitidas aqui, se necessário
                            );
                            echo wp_kses($title, $allowed_tags);
                        ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->
            
            <!-- Espaço -->
            <div class="espaco-80"></div>
        
            <!-- Linha 2 barra de pesquisa -->
            <form role="search" method="get" class="search-form" action="<?php echo esc_url(pll_home_url('/')); ?>">
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <input type="search" class="form-control" id="campo-busca" id="s" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e( pll__( 'digite aqui o que quer pesquisar', 'theme-escolha-livre' )); ?>" />
                            <span class="input input-group-btn">
                                <button type="submit" id="botao-busca" class="btn align-items-center justify-content-center">
                                    
                                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" fill="none" width="40" xmlns="http://www.w3.org/2000/svg" style="-webkit-print-color-adjust:exact" id="screenshot-634cb711-062a-8080-8002-60898dbd88f3" version="1.1" viewBox="5965.5 867.5 40 32" height="32">
                                        <g id="shape-634cb711-062a-8080-8002-60898dbd88f3" rx="0" ry="0"><g id="shape-634cb711-062a-8080-8002-60898dbdc92e"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92e"><ellipse rx="11.839810094838867" ry="11.939804420205178" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92e" class="strokes"><g class="stroke-shape"><ellipse rx="11.839810094838867" ry="11.939804420205178" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc92f"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92f"><ellipse rx="8.75116398314185" ry="8.825072832325418" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92f" class="strokes"><g class="stroke-shape"><ellipse rx="8.75116398314185" ry="8.825072832325418" style="fill:none;fill-opacity:none;stroke-width:0.5;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc930"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc930"><path rx="0" ry="0" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc930" class="strokes"><g class="stroke-shape"><path rx="0" ry="0" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g></g></g></g>
                                    </svg>

                                    <?php esc_html_e( pll__( 'Pesquisar', 'theme-escolha-livre' )); ?>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 Filtro por idioma -->
            <div class="row py-5 idiomas">
                <h3 class="col-12 pb-3">
                    <?php echo esc_html( pll__( 'Escolha o idioma', 'theme-escolha-livre' )); ?>
                </h3>

                <!-- Botões do Polylang -->
                <?php if ( function_exists('pll_the_languages') ) : ?>
                <ul class="polylang-buttons row">
                  <?php $languages = pll_the_languages( array( 'raw' => 1 ) ); ?>
                  <?php if ( $languages ) : ?>
                  <?php foreach ( $languages as $language ) : ?>
                  <?php 
                    // Verifica a existência das chaves 'slug', 'locale', 'url', 'name' e 'current_lang'
                    $slug = isset($language['slug']) ? $language['slug'] : '';
                    $locale = isset($language['locale']) ? $language['locale'] : '';
                    $url = isset($language['url']) ? $language['url'] : '#';
                    $name = isset($language['name']) ? $language['name'] : '';
                    $current_lang = isset($language['current_lang']) && $language['current_lang'] ? 'current-lang' : '';
                  ?>
                  <div class="col-md-4">
                    <li class="my-2 borda-dir-base lang-item <?php echo esc_attr( $slug ); ?> <?php echo esc_attr( $current_lang ); ?>">
                      <a class="btn btn-block <?php echo esc_attr( $slug ); ?>" hreflang="<?php echo esc_attr( $locale ); ?>" href="<?php echo esc_url( $url ); ?>" lang="<?php echo esc_attr( $locale ); ?>">
                        <h3 class="py-4">
                          <?php echo esc_html( $name ); ?>
                        </h3>
                      </a>
                    </li>
                  </div>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </ul>
                <?php endif; ?>
            
            <!-- Espaço -->
            <div class="espaco-80"></div>
        
        </main>

        <?php get_footer(); ?>
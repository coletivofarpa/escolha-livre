<?php
 /****************************************************
 * Registra as strings para tradução pelo Polylang
 * ***************************************************/
require_once get_template_directory() . '/includes/escolha-livre/traduzir-strings.php';


/****************************************************
* Adiciona suporte ao editor de blocos Gutenberg
* ***************************************************/
require_once get_template_directory() . '/includes/escolha-livre/blocos-suporte.php';


/****************************************************
* Adiciona suporte a Shortcodes
* ***************************************************/
require_once get_template_directory() . '/includes/escolha-livre/shortcodes.php';

/****************************************************
* Adiciona suporte a Shortcodes
* ***************************************************/
require_once get_template_directory() . '/includes/escolha-livre/trilha-de-navegacao.php';

/****************************************************
 * Personalização do painel personalizador do tema
 ***************************************************/
// Adiciona as configurações de personalização do logotipo do rodapé
function custom_footer_logo_customize_register( $wp_customize ) {
    // Adiciona uma seção para as opções do logotipo do rodapé
    $wp_customize->add_section( 'footer_logo_section' , array(
        'title'      => __( 'Opções de Logotipo do Rodapé', 'theme-escolha-livre' ),
        'priority'   => 30,
    ) );

    // Adiciona quatro configurações de logotipo e link do rodapé
    for ($i = 1; $i <= 4; $i++) {
        // Configuração do logotipo do rodapé
        $wp_customize->add_setting( 'footer_logo_image_' . $i, array(
            'default'     => '',
            'transport'   => 'refresh',
            'sanitize_callback' => 'custom_sanitize_footer_logo_image', // Adicionando a função de sanitização
        ) );

        // Controle de upload de imagem do logotipo do rodapé
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo_image_' . $i, array(
            'label'    => sprintf( __( 'Selecionar Imagem do Logotipo do Rodapé %d', 'theme-escolha-livre' ), $i ),
            'section'  => 'footer_logo_section',
            'settings' => 'footer_logo_image_' . $i,
        ) ) );

        // Configuração do link do logotipo do rodapé
        $wp_customize->add_setting( 'footer_logo_link_' . $i, array(
            'default'     => '',
            'transport'   => 'refresh',
            'sanitize_callback' => 'esc_url_raw', // Sanitizando como URL
        ) );

        // Controle de campo de texto para o link do logotipo do rodapé
        $wp_customize->add_control( 'footer_logo_link_' . $i, array(
            'label'    => sprintf( __( 'Link do Logotipo do Rodapé %d', 'theme-escolha-livre' ), $i ),
            'section'  => 'footer_logo_section',
            'type'     => 'text',
        ) );
    }
}

add_action( 'customize_register', 'custom_footer_logo_customize_register' );

// Função de sanitização para as imagens do logotipo do rodapé
function custom_sanitize_footer_logo_image( $input ) {
    return esc_url_raw( $input ); // Sanitizando como URL
}

 

/* NÃO EXCLUA ESSA TAG DE FECHAMENTO | NO ELIMINE ESTA ETIQUETA DE CIERRE | DON'T DELETE THIS CLOSING TAG */ ?>
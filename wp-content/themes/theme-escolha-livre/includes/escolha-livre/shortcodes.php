<?php
/* ***************
 * Títulos H1
 * ***************/
// [titulos_h1_shortcode campo="nome_do_campo"]Texto do título[/titulos_h1_shortcode]
// Função para o shortcode
function titulos_h1_shortcode_layout_componente($atts, $content = null) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Se o conteúdo for fornecido dentro do shortcode, use-o, caso contrário, recupere o valor do campo personalizado ACF
    $conteudo = !empty($content) ? $content : get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/titulos-h1.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('titulos_h1_shortcode', 'titulos_h1_shortcode_layout_componente');


/* **************
 * Audio Player
 * **************/
// [audioplayer_shortcode campo="nome_do_campo"]
// Função para o shortcode
function audio_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/audio-part.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('audioplayer_shortcode', 'audio_shortcode_layout_componente');


/* ***************
 * Imagem Legenda
 * ***************/
// [imagem_legenda_shortcode campo="nome_do_campo"]
// Função para o shortcode
function imagem_legenda_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/imagem-legenda.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('imagem_legenda_shortcode', 'imagem_legenda_shortcode_layout_componente');


/* ***************
 * Legenda Imagem
 * ***************/
// [legenda_imagem_shortcode campo="nome_do_campo"]
// Função para o shortcode
function legenda_imagem_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/legenda-imagem.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('legenda_imagem_shortcode', 'legenda_imagem_shortcode_layout_componente');


/* ***************
 * Banner Legenda
 * ***************/
// [banner_legenda_shortcode campo="nome_do_campo"]
// Função para o shortcode
function banner_legenda_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/banner-legenda.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('banner_legenda_shortcode', 'banner_legenda_shortcode_layout_componente');


/* ***************
 * Banner Legenda
 * ***************/
// [video_grande_shortcode campo="nome_do_campo"]
// Função para o shortcode
function video_grande_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/video-grande.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('video_grande_shortcode', 'video_grande_shortcode_layout_componente');


/* ***************
 * Audio Esquerda
 * ***************/
// [audio_esquerda_shortcode campo="nome_do_campo"]
// Função para o shortcode
function audio_esquerda_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/audio-esquerda.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('audio_esquerda_shortcode', 'audio_esquerda_shortcode_layout_componente');


/* ***************
 * Audio Direita
 * ***************/
// [audio_direita_shortcode campo="nome_do_campo"]
// Função para o shortcode
function audio_direita_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/audio-direita.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('audio_direita_shortcode', 'audio_direita_shortcode_layout_componente');


/* ***************
 * Audio Linha
 * ***************/
// [audio_linha_shortcode campo="nome_do_campo"]
// Função para o shortcode
function audio_linha_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/audio-linha.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('audio_linha_shortcode', 'audio_linha_shortcode_layout_componente');


/* ***************
 * Vídeo Esquerda
 * ***************/
// [video_esquerda_shortcode campo="nome_do_campo"]
// Função para o shortcode
function video_esquerda_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/video-esquerda.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('video_esquerda_shortcode', 'video_esquerda_shortcode_layout_componente');


/* ***************
 * Vídeo Direita
 * ***************/
// [video_direita_shortcode campo="nome_do_campo"]
// Função para o shortcode
function video_direita_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/video-direita.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('video_direita_shortcode', 'video_direita_shortcode_layout_componente');


/* ***************
 * Imagem Lightbox Esquerda
 * ***************/
// [imagem_lightbox_esquerda_shortcode campo="nome_do_campo"]
// Função para o shortcode
function imagem_lightbox_esquerda_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/imagem-lightbox-esquerda.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('imagem_lightbox_esquerda_shortcode', 'imagem_lightbox_esquerda_shortcode_layout_componente');


/* ***************
 * Imagem Lightbox Direita
 * ***************/
// [imagem_lightbox_direita_shortcode campo="nome_do_campo"]
// Função para o shortcode
function imagem_lightbox_direita_shortcode_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/imagem-lightbox-direita.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('imagem_lightbox_direita_shortcode', 'imagem_lightbox_direita_shortcode_layout_componente');


/* ***************
 * Recurso Relacionado Coluna 1
 * ***************/
// [recurso_relacionado_coluna1 campo="nome_do_campo"]
// Função para o shortcode
function recurso_relacionado_coluna1_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/recurso-relacionado-coluna1.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('recurso_relacionado_coluna1', 'recurso_relacionado_coluna1_layout_componente');


/* ***************
 * Recurso Relacionado Coluna 2
 * ***************/
// [recurso_relacionado_coluna2 campo="nome_do_campo"]
// Função para o shortcode
function recurso_relacionado_coluna2_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/recurso-relacionado-coluna2.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('recurso_relacionado_coluna2', 'recurso_relacionado_coluna2_layout_componente');


/* ***************
 * Recurso Relacionado Coluna 3
 * ***************/
// [recurso_relacionado_coluna3 campo="nome_do_campo"]
// Função para o shortcode
function recurso_relacionado_coluna3_layout_componente($atts) {
    // Extrai os atributos (opcional)
    extract(shortcode_atts(array(
        'campo' => 'nome_do_campo',
        'post_id' => get_the_ID(), // ID da postagem atual
    ), $atts));

    // Recupera o valor do campo personalizado ACF
    $conteudo = get_field($campo, $post_id);

    // Inicia o buffer de saída
    ob_start();

    // Inclui o layout do componente
    include( get_template_directory() . '/template/partials/recurso-relacionado-coluna3.php');

    // Captura o conteúdo do buffer e limpa o buffer
    $output = ob_get_clean();

    // Retorna o conteúdo
    return $output;
}
// Registra o shortcode
add_shortcode('recurso_relacionado_coluna3', 'recurso_relacionado_coluna3_layout_componente');
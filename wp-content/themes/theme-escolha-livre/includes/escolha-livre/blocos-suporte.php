<?php
// Adiciona suporte ao editor de blocos Gutenberg
// Torna tema WordPress 100% compatível com o editor de blocos/Gutenberg
function escolha_livre_gutenberg_support() {
    add_theme_support( 'wp-block-styles' ); // Ativa os estilos padrão dos blocos. Eram vistos no editor mas não ao publicar

    add_theme_support( 'align-wide' ); // Suporte a alinhamento ampla e completa. Adiciona as classes 'alignfull', 'alignwide' e ''(padrão vazio)

    add_theme_support( 'editor-styles' ); // Estilos do editor. Paletas de cores e tamanho de fontes

    add_theme_support( 'responsive-embeds' ); // Add classe 'wp-embed-responsive' às mídias encorporadas

    // Adiciona classe .has-principal-background-color
    add_theme_support( 'editor-color-palette', 
        array(
            array(
                'name' => __( 'Vermelha', 'theme-escolha-livre' ),
                'slug' => 'vermelha',
                'color' => '#d04c4c',
                // Defina suas cores personalizadas aqui.
            ),
            array(
                'name' => __( 'Azul', 'theme-escolha-livre'),
                'slug' => 'azul',
                'color' => '#38a1b7',
            ),
            array(
                'name' => __( 'Cinza Escura', 'theme-escolha-livre'),
                'slug' => 'cinzaescura',
                'color' => '#3f3f3f',
            ),
            array(
                'name' => __( 'Cinza Clara', 'theme-escolha-livre'),
                'slug' => 'cinzaclara',
                'color' => '#bcbcbc',
            ),
            array(
                'name' => __( 'Roxa', 'theme-escolha-livre'),
                'slug' => 'roxa',
                'color' => '#5c5f98',
            ),
            array(
                'name' => __( 'Verde', 'theme-escolha-livre'),
                'slug' => 'verde',
                'color' => '#1b6a76',
            ),
            array(
                'name' => __( 'Branca de Fundo', 'theme-escolha-livre'),
                'slug' => 'brancafundo',
                'color' => '#f5f5f5',
            ),
            array(
                'name' => __( 'Branca', 'theme-escolha-livre'),
                'slug' => 'branca',
                'color' => '#ffffff',
            ),
            array(
                'name' => __( 'Preta', 'theme-escolha-livre'),
                'slug' => 'preta',
                'color' => '#000000',
            ),
        ),

    );
    add_theme_support( 'diseble-custom-colors', 'theme-escolha-livre' );
}
add_action( 'after_setup_theme', 'escolha_livre_gutenberg_support' );
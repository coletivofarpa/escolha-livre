<?php
// [breadcrumbs]
// Função para exibir breadcrumbs com shortcode
function custom_breadcrumbs_shortcode() {
    // Iniciar a saída de breadcrumbs
    $breadcrumbs_output = '<div class="breadcrumbs">';
    // Incluir link para a página inicial
    $breadcrumbs_output .= '<a href="' . home_url() . '">Home</a>';

    // Verificar se estamos em uma página, post ou categoria
    if (is_category() || is_single()) {
        // Obter as categorias da publicação atual
        $categories = get_the_category();
        if ($categories) {
            // Loop através das categorias
            foreach ($categories as $category) {
                // Exibir um link para cada categoria
                $breadcrumbs_output .= '<span class="separator"> > </span><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
            }
        }
        // Se estivermos em um post, exibir o título do post
        if (is_single()) {
            $breadcrumbs_output .= '<span class="separator"> > </span>' . get_the_title();
        }
    } elseif (is_page()) {
        // Se estivermos em uma página, exibir o título da página
        $breadcrumbs_output .= '<span class="separator"> > </span>' . get_the_title();
    }

    // Fechar a saída de breadcrumbs
    $breadcrumbs_output .= '</div>';

    // Retornar os breadcrumbs
    return $breadcrumbs_output;
}
add_shortcode('breadcrumbs', 'custom_breadcrumbs_shortcode');


// Função para exibir breadcrumbs
/*
function custom_breadcrumbs() {
    // Iniciar a saída de breadcrumbs
    echo '<div class="breadcrumbs">';
    // Incluir link para a página inicial
    echo '<a href="' . home_url() . '">Home</a>';

    // Verificar se estamos em uma página, post ou categoria
    if (is_category() || is_single()) {
        // Obter as categorias da publicação atual
        $categories = get_the_category();
        if ($categories) {
            // Loop através das categorias
            foreach ($categories as $category) {
                // Exibir um link para cada categoria
                echo '<span class="separator"> / </span><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
            }
        }
        // Se estivermos em um post, exibir o título do post
        if (is_single()) {
            echo '<span class="separator"> / </span>' . get_the_title();
        }
    } elseif (is_page()) {
        // Se estivermos em uma página, exibir o título da página
        echo '<span class="separator"> / </span>' . get_the_title();
    }

    // Fechar a saída de breadcrumbs
    echo '</div>';
}
*/
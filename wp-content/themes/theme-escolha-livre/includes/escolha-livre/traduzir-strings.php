<?php
/* Registrar a string para tradução:
Use a função pll_register_string() para registrar a string que você deseja traduzir. Esta função deve ser chamada no lado administrativo (por exemplo, no arquivo functions.php do seu tema). Aqui está um exemplo de como você pode registrar sua string:
*/
pll_register_string('miolo_home', 'Aprenda', 'theme-escolha-livre');
pll_register_string('miolo_home', 'Recursos', 'theme-escolha-livre');
pll_register_string('miolo_home', 'Tutoriais', 'theme-escolha-livre');
pll_register_string('rodape', 'Realização:', 'theme-escolha-livre');
pll_register_string('rodape', 'Termos de uso', 'theme-escolha-livre');
pll_register_string('rodape', 'Política de privacidade', 'theme-escolha-livre');
pll_register_string('rodape', 'A não ser que indicado nos termos de uso, todo conteúdo do site está sob licença', 'theme-escolha-livre');
pll_register_string('rodape', 'Copyleft Escolha Livre | Criado com', 'theme-escolha-livre');
pll_register_string('rodape', 'pelo', 'theme-escolha-livre');
pll_register_string('rodape', 'utilizando tecnologias de Software Livre GPLv3+', 'theme-escolha-livre');
pll_register_string('titulo_filtro', 'Categorias', 'theme-escolha-livre');
pll_register_string('titulo_filtro', 'Temas', 'theme-escolha-livre');
pll_register_string('titulo_filtro', 'Recursos', 'theme-escolha-livre');
pll_register_string('filtros', 'Mídias', 'theme-escolha-livre');
pll_register_string('filtros', 'Ferramentas', 'theme-escolha-livre');
pll_register_string('filtros', 'Vídeos', 'theme-escolha-livre');
pll_register_string('filtros', 'Sons e música', 'theme-escolha-livre');
pll_register_string('filtros', 'Compartilhamento', 'theme-escolha-livre');
pll_register_string('filtros', 'Educacionais', 'theme-escolha-livre');
pll_register_string('filtros', 'Fotos e imagens', 'theme-escolha-livre');
pll_register_string('filtros', 'Diversos', 'theme-escolha-livre');
pll_register_string('filtros', 'Conferências virtuais', 'theme-escolha-livre');
pll_register_string('filtros', 'Textos/ Planilhas/Apresentações', 'theme-escolha-livre');
pll_register_string('filtros', 'Busca', 'theme-escolha-livre');
pll_register_string('filtros', 'Redes sociais', 'theme-escolha-livre');
pll_register_string('botão_recurso', 'Tutorial', 'theme-escolha-livre');
pll_register_string('pesquisar', 'digite aqui o que quer pesquisar', 'theme-escolha-livre');
pll_register_string('pesquisar', 'Pesquisar', 'theme-escolha-livre');
pll_register_string('pesquisar', 'Escolha o idioma', 'theme-escolha-livre');
pll_register_string('pesquisar', 'Escolha a categoria', 'theme-escolha-livre');
pll_register_string('menu', 'Recursos', 'theme-escolha-livre');
pll_register_string('menu', 'Aprenda', 'theme-escolha-livre');
pll_register_string('menu', 'Tutoriais', 'theme-escolha-livre');
pll_register_string('menu', 'Sobre', 'theme-escolha-livre');
pll_register_string('menu', 'Pesquisar', 'theme-escolha-livre');
pll_register_string('menu', 'Contato', 'theme-escolha-livre');

// Função para registrar as strings para tradução
function registrar_strings_para_traducao() {
  // Registra a string para o campo de nome
  pll_register_string('contato', 'Nome', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Escreva aqui seu nome', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Email', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Escreva aqui seu email', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Categoria', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Selecione uma opção', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Sugerir novo recurso', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Apontar um erro/sugerir mudança', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Comentário/elogio/crítica', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Digite sua mensagem', 'theme-escolha-livre', true);
  pll_register_string('contato', 'Enviar mensagem', 'theme-escolha-livre', true);

  // Adicione mais linhas semelhantes para outros campos que você deseja traduzir
}
add_action('after_setup_theme', 'registrar_strings_para_traducao');



/* DON'T DELETE THIS CLOSING TAG */?>
<?php $page = 'livre-e-aberto';
/* Template Name: Livre, gratis ou aberto?
 * @package escolha-livre
 */
?>

    <?php get_header(); ?>

        <main id="page-livre-e-aberto" class="container">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim Linha 1 -->

            <!-- Linha 2 Audio -->
            <section class="row mb-5">
                <div class="col-md-12 mb-5">
                    <p>
                        A palavra livre traz consigo vários significados, como gratuidade e liberdade. Grátis significa algo pelo qual não se paga diretamente, como quando se recebe um produto pelo qual não se desembolsa dinheiro.  Liberdade remete à redução ou inexistência de restrições - livre, no sentido de liberdade de imprensa. O software livre está associado ao conceito de liberdade e, muitas vezes, é também gratuito.
                    </p>
                    <br>
                    <p>
                        A palavra aberto tem se tornado cada vez mais comum, como nos termos "educação aberta", "dados abertos" e "acesso aberto". Os Recursos Educacionais Abertos (REA) são recursos e obras (livros, cursos, vídeos, textos, imagens, jogos etc.), impressos ou digitais, que, de acordo com a definição da UNESCO, podem ser acessados de forma gratuita por qualquer um, podendo ser livremente compartilhados.
                    </p>
                </div>

                <div class="px-5">
                    <div class="col-md-12 borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                        <!-- Linha interna -->
                        <div class="row">

                            <p class="pretto col-md-12 align-bottom m-0 p-3">
                                "É fundamental do ponto de vista da formação dessa juventude, que a gente trabalhe com todas essas soluções…livres e abertas e que serão, por consequência, criativas."
                            </p>

                            <img class="col-md-2 img-fluid py-3 img-thumbnail" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/pretto_nelson.jpg" width="230" height="230" alt="Foto de professor Nelson Pretto da UFBA" />

                            <div class="col-md-4 align-self-end py-3">
                                <p class="d-inline align-text-bottom">
                                    "Prof. Nelson Pretto
                                    <br><br>
                                    Professor Titular (e ativista) da Faculdade de Educação da Universidade Federal da Bahia."
                                </p>
                            </div>

                            <!-- Audio player -->
                            <div class="holder col-md-6 d-flex align-self-end p-0">
                                <div class="audio green-audio-player">
                                    <!-- Adicione a palavra "Escute" aqui -->
                                    <span class="listen-text text-light me-3">Escute</span>
                                    <div class="loading">
                                        <div class="spinner"></div>
                                    </div>
                                    <div class="play-pause-btn">  
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                            <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                        </svg>
                                    </div>

                                    <div class="controls">
                                        <span class="current-time">0:00</span>
                                        <div class="slider" data-direction="horizontal">
                                            <div class="progress">
                                                <div class="pin" id="progress-pin" data-method="rewind"></div>
                                            </div>
                                        </div>
                                        <span class="total-time">0:00</span>
                                    </div>

                                    <div class="volume">
                                        <div class="volume-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                            </svg>
                                        </div>
                                        <div class="volume-controls hidden">
                                            <div class="slider" data-direction="vertical">
                                                <div class="progress">
                                                    <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <audio crossorigin>
                                        <source src="https://localhost/escolha-livre/wp-content/uploads/2023/09/pretto1.mp3" type="audio/mp3">
                                    </audio>
                                </div>
                                <!-- Script do audio player acima -->
                                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                            </div>
                            <!-- /Fim do audio player -->
                        </div>

                    </div>
                    <!-- /Fim da Linha interna -->
                </div>
                <!-- /Fim da Coluna 2 -->
            </section>

            <!-- Linha 3 barra de pesquisa -->
            <section class="row">
                <!-- Coluna 0 -->
                <div class="col-12">
                    <!-- Agrupamento de inputs Bootstrap -->
                    <div class="input-group mb-3">
                        <!-- Contexto interno do agrupamento -->
                        <div class="input-group-append" role="group">
                            <!-- Botão com a lupa -->
                            <button id="botao-busca" class="borda-esq-topo btn align-items-center justify-content-center" type="button">
                                <!-- Ícone lupa -->
                                <svg xmlns:xlink="http://www.w3.org/1999/xlink" fill="none" width="40" xmlns="http://www.w3.org/2000/svg" style="-webkit-print-color-adjust:exact" id="screenshot-634cb711-062a-8080-8002-60898dbd88f3" version="1.1" viewBox="5965.5 867.5 40 32" height="32"><g id="shape-634cb711-062a-8080-8002-60898dbd88f3" rx="0" ry="0"><g id="shape-634cb711-062a-8080-8002-60898dbdc92e"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92e">
                                    <ellipse rx="11.839810094838867" ry="11.939804420205178" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92e" class="strokes"><g class="stroke-shape"><ellipse rx="11.839810094838867" ry="11.939804420205178" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc92f"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc92f"><ellipse rx="8.75116398314185" ry="8.825072832325418" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc92f" class="strokes"><g class="stroke-shape"><ellipse rx="8.75116398314185" ry="8.825072832325418" style="fill:none;fill-opacity:none;stroke-width:0.5;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" cx="5977.839810094839" cy="879.9398044202053" transform="matrix(1.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000)"></ellipse></g></g></g><g id="shape-634cb711-062a-8080-8002-60898dbdc930"><g class="fills" id="fills-634cb711-062a-8080-8002-60898dbdc930"><path rx="0" ry="0" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g><g id="strokes-634cb711-062a-8080-8002-60898dbdc930" class="strokes"><g class="stroke-shape"><path rx="0" ry="0" style="fill:none;fill-opacity:none;stroke-width:1;stroke:#3f3f3f;stroke-opacity:1;stroke-dasharray:" d="M5986.591,888.997L5997.428,895.922C5997.428,895.922,6009.367,901.093,6000.183,891.537C6000.183,891.537,6000.183,891.537,6000.183,891.537L5989.346,884.612"></path></g></g></g></g>
                                </svg>
                                <!-- /Fim Ícone lupa -->
                            </button>
                            <!-- /FIm do Botão com a lupa -->
                        </div>
                        <!-- /Fim do Contexto interno do agrupamento -->

                        <!-- Campo de entrada da busca por termo -->
                        <input id="campo-busca" class="borda-esq-topo form-control" type="text" placeholder="digite aqui o que quer pesquisar">
                        <!-- /Fim do Campo de entrada da busca por termo -->

                        <!-- Borda esquerda aberta no topo via :after pois não funciona com elemento html de entrada -->
                        <span class="borda-esq-topo-entrada"></span>
                        <!-- /Fim Borda esquerda aberta no topo -->
                    </div>
                    <!-- /Fim do Agrupamento de inputs -->
                </div>
                <!-- /Fim Coluna 0 -->

                <!-- Linha 4 Filtro 1/2 -->
                <div class="row m-1">

                    <h2 class="col-12 text-uppercase pb-2">
                        Filtro
                    </h2>

                    <!-- Coluna 1 -->
                    <div class="col-md-2">
                        <div class="row">

                            <h3 class="caixa col-12 borda-esq-topo my-2 d-flex align-items-center justify-content-center">Redes Sociais</h3>
                            <h3 class="caixa col-12 borda-esq-topo my-2 d-flex align-items-center justify-content-center">Conferências<br>Virtuais</h3>
                        </div>
                    </div>
                    <!-- /Fim Coluna 1 -->
                    <!-- Coluna 2 -->
                    <div class="col-md-6">
                        <div class="row">

                            <div class="meia-caixa borda-dir-base col my-2 d-flex align-items-center justify-content-center">
                                <h3>Busca</h3>
                            </div>
                            <div class="meia-caixa borda-dir-base col my-2 d-flex align-items-center justify-content-center">
                                <h3>Armazenamento</h3>
                            </div>
                            <div class="caixa borda-esq-base col-12 my-2 d-flex align-items-center justify-content-center">
                                <h3>Compartilhamento</h3>
                            </div>

                        </div>
                    </div>
                    <!-- /Fim Coluna 2 -->
                    <!-- Coluna 3 -->
                    <div class="col-md-4">
                        <div class="row">

                            <div class="caixa-2x col-12 borda-dir-topo my-2 d-flex align-items-center justify-content-center">
                                <h3 class="">Textos <br>Planilhas <br>Apresentações</h3>
                            </div>

                        </div>
                    </div>
                    <!-- /Fim Coluna 0 -->
                </div>
                <!-- /Fim da Linha 4 Filtro 1/2 -->
            </section>
            <!-- /Fim da Linha 3 -->
        </main>

<?php get_footer(); ?>
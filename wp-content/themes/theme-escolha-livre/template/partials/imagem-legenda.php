        <?php if (!empty(get_field('imagem_conteudo_1'))) { ?>
            <!-- Shortcode Imagem Legenda -->
            <!-- Imagem Conteúdo 1 -->
            <div class="col-12 d-flex justify-content-center">
                <div class="col col-lg-8 mb-5">
                <figure class="borda-esq-base row d-flex flex-row p-md-3">
                    <img class="img-fluid col-md-6 py-3" src="<?php echo get_field('imagem_conteudo_1'); ?>">
                    <figcaption class="col-md-6 align-items-end justify-content-center">
                        <?php //echo get_field('texto_legenda_1'); ?>
                        <?php 
                        // Verificar o idioma atual
                        $current_language = pll_current_language();
                      
                        // Obter o conteúdo do campo personalizado com base no idioma
                        if($current_language == 'pt_BR') {
                            echo get_field('texto_legenda_1');
                        } elseif($current_language == 'es') {
                            echo get_field('texto_legenda_1_es');
                        } elseif($current_language == 'en') {
                            echo get_field('texto_legenda_1_en');
                        } else {
                            // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                            echo get_field('texto_legenda_1');
                        }
                        ?>
                    </figcaption>
                </figure>
                </div>
            </div>
          <?php } ?>
        <!-- Shortcode Imagem Legenda -->
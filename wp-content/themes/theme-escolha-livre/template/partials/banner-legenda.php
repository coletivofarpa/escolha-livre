        <?php if (!empty(get_field('imagem_banner_3'))) { ?>
            <!-- Imagem Conteúdo 3 -->
            <div class="col-12 d-flex justify-content-center mb-5">
                <figure class="borda-esq-base row d-flex flex-row p-md-1">
                    <img class="img-fluid" src="<?php echo get_field('imagem_banner_3'); ?>">
                
                    <figcaption class="align-items-end justify-content-center">
                        <?php //echo get_field('texto_imagem_3'); // Foi editado para o termo abaixo ?>
                        <?php //echo get_field('legenda_banner'); ?>
                        <?php 
                            // Verificar o idioma atual
                            $current_language = pll_current_language();
                                        
                            // Obter o conteúdo do campo personalizado com base no idioma
                            if($current_language == 'pt_BR') {
                            echo get_field('field_655eb0b782032');
                            } elseif($current_language == 'es') {
                            echo get_field('field_6603fe376cbac');
                            } elseif($current_language == 'en') {
                            echo get_field('field_6603fe676cbad');
                            } else {
                            // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                            echo get_field('field_655eb0b782032');
                            }
                        ?>
                    </figcaption>
                
                </figure>
            </div>
            <?php } ?>
        <?php if (!empty(get_field('subtitulo_3'))) { ?>
            <!-- Coluna 3 -->
            <div class="col-md-12 mb-3">
                <!-- Caixa de texto 3 -->
                <div class="borda-dir-base">
                    <h3 class="fundo-preto p-3">
                        <?php //echo get_field('subtitulo_3'); ?>
                        <?php 
                            // Verificar o idioma atual
                            $current_language = pll_current_language();
                            // Obter o subtítulo com base no idioma
                            if($current_language == 'pt_BR') {
                                echo get_field('subtitulo_3');
                            } elseif($current_language == 'es') {
                                echo get_field('subtitulo_3_es');
                            } elseif($current_language == 'en') {
                                echo get_field('subtitulo_3_en');
                            } else {
                                // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                                echo get_field('subtitulo_3');
                            }
                        ?>
                    </h3>
                    <div class="p-3">
                        <?php //echo get_field('texto_da_caixa_3'); ?>
                        <?php 
                            // Verificar o idioma atual
                            $current_language = pll_current_language();                
                            // Obter o subtítulo com base no idioma
                            if($current_language == 'pt_BR') {
                                echo get_field('texto_da_caixa_3');
                            } elseif($current_language == 'es') {
                                echo get_field('texto_da_caixa_3_es');
                            } elseif($current_language == 'en') {
                                echo get_field('texto_da_caixa_3_en');
                            } else {
                                // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                                echo get_field('texto_da_caixa_3');
                            }
                        ?>
                    </div>
                    <?php if (!empty(get_field('audio_linque_3'))) { ?>
                        <div class="row justify-content-center">
                            <!-- Audio player -->
                            <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                                <div class="audio green-audio-player">
                                    <span class="listen-text text-light me-3">
                                        Escute
                                    </span>
                                    <div class="loading">
                                        <div class="spinner"></div>
                                    </div>
                                    <div class="play-pause-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                            <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                        </svg>
                                    </div>
                                    <div class="controls">
                                        <span class="current-time">
                                            0:00
                                        </span>
                                        <div class="slider" data-direction="horizontal">
                                            <div class="progress">
                                                <div class="pin" id="progress-pin" data-method="rewind"></div>
                                            </div>
                                        </div>
                                        <span class="total-time">
                                            0:00
                                        </span>
                                    </div>
                                    <div class="volume">
                                        <div class="volume-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                            </svg>
                                        </div>
                                        <div class="volume-controls hidden">
                                            <div class="slider" data-direction="vertical">
                                                <div class="progress">
                                                    <div class="pin" id="volume-pin" data-method="changeVolume">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <audio crossorigin>
                                        <source src="<?php echo get_field('audio_linque_3'); ?>" type="audio/mpeg">
                                    </audio>
                                </div>
                                <!-- Script do audio player acima -->
                                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /Fim da Coluna 3 -->
        <?php } ?>
    <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
      <div class="col-md-4 my-2">
        <!-- Recurso 2 -->
        <a class="link-offset-2 link-underline link-underline-opacity-0 d-block h-100" href="<?php echo get_field('recurso_2_linque'); ?>" target="_blank">
          <div class="borda-esq-topo d-flex align-items-center justify-content-center h-100">
            <figure class="figure p-3">
              <figcaption class="figure-caption text-center py-3">
                <h5>
                <?php //echo get_field('recurso_2_titulo'); ?>
                <?php // Verificar o idioma atual
                  $current_language = pll_current_language();
                  // Obter o subtítulo com base no idioma
                  if($current_language == 'pt_BR') {
                    echo get_field('recurso_2_titulo');
                  } elseif($current_language == 'es') {
                    echo get_field('recurso_2_titulo_es');
                  } elseif($current_language == 'en') {
                    echo get_field('recurso_2_titulo_en');
                  } else {
                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                    echo get_field('recurso_2_titulo');
                  }
                ?>
                </h5>
              </figcaption>
              <img src="<?php echo get_field('recurso_2_imagem'); ?>" class="img-fluid pt-3 pb-4" width="180" height="140" alt="...">
            </figure>
          </div>
        </a>
      </div>
    <?php } ?>

        <?php if (!empty(get_field('imagem_conteudo_2'))) { ?>
            <!-- Imagem Conteúdo 2 -->
            <div class="col-12 mb-5 d-flex justify-content-center">
                <div class="col col-lg-8">
                    <figure class="borda-esq-base row py-3">
                        <figcaption class="col-md-8 align-items-end justify-content-center">
                        <?php //echo get_field('texto_legenda_2'); ?>
                        <?php 
                            // Verificar o idioma atual
                            $current_language = pll_current_language();
                                    
                            // Obter o conteúdo do campo personalizado com base no idioma
                            if($current_language == 'pt_BR') {
                            echo get_field('texto_legenda_2');
                            } elseif($current_language == 'es') {
                            echo get_field('texto_legenda_2_es');
                            } elseif($current_language == 'en') {
                            echo get_field('texto_legenda_2_en');
                            } else {
                            // Caso o idioma não seja encontrado, exibir o conteúdo padrão
                            echo get_field('texto_legenda_2');
                            }
                        ?>
                        </figcaption>
                        <img class="img-fluid col-md-4 py-md-3 py-3" src="<?php echo get_field('imagem_conteudo_2'); ?>">
                    </figure>
                </div>
            </div>
        <?php } ?>
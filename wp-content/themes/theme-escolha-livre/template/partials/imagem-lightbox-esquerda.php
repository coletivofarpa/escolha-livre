        <?php if (!empty(get_field('imagem_1_titulo_4'))) { ?>
            <div class="col-md-6 my-2">
                <!-- Imagem Escrita 1 -->
                <a class="link-offset-2 link-underline link-underline-opacity-0" href="<?php echo get_field('abrir_a_imagem'); ?>">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">
                        <figure class="figure p-3">
                            <img src="<?php echo get_field('imagem_1_titulo_4'); ?>" class="img-fluid" width="600" alt="...">
                            <figcaption class="figure-caption text-center py-3">
                                <?php //echo get_field('legenda_imagem'); ?>
                                <?php 
                                    // Verificar o idioma atual
                                    $current_language = pll_current_language();
                                            
                                    // Obter o subtítulo com base no idioma
                                    if($current_language == 'pt_BR') {
                                    echo get_field('legenda_imagem');
                                    } elseif($current_language == 'es') {
                                    echo get_field('legenda_imagem_es');
                                    } elseif($current_language == 'en') {
                                    echo get_field('legenda_imagem_en');
                                    } else {
                                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                                    echo get_field('legenda_imagem');
                                    }
                                ?>
                            </figcaption>
                        </figure>
                    </div>
                </a>
            </div>
        <?php } ?>
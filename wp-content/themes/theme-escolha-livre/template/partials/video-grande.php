    <?php if (!empty(get_field('video_grande'))) { ?>
        <div class="row enviar-publicar mb-3">
            <!-- Caixa de vídeo Grande -->
            <div class="col-md-12">
                <div class="borda-esq-base">
                    <h3 class="fundo-preto p-3">
                        <?php //echo get_field('video_grande_titulo'); ?>
                        <?php 
                        // Verificar o idioma atual
                        $current_language = pll_current_language();
                        
                        // Obter o título do vídeo grande com base no idioma
                        if($current_language == 'pt_BR') {
                            echo get_field('video_grande_titulo');
                        } elseif($current_language == 'es') {
                            echo get_field('video_grande_titulo_es');
                        } elseif($current_language == 'en') {
                            echo get_field('video_grande_titulo_en');
                        } else {
                            // Caso o idioma não seja encontrado, exibir o título padrão
                            echo get_field('video_grande_titulo');
                        }
                        ?>
                    </h3>
                    <div class="p-3">
                        <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                            <iframe src="<?php echo get_field('video_grande'); ?>" allowfullscreen></iframe>
                        </div>
                        <figcaption class="figure-caption text-start py-3">
                            <?php //echo get_field('video_grande_legenda'); ?>
                            <?php 
                                // Obter a legenda do vídeo grande com base no idioma
                                if($current_language == 'pt_BR') {
                                echo get_field('video_grande_legenda');
                                } elseif($current_language == 'es') {
                                echo get_field('video_grande_legenda_es');
                                } elseif($current_language == 'en') {
                                echo get_field('video_grande_legenda_en');
                                } else {
                                // Caso o idioma não seja encontrado, exibir a legenda padrão
                                echo get_field('video_grande_legenda');
                                }
                            ?>
                        </figcaption>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
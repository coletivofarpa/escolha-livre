        <?php if (!empty(get_field('titulo_video_2'))) { ?>
            <div class="col-md-6">
                <!-- Caixa de texto 2 -->
                <div class="borda-dir-base">
                    <h3 class="fundo-preto p-3">
                        <?php //echo get_field('titulo_video_2'); ?>
                        <?php 
                            // Verificar o idioma atual
                            $current_language = pll_current_language();
                            
                            // Obter o subtítulo com base no idioma
                            if($current_language == 'pt_BR') {
                            echo get_field('titulo_video_2');
                            } elseif($current_language == 'es') {
                            echo get_field('titulo_video_2_es');
                            } elseif($current_language == 'en') {
                            echo get_field('titulo_video_2_en');
                            } else {
                            // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                            echo get_field('titulo_video_2');
                            }
                        ?>
                    </h3>
                    <div class="p-3">
                        <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                            <iframe src="<?php echo get_field('video_2'); ?>" allowfullscreen></iframe>
                        </div>
                        <figcaption class="figure-caption text-start py-3">
                            <?php //echo get_field('texto_video_2'); ?>
                            <?php 
                                // Verificar o idioma atual
                                $current_language = pll_current_language();
                                
                                // Obter o subtítulo com base no idioma
                                if($current_language == 'pt_BR') {
                                echo get_field('texto_video_2');
                                } elseif($current_language == 'es') {
                                echo get_field('texto_video_2_es');
                                } elseif($current_language == 'en') {
                                echo get_field('texto_video_2_en');
                                } else {
                                // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                                echo get_field('texto_video_2');
                                }
                            ?>
                        </figcaption>
                    </div>
                </div>
            </div>
        <?php } ?>
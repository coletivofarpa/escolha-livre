    <?php if (!empty(get_field('audio_linque'))) { ?>
        <!-- Componente audio-part.php -->
        <div id="audio" class="borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
            <!-- Linha interna -->
            <div class="row">
                <!-- SHORTCODE -->
                <!-- Título/introdução ao audio -->
                <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                    <?php //echo get_field('audio_titulo'); ?>
                    <?php 
                        // Verificar o idioma atual
                        $current_language = pll_current_language();

                        // Obter o subtítulo com base no idioma
                        if($current_language == 'pt_BR') {
                            echo get_field('audio_titulo');
                        } elseif($current_language == 'es') {
                            echo get_field('audio_titulo_es');
                        } elseif($current_language == 'en') {
                            echo get_field('audio_titulo_en');
                        } else {
                            // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                            echo get_field('audio_titulo');
                        }
                    ?>
                </p>

                <!-- Imagem da autoria do audio -->
                <?php
                    $image_url = get_field('audio_imagem');
                    if ($image_url) {
                        $attachment_id = attachment_url_to_postid($image_url);
                        $alt_text = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
                        if (empty($alt_text)) {
                            $alt_text = "Texto alternativo padrão";
                        }
                        echo '<img class="img-fluid" src="' . esc_url($image_url) . '" width="165" height="165" alt="' . esc_attr($alt_text) . '" />';
                    }
                ?>

                <div class="holder col d-flex align-self-end p-0">
                    <!-- Autoria do audio -->
                    <div class="align-self-end p-3">
                        <p class="d-inline align-text-bottom">
                            <?php //echo get_field('audio_autoria'); ?>
                            <?php 
                                // Verificar o idioma atual
                                $current_language = pll_current_language();

                                // Obter o subtítulo com base no idioma
                                if($current_language == 'pt_BR') {
                                    echo get_field('audio_autoria');
                                } elseif($current_language == 'es') {
                                    echo get_field('audio_autoria_es');
                                } elseif($current_language == 'en') {
                                    echo get_field('audio_autoria_en');
                                } else {
                                    // Caso o idioma não seja encontrado, exibir o subtítulo padrão
                                    echo get_field('audio_autoria');
                                }
                            ?>
                        </p>
                    </div>

                    <!-- Audio player -->
                    <div class="audio green-audio-player">
                        <span class="listen-text text-light me-3">Escute</span>
                        <div class="loading">
                            <div class="spinner"></div>
                        </div>
                        <div class="play-pause-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                            </svg>
                        </div>
                        <div class="controls">
                            <span class="current-time">0:00</span>
                            <div class="slider" data-direction="horizontal">
                                <div class="progress">
                                    <div class="pin" id="progress-pin" data-method="rewind"></div>
                                </div>
                            </div>
                            <span class="total-time">0:00</span>
                        </div>
                        <div class="volume">
                            <div class="volume-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                </svg>
                            </div>
                            <div class="volume-controls hidden">
                                <div class="slider" data-direction="vertical">
                                    <div class="progress">
                                        <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <audio crossorigin>
                            <source src="<?php echo get_field('audio_linque'); ?>" type="audio/mpeg">
                        </audio>
                    </div>
                    <!-- Script do audio player acima -->
                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                </div>
            </div>
            <!-- /Fim da Linha interna -->
        </div>
        <!-- /Fim do componente audio-part.php -->
    <?php } ?>
<?php $page='escrita-colaborativa';
/* Template Name: Escrita Colaborativa
 * @package escolha-livre
 */
?>

<?php get_header(); ?>

        <main id="page-escrita-colaborativa" class="container">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row pb-5">
                <div class="col-md-12">
                    <p class="pb-3">
                        Muitas vezes criamos atividades individuais que poderiam ser muito mais ricas (em processo e produto) se fossem feitas de forma colaborativa. Ao invés de produzir dezenas de resenhas individuais sobre um artigo ou filme, não seria melhor ter uma produção colaborativa? Muitas pessoas podem editar um texto, uma planilha, uma apresentação, ou vários outros tipos de documento, ao mesmo tempo (síncrono) ou em momentos distintos (assíncrono). Algumas possibilidades incluem:
                    </p>
                    <ol class="pb-3">
                        <li>
                            1. Criar recursos em grupo em uma plataforma forma aberta, para que outros alunos e o professor possam acompanhar o desenvolvimento do produto, cujo resultado final seja público;
                        </li>
                        <li>
                            2. Criar um espaço para coletar dados (links, referências, pequenos trechos de texto) que podem ser úteis para todo o coletivo de alunos;
                        </li>
                        <li>
                            3. Criação de um artigo ou texto ao longo do semestre com comentários de colegas e dos docentes.
                        </li>
                    </ol>
                    <p class="pb-3">
                        Arquivos publicados em redes sociais e em Muitas vezes, os alunos somente compartilham uma atividade quando ela está pronta. Por que já não iniciar de forma aberta e colaborativa? Ao criar um novo recurso, considere usar um espaço aberto que permita a outros (alunos, inclusive) ver, comentar e colaborar. 
                    </p>
                    <p class="pb-3">
                        Existem diferentes modalidades e possibilidades da escrita colaborativa, incluindo o uso de tradicionais editores de texto. Vamos explorar! 
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="px-5">
                        <div class="borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                            <!-- Linha interna -->
                            <div class="row">

                                <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                                    Eu fui refletindo que isso seria uma possibilidade…que a educação e minha prática como professora pudesse cada vez mais incorporar os conceitos e a filosofia do software livre; tentando compreender a minha sala de aula também como um espaço de criação de colaboração de generosidade…
                                </p>

                                <img class="col-md-2 img-fluid py-3" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/carla_loureiro.jpg" width="400" height="533" alt="Foto de professor Nelson Pretto da UFBA" />
                                
                                <div class="col-md-4 align-self-end py-3">
                                    <p class="d-inline align-text-bottom">
                                        Profa. Carla Loureiro
                                        <br><br>
                                        Professora dos Anos Iniciais do Colégio de Aplicação da UFSC. Doutora em Educação. Trabalha há 30 anos como professora de crianças
                                    </p>
                                </div>

                                <!-- Audio player -->
                                <div class="holder col-md-6 d-flex align-self-end p-0">
                                    <div class="audio green-audio-player">
                                        <!-- Adicione a palavra "Escute" aqui -->
                                        <span class="listen-text text-light me-3">Escute</span>
                                        <div class="loading">
                                            <div class="spinner"></div>
                                        </div>
                                        <div class="play-pause-btn">  
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                            </svg>
                                        </div>

                                        <div class="controls">
                                            <span class="current-time">0:00</span>
                                            <div class="slider" data-direction="horizontal">
                                                <div class="progress">
                                                    <div class="pin" id="progress-pin" data-method="rewind"></div>
                                                </div>
                                            </div>
                                            <span class="total-time">0:00</span>
                                        </div>

                                        <div class="volume">
                                            <div class="volume-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                                </svg>
                                            </div>
                                            <div class="volume-controls hidden">
                                                <div class="slider" data-direction="vertical">
                                                    <div class="progress">
                                                        <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <audio crossorigin>
                                            <source src="https://archive.org/download/gislaine-software-livre/gislaine.mp3" type="audio/mp3">
                                        </audio>
                                    </div>
                                    <!-- Script do audio player acima -->
                                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                                </div>
                                <!-- /Fim do audio player -->
                            </div>

                        </div>
                        <!-- /Fim da Linha interna -->
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

            <!-- Linha 4 -->
            <div id="online-ofline" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Online ou offline?
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 4 -->

            <!-- Linha 5 -->
            <div class="row enviar-publicar">

                <div class="col-md-6">
                    <div class="borda-esq-base">

                        <h3 class="fundo-preto p-3">Um de cada vez</h3>

                        <p class="p-3">
                            No seu computador, é possível utilizar processadores de texto e pacotes de produtividade (como o LibreOffice) para trabalhar colaborativamente. Esses programas permitem registrar e visualizar as alterações feitas por cada autor, apontar comentários e sugestões de edição ao longo do texto, possibilitando que esse histórico seja registrado no processo de colaboração.
                        </p>
                        <p class="p-3">
                            No entanto, por serem ferramentas offline (instaladas no computador de cada pessoa), demandam que os autores trabalhem no arquivo e, uma vez finalizada uma parte, o enviem a outra pessoa para que o trabalho seja continuado.
                        </p>
                        <p class="p-3">
                            De modo alternativo, é possível utilizar arquivos distintos que, depois, são reunidos em um só. Isso torna mais complexa a junção de diversas edições e modificações feitas por pessoas trabalhando de forma independente. Nesse caso, sempre haverá a necessidade de que alguém mescle o conjunto das alterações realizadas em um único documento e, de forma centralizada, aprove (ou não) o que deverá constar no texto final.
                        </p>
                        <p class="p-3">
                            É um formato muito útil para quando não podemos estar todos online, ou, quando cada participante precisa de tempo para refletir e trabalhar no documento antes de encaminhá-lo a outro colaborador.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <div class="borda-dir-base">

                        <h3 class="fundo-preto p-3">Todo mundo junto</h3>

                        <p class="p-3">
                            O processo colaborativo online é instantâneo. Para além de comentários e recomendações, esse modelo permite a edição simultânea de documentos por diversas pessoas; facilita a comunicação entre os atores durante a edição do documento e possibilita que várias versões do documento sejam armazenadas.
                        </p>
                        <p class="p-3">
                            Algumas plataformas online são tão robustas quanto os editores de texto que você tem no seu computador, demandando a criação de uma conta e um cadastro (de ao menos um usuário). São relativamente “pesadas” por exigirem uma boa conexão. 
                        </p>
                        <p class="p-3">
                            Formas mais radicalmente abertas (como pads) permitem que usuários editem documentos sem cadastro. Esses sistemas guardam versões antigas do que foi editado (histórico) e permitem a edição anônima, sem cadastro. 
                        </p>
                    </div>
                </div>
                
            </div>
            <!-- /Fim da Linha 5 -->

            <!-- Linha 6 -->
            <div id="modelos-de-colaboracao" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Modelos de <br>Colaboração
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 6 -->

            <!-- Linha 7 -->
            <div class="row">

                <div class="col-md-6 mb-3 pb-3">

                    <figure class="figure">
                        <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/escrita-colaborativa.png" class="figure-img img-fluid rounded" alt="...">
                        <figcaption class="figure-caption d-flex align-items-center justify-content-center">
                            Como realizar a Escrita Colaborativa
                        </figcaption>
                    </figure>

                </div>

                <div class="col-md-6 mb-3 pb-3">

                    <figure class="figure">
                        <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/escrita-colaborativa-.jpg" class="figure-img img-fluid rounded" alt="...">
                        <figcaption class="figure-caption d-flex align-items-center justify-content-center">
                            Exemplos de softwares e características
                        </figcaption>
                    </figure>

                </div>
                
            </div>
            <!-- /Fim da Linha 7 -->

            <!-- Linha 8 -->
            <div id="recursos" class="row titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        Recursos
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 8 -->

            <!-- Linha 9 -->
            <div class="row">
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">Etherpad</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/etherpad_logo-original.png" class="img-fluid py-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-esq-topo d-flex align-items-center justify-content-center">

                        <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">OnlyOffice</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo-onlyoffice.png" class="img-fluid pt-3 pb-3" width="150" alt="...">
                        </figure>

                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="borda-dir-topo d-flex align-items-center justify-content-center">
                    <figure class="figure p-3">
                            <figcaption class="figure-caption text-center py-3">Wikiversidade</figcaption>
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/logo-wikiversidade.png" class="img-fluid pb-5" width="150" alt="...">
                        </figure>
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 9-->

            <!-- Linha 10 -->
            <div class="row">
                    
                <!-- Coluna Migalhas de pão -->
                <div class="migalhas col-md-12 pb-5">

                    <!-- Migalhas de pão -->
                    <nav aria-label="breadcrumb">
                        <!-- .linque-verde - Cor do hover -->
                        <ol class="linque-verde breadcrumb d-flex justify-content-end">
                            <li class="breadcrumb-item">
                                <a href="<?php echo get_site_url(); ?>/">Conheça mais recursos ></a>
                            </li>
                        </ol>
                    </nav>
                    
                </div>
                <!-- /Fim da Linha 10 -->

        </main>

<?php get_footer(); ?>
<?php $page = 'direitos-e-privacidade';
/* Template Name: Direitos e privacidade
 * @package escolha-livre
 */
?>

        <?php get_header(); ?>

        <main id="page-direitos-e-privacidade" class="container pb-5">
            
            <!-- Linha 1 -->
            <div class="titulo-h1 d-flex align-items-center justify-content-center">
                <div class="col-12 separador">
                    <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                        <?php esc_html_e(single_post_title('', false)); ?>
                    </h1>
                </div>
            </div>
            <!-- /Fim da Linha 1 -->

            <!-- Linha 2 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="pb-3">
                        É difícil realizar qualquer tarefa sem que dados criados por nós, ou dados que resultam de nossas ações, circulem por grandes empresas responsáveis por oferecer serviços gratuitos na forma de espaços para colaboração, produção e compartilhamento de conteúdos. Aos poucos, através do projeto Educação Vigiada, estamos aprendendo sobre as relações entre redes de ensino e instituições com essas empresas.
                    </p>
                    <p class="pb-3">
                        Sabemos pouco sobre como as plataformas efetivamente funcionam pois o código, ou software, não é aberto. Mas, conhecemos muito bem o modelo de negócios. Muitas delas se beneficiam da introdução de seus serviços para alunos criando uma “fidelização”.  As pessoas se acostumam a utilizar e acabam utilizando por toda a vida.
                    </p>
                    <p class="pb-3">
                        Outra grande preocupação é que diversos desses serviços se utilizam de uma quantidade enorme de dados que produzimos (planos de aula, fotos, tarefas, vídeos, texto, etc.), ou dados gerados através do nosso comportamento online (o que visualizamos, com quem falamos, onde estamos, etc.) para criar perfis, nos enviar propaganda e vender nossos dados. Nem sempre (ou quase nunca!) lemos os termos de uso das plataformas que utilizamos.
                    </p>
                    <p class="pb-3">
                        Mesmo tratando dos dados de forma agregada, os algoritmos das plataformas nos retornam informações, selecionando e promovendo conteúdos, indicando vídeos, fotos, notícias e contatos. Esses mecanismos podem, como ficou evidente em eleições ao redor do mundo, fortalecer visões de mundo, radicalismos e preconceitos – isto é, criar bolhas. A preocupação com esse tema se tornou um imperativo para instituições e redes de ensino com a  Lei Geral de Proteção de Dados, como explica Priscila Gonsales do Instituto Educadigital e da Iniciativa Educação Aberta.
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 2 -->

            <!-- Linha 3 -->
            <div class="row py-5">
                <div class="col-md-12 pt-3 pb-2 borda-esq-base">
                    <img class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/banner.jpg" width="1300" height="317" alt="banner" />
                </div>
            </div>
            <!-- /Fim da Linha 3 -->

            <!-- Linha 4 -->
            <div class="row">
                <div class="col-md-12">
                    <p class="pb-3">
                        A utilização de software livre e projetos em REA estão associados a um olhar mais cuidadoso para esse problema. Não há uma garantia de que um serviço de software livre não fará uso de dados dos internautas como forma de remuneração, por exemplo, mas essa informação será muito mais transparente. Ademais, a possibilidade de instalação do serviço em uma máquina da sua instituição (ou no seu computador) faz com que você tenha um controle muito maior do que acontece com o serviço.
                    </p>
                    <p class="pb-3">
                        Por último, o código é aberto, o que possibilita que qualquer um poderá analisá-lo e procurar por problemas ou potenciais violações de direitos. Mesmo que você não saiba programar ou ler o código, dezenas de outras pessoas que participam dos projetos têm acesso a ele. Assim, a chance de acontecerem violações aos seus direitos é proporcionalmente menor, uma vez considerado o número de entidades e pessoas independentes que podem visualizar o código do software.
                    </p>
                    <p class="pb-3">
                        Os professores da UFPA Leonardo Cruz e Filipe Saraiva, do projeto Educação Vigiada, foram entrevistados no Tecnopolitica sobre esse problema. Confira!
                    </p>
                </div>
            </div>
            <!-- /Fim da Linha 4 -->

            <!-- Linha 5 -->
            <div class="row p-5">
                <div class="col-md-12">

                    <!-- Linha interna -->
                    <div class="row borda-esq-base d-flex align-items-center justify-content-center p-0 mb-5">

                        <h2 class="videos codigo-aberto-livre col-md-12 align-srart m-0 p-3">
                            Tecnopolítica: Educação vigiada
                        </h2>

                        <div class="col-md-12 d-flex align-items-center justify-content-center align-self-end py-3">
                            <iframe src="https://player.vimeo.com/video/461392848?h=c607a02ac6&color=ff0179" width="660" height="390" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>
                    <!-- /Fim da Linha interna -->
                    
                </div>
            </div>
            <!-- /Fim da Linha 5 -->

            <!-- Linha 6 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="px-5">
                        <div class="col-md-12 borda-esq-base d-flex align-items-center justify-content-bottom p-0 mb-5">
                            <!-- Linha interna -->
                            <div class="row">

                                <p class="cita-audio col-md-12 align-bottom m-0 p-3">
                                    A gente é viciado desde pequeno a utilizar software proprietário… A gente tem que sair desse vício… Na educação a gente tem muito mais a ver com software livre do que com o software proprietário.
                                </p>

                                <img class="col-md-2 img-fluid py-3" src="<?php echo get_site_url(); ?>/wp-content/uploads/2023/09/bordignon.jpg" width="400" height="533" alt="Foto de professor Nelson Pretto da UFBA" />
                                
                                <div class="col-md-4 align-self-end py-3">
                                    <p class="d-inline align-text-bottom">
                                        Prof. André Luis Bordignon
                                        <br><br>
                                        É professor do Instituto Federal de São Paulo, na área de computação. Um dos fundadores da organização Minha Campinas.
                                    </p>
                                </div>

                                <!-- Audio player -->
                                <div class="holder col-md-6 d-flex align-self-end p-0">
                                    <div class="audio green-audio-player">
                                        <!-- Adicione a palavra "Escute" aqui -->
                                        <span class="listen-text text-light me-3">Escute</span>
                                        <div class="loading">
                                            <div class="spinner"></div>
                                        </div>
                                        <div class="play-pause-btn">  
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                            </svg>
                                        </div>

                                        <div class="controls">
                                            <span class="current-time">0:00</span>
                                            <div class="slider" data-direction="horizontal">
                                                <div class="progress">
                                                    <div class="pin" id="progress-pin" data-method="rewind"></div>
                                                </div>
                                            </div>
                                            <span class="total-time">0:00</span>
                                        </div>

                                        <div class="volume">
                                            <div class="volume-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                                                </svg>
                                            </div>
                                            <div class="volume-controls hidden">
                                                <div class="slider" data-direction="vertical">
                                                    <div class="progress">
                                                        <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <audio crossorigin>
                                            <source src="https://archive.org/download/andre-escolha-livre/andre.mp3" type="audio/mp3">
                                        </audio>
                                    </div>
                                    <!-- Script do audio player acima -->
                                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                                </div>
                                <!-- /Fim do audio player -->
                            </div>

                        </div>
                        <!-- /Fim da Linha interna -->
                    </div>
                </div>
            </div>
            <!-- /Fim da Linha 6 -->

        </main>

        <?php get_footer(); ?>
<?php

if (isset($_POST['submit'])) {
  // Get form data
  $name = $_POST['name'];
  $email = $_POST['email'];
  $message = $_POST['message'];

  echo $name

  // Set the recipient email address
  $to = 'fferraz.carol@gmail.com';

  // Set the email subject
  $subject = 'New Contact Form Submission from ' . $name;

  // Build the email message
  $messageBody = "Name: $name\n";
  $messageBody .= "Email: $email\n\n";
  $messageBody .= "Message:\n$message";

  // Set headers
  $headers = "From: $email" . "\r\n" .
    "Reply-To: $email" . "\r\n" .
    "X-Mailer: PHP/" . phpversion();

  // Send the email
  if (mail($to, $subject, $messageBody, $headers)) {
    echo "Email sent successfully!";
  } else {
    $error = error_get_last();
    echo "Error sending email. Please try again later. Error details: " . print_r($error, true);
  }
}

<?php $page = 'recursos';
/* Template Name: Recursos
 * @package escolha-livre
 */
get_header(); ?>

    <!-- identação do html da header -->
    <main id="page-recursos" class="container">

      <!-- Espaço -->
      <div class="espaco-80"></div>

      <div class="titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
              <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                <?php //esc_html_e(single_post_title('', false), 'theme-escolha-livre'); ?>
                <?php
                  // Permitindo html seguro ao filtrar/ascapar tags inseguras 
                  $title = get_the_title();
                  $allowed_tags = array(
                    'br' => array(),
                    'a' => array(
                      'href' => array(),
                      'title' => array()
                    ),
                    'em' => array(),
                    'strong' => array(),
                    'p' => array(),
                    'span' => array(),
                    // Adicione outras tags permitidas aqui, se necessário
                  );
                  echo wp_kses($title, $allowed_tags);
                ?>
              </h1>
          </div>
      </div>

      <!-- Espaço -->
      <div class="espaco-60"></div>

      <?php
        // Função para obter posts
        function get_posts_data() {
          $posts = array();
          $args = array('post_type'   => 'recurso','post_status' => 'publish','orderby'   => array('date' =>'DESC'));

          $data = new WP_Query( $args );

          if( $data->have_posts() ){
            while( $data->have_posts() ){
                $data->the_post();
                $post_content = get_the_content();

                // get acf
                $resumo = get_field('resumo');
                $resumo_es = get_field('resumo_es');
                $resumo_en = get_field('resumo_en');
                $imagem = get_field('imagem');
                $tema = get_field('tema');
                $categoria = get_field('categoria');
                $tutorial = get_field('tutorial');
                $tituloLinque = get_field('titulo_linque');

                // limita a string do resumo
                $resumo_limit = $resumo ? substr($resumo, 0, 10000) : '';
                $resumo_limit_es = $resumo_es ? substr($resumo_es, 0, 10000) : '';
                $resumo_limit_en = $resumo_en ? substr($resumo_en, 0, 10000) : '';

                array_push($posts, array(
                  'id' => get_the_ID(), // Obter o ID da postagem
                  'title' => get_the_title(),
                  'imagem' => $imagem,
                  'resumo_limit' => $resumo_limit,
                  'resumo_limit_es' => $resumo_limit_es,
                  'resumo_limit_en' => $resumo_limit_en,
                  'temas' => $tema,
                  'categorias' => $categoria,
                  'tutorial' => $tutorial,
                  'titulo_linque' => $tituloLinque,
                  )
                );
              }
            }
            return $posts;
          }

          // Função para obter categorias e temas
          function get_categories_and_themes($posts) {
            $categories = array();
            $themes = array();

            foreach ($posts as $post) {
              if (isset($post['categorias']) && is_array($post['categorias'])) {
                $categories = array_merge($categories, $post['categorias']);
              }
              
              if (isset($post['temas']) && is_array($post['temas'])) {
                $themes = array_merge($themes, $post['temas']);
              }
            }

            $categories = array_unique($categories);
            $themes = array_unique($themes);

            return array('categories' => $categories, 'themes' => $themes);
          }

          $posts = get_posts_data();
          $categories_and_themes = get_categories_and_themes($posts);
          
          $categories = $categories_and_themes['categories'];
          $themes = $categories_and_themes['themes'];

          // Obtém o idioma atual usando Polylang
          $current_language = function_exists('pll_current_language') ? pll_current_language() : 'pt_BR';
            
          ?>

      <!-- Subtítulo Categorias -->
      <div class="col-12" id="titulo-filtro">
          <h2 class="mb-2 small-text">
              <?php 
              // Exibir a string traduzida
              echo esc_html( pll__( 'Categorias', 'theme-escolha-livre' ));
              ?>
          </h2>
      </div>
          
      <!-- Linha 2 -->
      <div class="row" id="filtro-div-categorias">
        <?php foreach ($categories as $key => $item) { ?>
        <div class="col-6 col-sm-6 col-md-4 mb-3 ps-0">
          <div class="card filtro armazenamento card d-flex align-items-center justify-content-center w-100 lnk-<?php echo formatString($item)?> categoria" onClick="filterPage('<?php echo formatString($item)?>', 'categoria')" id="card-filtro">
            <?php //echo $item?>
            <?php echo esc_html( pll__( $item, 'theme-escolha-livre' )); ?>
          </div>
        </div>
        <?php } ?>
      </div>

      <!-- Espaço -->
      <div class="espaco-40"></div>

      <div class="col-12" id="titulo-filtro">
        <h2 class="mb-2 small-text">
          <?php 
            // Exibir a string traduzida
            echo esc_html( pll__( 'Temas', 'theme-escolha-livre' ));
          ?>
        </h2>
      </div>

      <!-- Linha 3 -->
      <div class="row" id="filtro-div-temas">

        <?php foreach ($themes as $key => $item) { ?>
        <div class="col-6 col-sm-6 col-md-4 mb-3 ps-0">
          <div class="card filtro armazenamento card d-flex align-items-center justify-content-center w-100 lnk-<?php echo formatString($item)?> tema" onClick="filterPage('<?php echo formatString($item)?>', 'tema')" id="card-filtro">
            <?php //echo $item?>
            <?php echo esc_html( pll__( $item, 'theme-escolha-livre' )); ?>
          </div>
        </div>
        <?php } ?>

      </div>

      <!-- Espaço -->
      <div class="espaco-40"></div>
      
      <div class="col-12" id="subtitulo-recursos">
        <h2 class="mb-2 small-text">
          <?php echo esc_html( pll__( 'Recursos', 'theme-escolha-livre' )); ?>
        </h2>
      </div>

      <!-- Linha 4 -->
      <?php foreach ($posts as $key => $post) : ?>
      <div class="list-all 
        <?php foreach ($post['categorias'] as $k => $category) : ?>
        <?php echo formatString($category) . ' '; ?>
        <?php endforeach; ?>
        <?php foreach ($post['temas'] as $k => $theme) : ?>
        <?php echo formatString($theme) . ' '; ?>
        <?php endforeach; ?>">

        <div class="card recursos">
          <div class="row align-items-center g-0">

            <h3 class="col-12 card-header-recursos p-3">

              <?php if (!empty($post['titulo_linque'])) { ?>
              <a class="text-decoration-none" href="<?php echo esc_url($post['titulo_linque']); ?>" target="_blank">
              <?php } ?>

                <div class="texto-header-recursos">
                  <?php echo $post['title'] ?>
                </div>

                <?php if (!empty($post['titulo_linque'])) { ?>
              </a>
              <?php } ?>
            </h3>

            <div class="col-md-4 p-4 d-flex align-items-center justify-content-center">
              <img class="img-fluid" width="230" height="170" src="<?php echo $post['imagem'] ?>" alt="img...">
            </div>

            <div class="col-md-8 texto-card-recurso">
              <div class="texto-cards p-5 pb-0">
                <p class="texto-card-recursos">
                  <?php 
                    // Obter o resumo do post com base no idioma
                    if($current_language == 'pt_BR' && !empty($post['resumo_limit'])) {
                      echo $post['resumo_limit'];
                    } elseif($current_language == 'es' && !empty($post['resumo_limit_es'])) {
                      echo $post['resumo_limit_es'];
                    } elseif($current_language == 'en' && !empty($post['resumo_limit_en'])) {
                      echo $post['resumo_limit_en'];
                    } else {
                      // Caso o resumo no idioma específico não esteja disponível, exibir o resumo padrão
                      echo $post['resumo_limit'];
                    }
                  ?>
                </p>
              </div>
              <a class="m-5" href="<?php echo esc_url($post['tutorial']); ?>" target="_blank">
                
                <div class="mb-5 p-5">
                  <?php if (!empty($post['tutorial'])) { ?>
                  <button class="botao-card-recursos">
                    <?php echo esc_html( pll__( 'Tutorial', 'theme-escolha-livre' )); ?>
                  </button>
                  <?php } ?>
                </div>                
              </a>
            </div>
          </div>
        </div>
        <!-- Espaço -->
        <div class="espaco-40"></div>
      </div>
      <?php endforeach; ?>
      <!-- /FIM da Linha 4 -->
    </main>
    <script src="<?php echo get_template_directory_uri(); ?>/library/js/filtros-recursos.js"></script>
    
    <div class="espaco-40"></div>

<?php get_footer(); ?>


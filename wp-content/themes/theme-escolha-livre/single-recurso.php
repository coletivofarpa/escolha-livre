<?php
/* Template Name: Single Recursos
 * @package escolha-livre
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>

		<main id="single-aprenda" class="container pb-5">
      <!-- Linha 1 -->
			<section>
        <!-- Título 1 da Página -->
        <div class="titulo-h1 d-flex align-items-center justify-content-center">
            <div class="col-12 separador">
                <h1 class="text-uppercase me-5 text-decoration-none text-muted">
                    <?php esc_html_e(single_post_title('', false)); ?>
                </h1>
            </div>
        </div>
        <!-- /Fim da Linha 1 -->

        <!-- Linha 2 -->
        <!-- Conteúdo textual -->
        <div class="row pb-5 mb-3">
          <div class="col-md-12">
            <?php echo the_content() ?>
          </div>
          
          <?php if (!empty(get_field('imagem_conteudo_1'))) { ?>
          <!-- Imagem Conteúdo 1 -->
          <div class="col-12 d-flex justify-content-center">
            <div class="col-8 mb-5">
              <figure class="borda-esq-base row d-flex flex-row p-md-3">
                <img class="img-fluid col-md-6 py-3" src="<?php echo get_field('imagem_conteudo_1'); ?>">
                <figcaption class="col-md-6 d-flex flex-row align-items-end justify-content-center">
                  <p>
                    <?php echo get_field('texto_imagem_1'); ?>
                  </p>
                </figcaption>
              </figure>
            </div>
          </div>
          <?php } ?>

          <?php if (!empty(get_field('imagem_conteudo_1'))) { ?>
          <!-- Texto Conteúdo 1 -->
          <div class="texto-div col-12 mt-3 mb-5">
            <?php echo get_field('texto_conteudo_1'); ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 1 -->
          <?php } ?>
          
          <?php if (!empty(get_field('imagem_conteudo_2'))) { ?>
          <!-- Imagem Conteúdo 2 -->
          <div class="col-12 mb-5 d-flex justify-content-center">
            <div class="col-8">
              <figure class="borda-esq-base row py-3">
                <figcaption class="col-md-8 d-flex flex-row justify-content-center">
                  <?php echo get_field('texto_imagem_2'); ?>
                </figcaption>
                <img class="img-fluid col-md-4 py-md-3 py-3" src="<?php echo get_field('imagem_conteudo_2'); ?>">
              </figure>
            </div>
          </div>
          <?php } ?>
          
          <?php if (!empty(get_field('texto_conteudo_2'))) { ?>
          <!-- Texto Conteúdo 2 -->
          <div class="texto-div col-12 mt-3 mb-5">
            <?php echo get_field('texto_conteudo_2'); ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 2 -->
          <?php } ?>

          <?php if (!empty(get_field('imagem_conteudo_3'))) { ?>
          <!-- Imagem Conteúdo 3 -->
          <div class="col-12 d-flex justify-content-center mb-5">
              <figure class="borda-esq-base row d-flex flex-row p-md-1">
                <img class="img-fluid" src="<?php echo get_field('imagem_conteudo_3'); ?>">
              <!--
                <figcaption class="d-flex flex-row align-items-end justify-content-center">

                </figcaption>
              -->
              </figure>
          </div>
          <?php } ?>

          <?php if (!empty(get_field('texto_conteudo_3'))) { ?>
          <div class="texto-div col-12 mt-3 mb-5">
              <?php echo get_field('texto_conteudo_3'); ?>
          </div>
          <!-- /Fim da Imagem Conteúdo 3 -->
          <?php } ?>

        </div>
        <!-- /Fim da Linha 2 -->

        <!-- Linha 3 Vídeo Grande -->
        <div class="row enviar-publicar">
          <?php if (!empty(get_field('video_grande'))) { ?>
          <!-- Caixa de vídeo Grande -->
          <div class="col-md-12 mb-3 px-5">
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php echo get_field('video_grande_titulo'); ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_grande'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php echo get_field('video_grande_legenda'); ?>
                </figcaption>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <!-- /Fim da Linha 3 -->
                
        <!-- Linha 4 -->
        <!-- Componente audio -->
        <div class="row">
          <div class="col-md-12">
            <div class="px-5">
              <?php get_template_part('template/componentes/audio-part'); ?>
            </div>
          </div>
        </div>
        <!-- /Fim da Linha 4 -->
      </section>

      <!-- Linha 5 -->
      <section>

        <?php if (!empty(get_field('titulo_2'))) { ?>          
        <!-- Título 2 Produção de vídeo -->
        <div id="enviar-publicar" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php echo get_field('titulo_2') ?>
            </h1>
          </div>
        </div>
        <?php } ?>
        <!-- /Fim da Linha 5 -->

        <!-- Linha 6 -->
        <div class="row enviar-publicar">

          <!-- Coluna 1 Texto -->
          <div class="col-md-6 mb-3">

            <?php if (!empty(get_field('subtitulo_1'))) { ?>          
            <!-- Caixa de texto 1 -->
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php echo get_field('subtitulo_1'); ?>
              </h3>
              <div class="p-3">
                <?php echo get_field('texto_da_caixa_1'); ?>
              </div>

              <?php if (!empty(get_field('audio_linque_1'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner">
                      </div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                      <div class="slider" data-direction="horizontal">
                        <div class="progress">
                          <div class="pin" id="progress-pin" data-method="rewind">
                          </div>
                        </div>
                      </div>
                        <span class="total-time">
                          0:00
                        </span>
                      </div>
                      <div class="volume">
                        <div class="volume-btn">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                          </svg>
                        </div>
                        <div class="volume-controls hidden">
                          <div class="slider" data-direction="vertical">
                            <div class="progress">
                              <div class="pin" id="volume-pin" data-method="changeVolume">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <audio crossorigin>
                        <source src="<?php echo get_field('audio_linque_1'); ?>" type="audio/mp3">
                      </audio>
                    </div>
                    <!-- Script do audio player acima -->
                    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
                  </div>
                </div>
                <?php } ?>
              </div>
              <?php } ?>
            </div>

          <!-- Coluna 2 Texto -->
          <div class="col-md-6 mb-3">

            <?php if (!empty(get_field('subtitulo_2'))) { ?>
            <!-- Caixa de texto 2 -->
            <div class="borda-dir-base">
              <h3 class="fundo-preto p-3">
                <?php echo get_field('subtitulo_2'); ?>
              </h3>
              <div class="p-3">
                <?php echo get_field('texto_da_caixa_2'); ?>
              </div>

              <?php if (!empty(get_field('audio_linque_2'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner">
                      </div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                    <div class="slider" data-direction="horizontal">
                      <div class="progress">
                        <div class="pin" id="progress-pin" data-method="rewind">
                        </div>
                      </div>
                    </div>
                    <span class="total-time">
                      0:00
                    </span>
                  </div>
                  <div class="volume">
                    <div class="volume-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                      </svg>
                    </div>
                    <div class="volume-controls hidden">
                      <div class="slider" data-direction="vertical">
                        <div class="progress">
                          <div class="pin" id="volume-pin" data-method="changeVolume">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <audio crossorigin>
                    <source src="<?php echo get_field('audio_linque_2'); ?>" type="audio/mp3">
                  </audio>
                </div>
                <!-- Script do audio player acima -->
                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
              </div>
            </div>
            <?php } ?>        
          </div>
          <?php } ?>
        </div>

        <div class="row">
          <!-- Coluna 3 -->
          <div class="col-md-12 mb-3">
            <?php if (!empty(get_field('subtitulo_3'))) { ?>
            <!-- Caixa de texto 3 -->
            <div class="borda-dir-base">

              <h3 class="fundo-preto p-3">
                <?php echo get_field('subtitulo_3'); ?>
              </h3>

              <div class="p-3">
                <?php echo get_field('texto_da_caixa_3'); ?>
              </div>

              <?php if (!empty(get_field('audio_linque_3'))) { ?>
              <div class="row justify-content-center">
                <!-- Audio player -->
                <div id="audio" class="holder col-md-12 d-flex align-self-end p-3">
                  <div class="audio green-audio-player">
                    <span class="listen-text text-light me-3">
                      Escute
                    </span>
                    <div class="loading">
                      <div class="spinner"></div>
                    </div>
                    <div class="play-pause-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                        <path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                      </svg>
                    </div>
                    <div class="controls">
                      <span class="current-time">
                        0:00
                      </span>
                      <div class="slider" data-direction="horizontal">
                        <div class="progress">
                          <div class="pin" id="progress-pin" data-method="rewind"></div>
                        </div>
                      </div>
                      <span class="total-time">
                        0:00
                      </span>
                    </div>
                    <div class="volume">
                      <div class="volume-btn">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                          <path fill="#566574" fill-rule="evenodd" d="M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z" id="speaker"/>
                        </svg>
                      </div>
                      <div class="volume-controls hidden">
                        <div class="slider" data-direction="vertical">
                          <div class="progress">
                            <div class="pin" id="volume-pin" data-method="changeVolume">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <audio crossorigin>
                      <source src="<?php echo get_field('audio_linque_3'); ?>" type="audio/mp3">
                    </audio>
                  </div>
                </div>
                <!-- Script do audio player acima -->
                <script src="<?php echo esc_url(get_template_directory_uri()); ?>/library/js/audio-player.js"></script>
              </div>
              <?php } ?>
            </div>
          </div>
          <!-- /Fim da Coluna 3 -->
          <?php } ?>
        </div>
        </div>
      </section>

      <!-- Linha 7 -->
      <section>
        <?php if (!empty(get_field('titulo_3'))) { ?>
                          
        <!-- Título 2 -->
        <div id="enviar-publicar" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <div class="col-12 separador">
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php echo get_field('titulo_3') ?>
            </h1>
          </div>
        </div>
        <?php } ?>
        <!-- /Fim da Linha 5 -->

        <!-- Linha 6 -->
        <div class="row enviar-publicar">

          <?php if (!empty(get_field('titulo_video_1'))) { ?>         
          <!-- Caixa de vídeo 1 -->
          <div class="col-md-6 mb-3">
            <div class="borda-esq-base">
              <h3 class="fundo-preto p-3">
                <?php echo get_field('titulo_video_1'); ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_1'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php echo get_field('texto_video_1'); ?>
                </figcaption>
              </div>
            </div>
          </div>
          <?php } ?>
          <!-- Caixa de vídeo 2 -->
          <div class="col-md-6">
            <?php if (!empty(get_field('titulo_video_2'))) { ?>
            <!-- Caixa de texto 2 -->
            <div class="borda-dir-base">
              <h3 class="fundo-preto p-3">
                <?php echo get_field('titulo_video_2'); ?>
              </h3>
              <div class="p-3">
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-custom">
                  <iframe src="<?php echo get_field('video_2'); ?>" allowfullscreen></iframe>
                </div>
                <figcaption class="figure-caption text-start py-3">
                  <?php echo get_field('texto_video_2'); ?>
                </figcaption>
              </div>
            </div>
            <?php } ?>

          </div>
        </div>
      </section>
      <!-- /Fim da Linha 7 -->

      <!-- Linha 8 -->
      <section id="escrita" class="row titulo-h1 d-flex align-items-center justify-content-center">
        <div class="col-12 separador">
          
          <?php if (!empty(get_field('titulo_4'))) { ?>
          <!-- Título 4 Recursos  -->
          <h1 class="text-uppercase me-5 text-decoration-none text-muted">
            <?php echo get_field('titulo_4'); ?>
          </h1>
          <?php } ?>
        </div>
        <!-- /Fim da Linha 8 -->

        <!-- Linha 9 -->
        <div class="row">
          <div class="col-md-6 my-2">

            <?php if (!empty(get_field('imagem_1_titulo_4'))) { ?>
            <!-- Imagem Escrita 1 -->
            <div class="borda-esq-topo d-flex align-items-center justify-content-center">
              <figure class="figure p-3">
                <!--
                <figcaption class="figure-caption text-center py-3">
                                            
                </figcaption>
                -->
                <img src="<?php echo get_field('imagem_1_titulo_4'); ?>" class="img-fluid" width="600" alt="...">
              </figure>
            </div>
            <?php } ?>

          </div>
          <div class="col-md-6 my-2">

            <?php if (!empty(get_field('imagem_2_titulo_4'))) { ?>
            <!-- Imagem Escrita 2 -->
            <div class="borda-esq-topo d-flex align-items-center justify-content-center">
              <figure class="figure p-3">
                <!--
                <figcaption class="figure-caption text-center py-3">
                                              
                </figcaption>
                -->
                <img src="<?php echo get_field('imagem_2_titulo_4'); ?>" class="img-fluid" width="600" alt="...">
              </figure>
            </div>
            <?php } ?>
            
          </div>
        </div>
      </section>
      <!-- Linha 10 -->

      <!-- Linha 11 -->
      <section>
        <div id="recursos" class="row titulo-h1 d-flex align-items-center justify-content-center">
          <!-- Recursos Título 3 -->
          <div class="col-12 separador">

            <?php if (!empty(get_field('titulo_3_recursos'))) { ?>
            <h1 class="text-uppercase me-5 text-decoration-none text-muted">
              <?php echo get_field('titulo_3_recursos'); ?>
            </h1>
            <?php } ?>
          </div>
        </div>
        <!-- /Fim da Linha 9 -->

        <!-- Linha 10 -->
        <div class="row">
          <div class="col-md-4 my-2">
            <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
            <!-- Recurso 1 -->
            <div class="borda-esq-topo d-flex align-items-center justify-content-center">
              <figure class="figure p-3">
                <figcaption class="figure-caption text-center py-3">
                  <?php echo get_field('recurso_1_titulo'); ?>
                </figcaption>

                <img src="<?php echo get_field('recurso_1_imagem'); ?>" class="img-fluid" width="150" alt="...">
              </figure>
            </div>
            <?php } ?>
            </div>
            <div class="col-md-4 my-2">
              <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
              <!-- Recurso 2 -->
              <div class="borda-esq-topo d-flex align-items-center justify-content-center">
                <figure class="figure p-3">
                  <figcaption class="figure-caption text-center py-3">
                    <?php echo get_field('recurso_2_titulo'); ?>
                  </figcaption>
                  <img src="<?php echo get_field('recurso_2_imagem'); ?>" class="img-fluid pt-3 pb-4" width="150" alt="...">
                </figure>
              </div>
              <?php } ?>
            </div>
            <div class="col-md-4 my-2">
              <!-- Recurso 3 -->
              <?php if (!empty(get_field('recurso_1_titulo'))) { ?>
              <div class="borda-dir-topo d-flex align-items-center justify-content-center">
                <figure class="figure p-3">
                  <figcaption class="figure-caption text-center py-3">
                    <?php echo get_field('recurso_3_titulo'); ?>
                  </figcaption>
                  <img src="<?php echo get_field('recurso_3_imagem'); ?>" class="img-fluid py-3" width="150" alt="...">
                </figure>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <!-- /Fim da Linha 10 -->
      </section>
    </main>
    <?php endwhile; ?>
    <?php endif; ?>

<?php get_footer(); ?>
